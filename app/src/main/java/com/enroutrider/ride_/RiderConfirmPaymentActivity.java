package com.enroutrider.ride_;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.enroutrider.ride_.dbModels.TripCalculations;
import com.enroutrider.ride_.dbModels.TripComplete;
import com.enroutrider.ride_.network.ServiceCheck;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;

import org.apache.commons.lang3.time.DurationFormatUtils;

import id.voela.actrans.AcTrans;

public class RiderConfirmPaymentActivity extends AppCompatActivity {


    Spinner paymentSpinner;
    LinearLayout cashbutton, payment_update;
    private TextView cost,earning, finalCost, baseFare, timeCost, distanceCost, waitTime, cashbutton_text, PaymentMethod;

    ProgressBar loading;
    SharedPreferences settings;
    String selected_payment="CASH";
    ServiceCheck service = new ServiceCheck();
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rider_confirm_payment);

        cost = (TextView)findViewById(R.id.cost);

        finalCost = (TextView)findViewById(R.id.cost_plus_waiting);

        earning = (TextView)findViewById(R.id.earning);

        baseFare = findViewById(R.id.base_fare);

        cashbutton_text = findViewById(R.id.cashbutton_text);

        paymentSpinner =  findViewById(R.id.payment_spinner);

        loading = findViewById(R.id.show_loading);

        payment_update = findViewById(R.id.payment_update);

        timeCost = findViewById(R.id.time_amount);

        distanceCost = findViewById(R.id.distance_amount);

        PaymentMethod = findViewById(R.id.PaymentMethod);

        waitTime = findViewById(R.id.wait_amount);

        cashbutton = findViewById(R.id.cashbutton);

      settings = getSharedPreferences("details",
                Context.MODE_PRIVATE);

        ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter
                .createFromResource(this, R.array.payment_array,
                        android.R.layout.simple_spinner_item);

        staticAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        paymentSpinner.setAdapter(staticAdapter);

        paymentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                selected_payment = (String) parent.getItemAtPosition(position);
                paymentSpinner.setSelection(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        paymentmethoddisplay();

    }


    private void paymentmethoddisplay(){

       final SharedPreferences.Editor editor = settings.edit();

        final String paymentmethod = settings.getString("paymentMethod", "");

        final float tripcost = settings.getFloat("estimatedCost",0);

        final  String rider_id =  settings.getString("enrout_rider_id","");

        final  String request_id =  settings.getString("the_request_id","");

        TripCalculations calculate = new TripCalculations();

        double couponCost = calculate.promo_percentage(settings.getFloat("couponpercentage",0), tripcost);

        final double earnings = (float) Constants.riderPercentage*(tripcost + settings.getFloat("waitAmount",0) + couponCost);

        final double finalCosts = tripcost + settings.getFloat("waitAmount",0);
        final double timeCosts = ((float)Constants.costpermin) * (settings.getFloat("timeinMin",0));
        final  double waitCosts = settings.getFloat("waitAmount",0);
        final double distanceCosts = ((float)Constants.costperkm)*(settings.getFloat("distanceinKm",0));


        PaymentMethod.setText(paymentmethod);

        payment_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loading.setVisibility(View.VISIBLE);
                db.collection("REQUEST").document(request_id.trim()).update("paymentMethod",selected_payment).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        Toast.makeText(RiderConfirmPaymentActivity.this, "Payment method updated",Toast.LENGTH_SHORT).show();
                        PaymentMethod.setText(selected_payment);

                        loading.setVisibility(View.GONE);
                    }
                });

            }
        });

        finalCost.setText(""+ new Float(Math.round(finalCosts)) );

        baseFare.setText(""+  new Float(Math.round(Constants.baseFare)));

        timeCost.setText(""+  new Float(Math.round(timeCosts)));

        waitTime.setText(""+  new Float(Math.round(waitCosts)) );

        distanceCost.setText(""+  new Float(Math.round(distanceCosts)) );

        earning.setText(""+  new Float(Math.round(earnings)) );

        cost.setText(""+  new Float(Math.round(tripcost)));

        if(!paymentmethod.equals("CASH")){

            cashbutton_text.setText("COMPLETE TRIP");

            android.app.AlertDialog.Builder alert1 = new android.app.AlertDialog.Builder(RiderConfirmPaymentActivity.this);

            alert1.setMessage("Client is paying via Mobile Money / Visa Card. You can complete the trip");
            alert1.setTitle("Payment Type");
            alert1.setCancelable(false);

            alert1.setPositiveButton("OK", new DialogInterface.OnClickListener() {



                public void onClick(DialogInterface dialog, int whichButton) {


                    dialog.dismiss();

                }
            });
            alert1.show();



            cashbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    loading.setVisibility(View.VISIBLE);

                    editor.putBoolean("isFound", false);
                    editor.putBoolean("workingstatus",false);
                    editor.apply();



                    Boolean running = service.isMyServiceRunning(RiderGetLocationAndRequestService.class, RiderConfirmPaymentActivity.this);

                    if(!running){
                        startService(new Intent(getBaseContext(), RiderGetLocationAndRequestService.class));

                    }

                    db.collection("DISPATCH").document(settings.getString("dispatch_id","").trim()).update("status","COMPLETED");

                    addCompletedTrip(  new Float(Math.round(Constants.baseFare)), settings.getString("array_destinations",""),settings.getString("array_pickups",""), new Float( Math.round(distanceCosts)) ,rider_id.trim(),  new Float( Math.round(earnings)) ,
                            paymentmethod,request_id.trim(), settings.getString("senderName",""),settings.getString("senderPhoneNumber",""),"COMPLETED",   new Float( Math.round(timeCosts)),    new Float( Math.round(tripcost)),
                            settings.getString("trip_date", ""), new Float( Math.round(waitCosts)),  new Float( Math.round(finalCosts + (0.03*finalCosts))),DurationFormatUtils.formatDuration(settings.getLong("totalWaitTime",0), "HH' hr' mm' min'"));

                }
            });
        }

        else {

            android.app.AlertDialog.Builder alert1 = new android.app.AlertDialog.Builder(RiderConfirmPaymentActivity.this);

            alert1.setMessage("Client is paying with Cash. Kindly collect cash after trip completion");
            alert1.setTitle("Payment Type");
            alert1.setCancelable(false);

            alert1.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int whichButton) {

                    dialog.dismiss();

                }
            });
            alert1.show();

            cashbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    loading.setVisibility(View.VISIBLE);

                    editor.putBoolean("isFound", false);
                    editor.putBoolean("workingstatus",false);
                    editor.apply();

                    Boolean running = service.isMyServiceRunning(RiderGetLocationAndRequestService.class, RiderConfirmPaymentActivity.this);

                    if(!running){
                        startService(new Intent(getBaseContext(), RiderGetLocationAndRequestService.class));
                    }

                    db.collection("DISPATCH").document(settings.getString("dispatch_id","").trim()).update("status","COMPLETED");

                    if(paymentmethod.equals("Cash"))
                        db.collection("REQUEST").document(request_id.trim()).update("hasPaid", true);


                    addCompletedTrip(  new Float(Math.round(Constants.baseFare)), settings.getString("array_destinations",""),settings.getString("array_pickups",""), new Float( Math.round(distanceCosts)) ,rider_id.trim(),  new Float( Math.round(earnings)) ,
                            paymentmethod,request_id.trim(), settings.getString("senderName",""),settings.getString("senderPhoneNumber",""),"COMPLETED",   new Float( Math.round(timeCosts)),    new Float( Math.round(tripcost)),
                            settings.getString("trip_date", ""), new Float( Math.round(waitCosts)),  new Float( Math.round(finalCosts)), DurationFormatUtils.formatDuration(settings.getLong("totalWaitTime",0), "HH' hr' mm' min'") );
                }
            });

        }


    }


    public void addCompletedTrip(float basefare, String deliveryLoc, String pickuploc, float distanceCost, String driverRef, float earning,
                                 String paymentMethod, String requestRef, String senderName, String senderNumber, String status, float timeCost, float tripCost, String tripDate, float waitTime, float finalCost,String waitDuration )
    {

        TripCalculations calculate = new TripCalculations();

        double couponCost = calculate.promo_percentage(settings.getFloat("couponpercentage",0), tripCost);

        double enroutFee =  calculate.enrout_fee(finalCost,couponCost,earning);


        TripComplete trip_ended = new TripComplete();

        trip_ended.basefare = basefare;
        trip_ended.deliveryLocation = deliveryLoc;
        trip_ended.pickupLocation = pickuploc;
        trip_ended.distanceCost = distanceCost;
        trip_ended.driverReference = driverRef;
        trip_ended.earning = earning;
        trip_ended.paymentMethod = paymentMethod;
        trip_ended.requestReference = requestRef;
        trip_ended.senderName = senderName;
        trip_ended.senderNumber = senderNumber;
        trip_ended.status = status;
        trip_ended.timeCost = timeCost;
        trip_ended.tripCost = tripCost;
        trip_ended.tripDate = tripDate;
        trip_ended.waitTime = waitTime;
        trip_ended.finalCost = finalCost;
        trip_ended.payForMe = settings.getFloat("pay_for_me",0);
        trip_ended.couponAmount = Math.round(couponCost);
        trip_ended.enroutFee = Math.round(enroutFee);
        trip_ended.scheduleId = settings.getString("riderScheduleId","-").trim();
        trip_ended.waitDuration = waitDuration;

        db.collection("TRIPAUDITS").document().set(trip_ended)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                     reassignToNextTrip();

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("Failure", "Failed", e);


                        Toast.makeText(getApplicationContext(), "Unable to retrieve any data from server. Kindly check connection", Toast.LENGTH_LONG).show();
                        return;


                    }
                });;

          // double finalPMF = settings.getFloat("withdrawal_pfm",0) + settings.getFloat("pay_for_me",0);

           // db.collection("DRIVERS").document(rider_id.trim()).update("withdrawalAmount",finalPMF);


    }


    private void reassignToNextTrip() {

        Intent intent = new Intent(getApplicationContext(),ProcessTrip.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
        new AcTrans.Builder(RiderConfirmPaymentActivity.this).performSlideToTop();
        finish();

    }

    @Override
    public void onStop() {
        super.onStop();

    }


    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new AcTrans.Builder(this).performSlideToRight();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }


}
