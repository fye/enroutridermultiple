package com.enroutrider.ride_;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.enroutrider.ride_.network.ServiceCheck;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;
import java.util.Map;

import id.voela.actrans.AcTrans;


public class ProcessTrip extends Activity {

    SharedPreferences settings;

    SharedPreferences.Editor editor ;

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    ServiceCheck service = new ServiceCheck();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.process_trip);

        settings = getSharedPreferences("details",
                Context.MODE_PRIVATE);

        editor = settings.edit();

        final  String rider_id =  settings.getString("enrout_rider_id","");


        Boolean running = service.isMyServiceRunning(RiderGetLocationAndRequestService.class, ProcessTrip.this);

        if (!running) {
            startService(new Intent(this, RiderGetLocationAndRequestService.class));
        }


        prepareListData(tripDataArrayList -> displayItems(rider_id,tripDataArrayList));

    }

    private void prepareListData(final OnDataGathered onDataGathered) {

        String rider_id = settings.getString("enrout_rider_id","").trim();


            db.collection("DISPATCH").whereEqualTo("acceptedBy", rider_id).whereEqualTo("isAccepted",true).whereEqualTo("status","ONGOING")
                    .whereEqualTo("timeCode",settings.getString("timeCode","-").trim()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {

                    if(task.getResult().getDocuments()!=null)
                    {

                        onDataGathered.dataGathered(task.getResult().getDocuments());
                    }

                    else return;
                }
            });
            //ends here

           // startService(new Intent(getBaseContext(), RiderGetLocationAndRequestService.class));



    }

    private interface OnDataGathered{
        void dataGathered(List<DocumentSnapshot> tripDataArrayList);
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new AcTrans.Builder(this).performSlideToRight();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }



private void displayItems(String rider_id, List<DocumentSnapshot> data){

    if (data.size()==0) {
        db.collection("DRIVERS").document(rider_id.trim()).update("isWorking", false);

        Intent intent = new Intent(getApplicationContext(), StartLoadingActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
        new AcTrans.Builder(ProcessTrip.this).performSlideToLeft();
        finish();
    }


    if (data.size() > 1) {

        editor.putBoolean("isFound", true);
        editor.putBoolean("acceptedDispatch", true);
        editor.putBoolean("workingstatus", true);
        editor.putBoolean("isFull", true);
        editor.putBoolean("MoreTHanOneTrip", true);
        editor.apply();

    } else {
        editor.putBoolean("isFound", false);
        editor.putBoolean("acceptedDispatch", true);
        editor.putBoolean("workingstatus", true);
        editor.putBoolean("isFull", false);
        editor.putBoolean("MoreTHanOneTrip", false);
        editor.apply();


    }

    for (DocumentSnapshot the_document : data) {

        String the_documentId = the_document.getId();

        Map<String, Object> values_of_rider = the_document.getData();

        String trip_type = (String) values_of_rider.get("type");
        boolean actingStatus = (boolean) values_of_rider.get("actingOn");
        List<String> rejectedTrips = (List<String>) values_of_rider.get("rejectedBy");

        if (!rejectedTrips.contains(rider_id))
        {

            if (data.size() == 1) {
                if (!actingStatus)
                    db.collection("DISPATCH").document(the_documentId.trim()).update("actingOn", true).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            Intent intent = new Intent(getApplicationContext(), StartTripSummary.class);

                            intent.putExtra("trip_type",trip_type);
                            intent.putExtra("the_document_ID",the_documentId);
                            Constants.map = values_of_rider;

                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                            startActivity(intent);
                            new AcTrans.Builder(ProcessTrip.this).performSlideToLeft();

                        }
                    });

            }


            if (actingStatus) {

                Intent intent = new Intent(getApplicationContext(), StartTripSummary.class);

                intent.putExtra("trip_type",trip_type);
                intent.putExtra("the_document_ID",the_documentId);
                Constants.map = values_of_rider;

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                startActivity(intent);
                new AcTrans.Builder(ProcessTrip.this).performSlideToLeft();
            }
        }
    }
}

    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();


    }




}