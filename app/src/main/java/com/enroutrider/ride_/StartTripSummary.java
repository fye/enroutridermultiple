package com.enroutrider.ride_;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.enroutrider.ride_.dbModels.TripCalculations;
import com.enroutrider.ride_.dbModels.TripComplete;
import com.enroutrider.ride_.model.CustomArrayAdapter;
import com.enroutrider.ride_.model.CustomArrayAdapterDest;
import com.enroutrider.ride_.model.Destination;
import com.enroutrider.ride_.model.Pickup;
import com.enroutrider.ride_.network.ServiceCheck;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.json.JSONException;
import org.json.JSONObject;

import id.voela.actrans.AcTrans;


public class StartTripSummary extends AppCompatActivity {


    SharedPreferences settings;
    LinearLayout finish_trip;
    int pickupsForARequestCounter = 0;
    int detinationForPickupCounter = 0;
    ListView listView,listView2;
    private CustomArrayAdapter customArrayAdapter;
    String the_number;
    RelativeLayout pendingTrip;
    ProgressBar loading;

    Set<String> set = new HashSet<String>();
    //RelativeLayout On_trip;
    Destination destination;
    Pickup theselected_pickup;

    TextView customer_name;
    RelativeLayout takeover_view;
    MediaPlayer mp;
    SharedPreferences.Editor editor ;
    FloatingActionButton sos_alert, call_office, locate_previous_rider;

    private CustomArrayAdapterDest adapter2;

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    ServiceCheck service = new ServiceCheck();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trip_summary);

        settings = getSharedPreferences("details",
                Context.MODE_PRIVATE);

        editor = settings.edit();

        final  String rider_id =  settings.getString("enrout_rider_id","");

        finish_trip = (LinearLayout) findViewById(R.id.start_trip);

        pendingTrip = findViewById(R.id.pendingTrip);

        listView = (ListView) findViewById(R.id.pickup_destination_list);

        listView2 = (ListView) findViewById(R.id.destination_list);

        loading = findViewById(R.id.loading);

        customArrayAdapter = new CustomArrayAdapter(getApplicationContext(), R.layout.trip_summary_pickups);

        adapter2 = new CustomArrayAdapterDest(getApplicationContext(), R.layout.trip_summary_pickups);

        listView.setAdapter(customArrayAdapter);

        listView2.setAdapter(adapter2);

        customer_name = findViewById(R.id.customer_name);

        sos_alert = findViewById(R.id.SOS);

        takeover_view = findViewById(R.id.takeover);

        call_office = findViewById(R.id.call_office);

        locate_previous_rider = findViewById(R.id.get_previous_rider_cordinate);

        JSONObject mapData = new JSONObject();

        try {

            mapData.put("requestId", settings.getString("the_request_id", "").trim());

            SingletonSocket.getInstance().emit("request::leave::live-locations", mapData);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        ListUtils.setDynamicHeight(listView);
        ListUtils.setDynamicHeight(listView2);

        Boolean running = service.isMyServiceRunning(RiderGetLocationAndRequestService.class, StartTripSummary.this);

        if (!running) {
            startService(new Intent(this, RiderGetLocationAndRequestService.class));
        }



        db.collection("SETTINGS")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if (task.getResult().isEmpty())
                            {

                                return;

                            }
                            else{

                                List<settings> settings = task.getResult().toObjects(settings.class);

                                settings mSettings1 = settings.get(0);

                                Constants.baseFare = mSettings1.baseFare;
                                Constants.cancelPenalty = mSettings1.cancelPenalty;
                                Constants.costperkm = mSettings1.costPerKm;
                                Constants.costpermin = mSettings1.costPerMin;
                                Constants.acceptanceRadius = mSettings1.acceptanceRadius;
                                Constants.waitPenalty = mSettings1.waitPenalty;
                                Constants.riderPercentage = mSettings1.riderPercentage;

                            }
                        } else {
                            Log.w("Tag", "Error connecting to server.", task.getException());
                        }
                    }
                });

        String trip_type = getIntent().getStringExtra("trip_type");
        String the_documentId = getIntent().getStringExtra("the_document_ID");
        Map<String, Object> values_of_rider = Constants.map;



        view_processed_trip(trip_type,the_documentId,values_of_rider);

        if(settings.getBoolean("MoreTHanOneTrip",false))
        {
            pendingTrip.setVisibility(View.VISIBLE);
            animate(pendingTrip);

            playSoundMoreTrip(StartTripSummary.this);

        }

        sos_alert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                android.app.AlertDialog.Builder alert1 = new android.app.AlertDialog.Builder(StartTripSummary.this);


                alert1.setMessage("Are you in an EMERGENCY ?\nThis will send a distress alert to the ENROUT office. Do you want to continue ? ");
                alert1.setTitle("Distress Alert");
                alert1.setCancelable(false);

                alert1.setPositiveButton("Continue", new DialogInterface.OnClickListener() {


                    public void onClick(DialogInterface dialog, int whichButton) {

                        db.collection("DISPATCH").document(settings.getString("dispatch_id","")).update("rejectedBy", FieldValue.arrayUnion(rider_id),"status", "TAKEOVER").addOnSuccessListener(new OnSuccessListener<Void>() {

                            @Override
                            public void onSuccess(Void aVoid) {

                                db.collection("DRIVERS").document(rider_id.trim()).update("SOS", true);

                                db.collection("REQUEST").document(settings.getString("the_request_id","").trim()).update("rejectedBy", FieldValue.arrayUnion(rider_id));


                                addCancelledTrip(new Float( Constants.baseFare)   , settings.getString("array_destinations",""),settings.getString("array_pickups",""),   new Float( Math.round(Constants.costperkm)*(settings.getFloat("distanceinKm",0))),rider_id.trim(),0,
                                        settings.getString("paymentMethod",""),settings.getString("the_request_id","").trim(),
                                        settings.getString("customerName",""),settings.getString("customerPhoneNumber",""),"CANCELLED(SOS)",  new Float( Math.round(Constants.costpermin) * (settings.getFloat("timeinMin",0))) ,   new Float( Math.round(settings.getFloat("estimatedCost",0)))  ,
                                        settings.getString("trip_date", ""),   new Float( Math.round(settings.getFloat("waitAmount",0))), DurationFormatUtils.formatDuration(settings.getLong("totalWaitTime",0), "HH' hr' mm' min'") );


                                Intent intent = new Intent(getApplicationContext(), ProcessTrip.class);

                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                                startActivity(intent);

                                new AcTrans.Builder(StartTripSummary.this).performSlideToTop();

                                SharedPreferences.Editor editor = settings.edit();
                                editor.putBoolean("SOS",  true);
                                editor.apply();


                                scheduleAlarm();

                                Toast.makeText(StartTripSummary.this,
                                        "A distress alert has been sent to the Enrout office. We will contact you immediately.", Toast.LENGTH_LONG).show();

                                JSONObject mapData = new JSONObject();

                                try {
                                    mapData.put("riderId", rider_id.trim());
                                    mapData.put("requestId", settings.getString("the_request_id", "").trim());


                                    SingletonSocket.getInstance().emit("rider::sos::mode", mapData);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }
                        });


                    }
                });

                alert1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {


                    public void onClick(DialogInterface dialog, int whichButton) {

                        dialog.dismiss();
                    }
                });

                alert1.show();

            }
        });

        call_office.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callPhoneNumber(Constants.officeLine);
            }
        });



    }


    public void scheduleAlarm()

    {

        Long time = new GregorianCalendar().getTimeInMillis()+24*60*60*1000;

        Intent intentAlarm = new Intent(this, ResetSosReceiver.class);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        alarmManager.set(AlarmManager.RTC_WAKEUP,time, PendingIntent.getBroadcast(this,1,  intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT));

        //  Toast.makeText(this, "Alarm Scheduled for Tomorrow", Toast.LENGTH_LONG).show();

    }

    private static class settings{

        public settings () {}

        public double acceptanceRadius;
        public double baseFare;
        public double cancelPenalty;
        public double costPerKm;
        public double costPerMin;
        public double riderPercentage;
        public double waitPenalty;
    }


    private void animate(RelativeLayout pendingNotification)
    {
        Animation animation = new AlphaAnimation(1, 0); //to change visibility from visible to invisible
        animation.setDuration(1000); //1 second duration for each animation cycle
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE); //repeating indefinitely
        animation.setRepeatMode(Animation.REVERSE); //animation will start from end point once ended.
        pendingNotification.startAnimation(animation); //to start animation
    }



    private void getDestinationsOfAGivenPickup(String customerType, final List<DocumentReference> arrayListDestinations, final OnDestinationsGathered onDestinationsGathered){

        final int pending = getResourseId(this, "ic_lens", "drawable", getPackageName());
        final int done = getResourseId(this, "ic_pickup_address", "drawable", getPackageName());

        final List<Destination> destinationsArrayList = new ArrayList<>();

        detinationForPickupCounter = 0;

        for(final DocumentReference destination_references : arrayListDestinations){

            destination_references.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {



                    if (task.getResult() != null && task.getResult().exists()) {

                        String destinationsId = destination_references.getId();

                        Map<String, Object> destination_details = task.getResult().getData();

                        if (destination_details == null) return;


                        String recipientName = (String) destination_details.get("recipientName");
                        String recipientNumber = (String) destination_details.get("recipientNumber");
                        String destinationPlace = (String) destination_details.get("place");
                        Boolean deliveryStatus = (Boolean)destination_details.get("isDelivered");
                        Map<String, Object> destinationCordinates = (Map<String, Object>) destination_details.get("pos");
                        Map<String, Object> geopoints = (Map<String, Object>) destinationCordinates.get("geopoint");
                        double latitude = (double)geopoints.get("latitude");
                        double longitude = (double)geopoints.get("longitude");



                        if(deliveryStatus) {

                            SharedPreferences.Editor editor = settings.edit();

                            editor.putBoolean("hideCancel", true);

                            editor.apply();

                            if (customerType.equals("VENDOR")) {
                                int individualPrice = ((Number) destination_details.get("individualCost")).intValue();

                                destination = new Destination(done, destinationPlace, destinationsId, deliveryStatus, recipientName, recipientNumber, individualPrice, new LatLng(latitude, longitude));

                                destination.individualPrice = individualPrice;
                                destination.destinationId = destinationsId;
                                destination.recipientName = recipientName;
                                destination.isDelivered = deliveryStatus;
                                destination.recipientNumber = recipientNumber;
                                destination.placeName = destinationPlace;
                                destination.latLng = new LatLng(latitude, longitude);


                                destinationsArrayList.add(destination);

                                adapter2.add(destination);

                            }

                            else
                            {
                                destination = new Destination(done, destinationPlace, destinationsId, deliveryStatus, recipientName, recipientNumber, 0, new LatLng(latitude, longitude));

                                destination.destinationId = destinationsId;
                                destination.recipientName = recipientName;
                                destination.isDelivered = deliveryStatus;
                                destination.recipientNumber = recipientNumber;
                                destination.placeName = destinationPlace;
                                destination.latLng = new LatLng(latitude, longitude);


                                destinationsArrayList.add(destination);

                                adapter2.add(destination);


                            }

                        }

                        else
                        {

                            if (customerType.equals("VENDOR")) {
                                int individualPrice = ((Number) destination_details.get("individualCost")).intValue();

                                destination = new Destination(pending, destinationPlace, destinationsId, deliveryStatus, recipientName, recipientNumber, individualPrice, new LatLng(latitude, longitude));

                                destination.individualPrice = individualPrice;
                                destination.destinationId = destinationsId;
                                destination.recipientName = recipientName;
                                destination.isDelivered = deliveryStatus;
                                destination.recipientNumber = recipientNumber;
                                destination.placeName = destinationPlace;
                                destination.latLng = new LatLng(latitude, longitude);


                                destinationsArrayList.add(destination);

                                adapter2.add(destination);



                            }

                            else
                            {
                                destination = new Destination(pending, destinationPlace, destinationsId, deliveryStatus, recipientName, recipientNumber, 0, new LatLng(latitude, longitude));

                                destination.destinationId = destinationsId;
                                destination.recipientName = recipientName;
                                destination.isDelivered = deliveryStatus;
                                destination.recipientNumber = recipientNumber;
                                destination.placeName = destinationPlace;
                                destination.latLng = new LatLng(latitude, longitude);


                                destinationsArrayList.add(destination);

                                adapter2.add(destination);




                            }


                            listView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                    Destination the_selectedDestination = destinationsArrayList.get(i);

                                    Intent intent = new Intent(StartTripSummary.this, RiderAtWorkActivity.class);

                                    intent.putExtra("flag", false); //indicate delivery

                                    intent.putExtra("destinationCordinates", the_selectedDestination.getLatLng());
                                    intent.putExtra("destinationId", the_selectedDestination.getDestinationId());
                                    intent.putExtra("destinationPlace", the_selectedDestination.getPlaceName());
                                    intent.putExtra("recipientNumber", the_selectedDestination.getRecipientNumber());
                                    intent.putExtra("actedOnStatus",the_selectedDestination.getDelivered());

                                    if (customerType.equals("VENDOR"))
                                        intent.putExtra("individualPrice", the_selectedDestination.individualPrice);

                                    SharedPreferences.Editor editor = settings.edit();

                                    editor.putString("geolocation_name", the_selectedDestination.getPlaceName());
                                    editor.putBoolean("checkWhetherActed",the_selectedDestination.getDelivered());
                                    editor.putBoolean("isTapped", true);

                                    editor.apply();

                                    startActivity(intent);
                                    new AcTrans.Builder(StartTripSummary.this).performSlideToLeft();

                                    //     startService(new Intent(getBaseContext(), RiderGetLocationAndRequestService.class));


                                }
                            });


                        }



                        detinationForPickupCounter++;

                        if (detinationForPickupCounter >= (arrayListDestinations.size())){
                            onDestinationsGathered.destinationsGathered(destinationsArrayList);

                            adapter2.notifyDataSetChanged();
                        }



                    }

                }
            });


        }

    }



    private void getPickUpsOfAGivenRequest(final List <DocumentReference> arrayListPickups, final OnPickUpsGathered onPickUpsGathered){


        final int pending = getResourseId(this, "ic_lens", "drawable", getPackageName());
        final int done = getResourseId(this, "ic_pickup_address", "drawable", getPackageName());
        pickupsForARequestCounter = 0;


        final List<Pickup> pickupsArrayList = new ArrayList<>();

        for (final DocumentReference pickup_references : arrayListPickups ) {


            pickup_references.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                    if (task.getResult() != null && task.getResult().exists()) {

                        String pickupPointsId = pickup_references.getId();

                        Map<String, Object> pickup_details = task.getResult().getData();

                        if (pickup_details == null) return;


                        String category = (String) pickup_details.get("itemCategory");
                        String instructions = (String) pickup_details.get("instruction");
                        String pickupPlace = (String) pickup_details.get("place");
                        Boolean pickupStatus = (Boolean)pickup_details.get("isPickedup");
                        Map<String, Object> pickUpcordinates = (Map<String, Object>) pickup_details.get("pos");
                        Map<String, Object> geopoints = (Map<String, Object>) pickUpcordinates.get("geopoint");

                        double latitude = (double)geopoints.get("latitude");
                        double longitude = (double)geopoints.get("longitude");


                        if(pickupStatus){

                            SharedPreferences.Editor editor = settings.edit();

                            editor.putBoolean("hideCancel", true);

                            editor.apply();

                            theselected_pickup = new Pickup(done, pickupPlace,pickupStatus,pickupPointsId,category,instructions,new LatLng(latitude,longitude));

                            theselected_pickup.pickUpId = pickupPointsId;
                            theselected_pickup.itemCategory = category;
                            theselected_pickup.instruction = instructions;
                            theselected_pickup.isPickedup = pickupStatus;
                            theselected_pickup.placeName = pickupPlace;
                            theselected_pickup.latLng = new LatLng(latitude,longitude);

                            pickupsArrayList.add(theselected_pickup);
                            customArrayAdapter.add(theselected_pickup);




                        }

                        else{

                            theselected_pickup = new Pickup(pending, pickupPlace,pickupStatus,pickupPointsId,category,instructions,new LatLng(latitude,longitude));

                            theselected_pickup.pickUpId = pickupPointsId;
                            theselected_pickup.itemCategory = category;
                            theselected_pickup.instruction = instructions;
                            theselected_pickup.isPickedup = pickupStatus;
                            theselected_pickup.placeName = pickupPlace;
                            theselected_pickup.latLng = new LatLng(latitude,longitude);

                            pickupsArrayList.add(theselected_pickup);
                            customArrayAdapter.add(theselected_pickup);




                            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                    Pickup the_selectedPickup = pickupsArrayList.get(i);

                                    Intent intent = new Intent(StartTripSummary.this, RiderAtWorkActivity.class);

                                    intent.putExtra("flag", true); //indicate pickup

                                    intent.putExtra("pickup_instructions", the_selectedPickup.getInstruction());
                                    intent.putExtra("pickupCordinates", the_selectedPickup.getLatLng());
                                    intent.putExtra("PickupId", the_selectedPickup.getPickUpId());
                                    intent.putExtra("PickupPlace", the_selectedPickup.getPlaceName());
                                    intent.putExtra("actedOnStatus",the_selectedPickup.getitemPicked());

                                    SharedPreferences.Editor editor = settings.edit();

                                    editor.putString("geolocation_name", the_selectedPickup.getPlaceName());
                                    editor.putBoolean("checkWhetherActed",the_selectedPickup.getitemPicked());
                                    editor.putBoolean("isTapped", true);

                                    editor.apply();

                                    startActivity(intent);
                                    new AcTrans.Builder(StartTripSummary.this).performSlideToLeft();
                                    //   startService(new Intent(getBaseContext(), RiderGetLocationAndRequestService.class));



                                }
                            });


                        }



                        pickupsForARequestCounter ++;

                        if (pickupsForARequestCounter >= (arrayListPickups.size())){
                            onPickUpsGathered.pickupsGathered(pickupsArrayList);

                            customArrayAdapter.notifyDataSetChanged();
                        }

                    }

                }
            });

        }



    }

    private interface OnDestinationsGathered{
        void destinationsGathered(List<Destination> destinations);
    }

    private interface OnPickUpsGathered{
        void pickupsGathered(List<Pickup> pickupArrayList);
    }


    public void do_Counting(){

        db.collection("REQUEST").document(settings.getString("the_request_id","-").trim())
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                if (task.getResult() != null && task.getResult().exists()) {

                    Map<String, Object> count_trips = task.getResult().getData();

                    if (count_trips == null) return;

                    int taskCount = ((Number) count_trips.get("taskCount")).intValue();
                    int taskCompleted = ((Number) count_trips.get("taskCompleted")).intValue();

                    if((taskCount - taskCompleted) == 1){

                        editor.putBoolean("onLastTrip",  true);
                        editor.apply();
                    }
                    else{
                        editor.putBoolean("onLastTrip",  false);
                        editor.apply();
                    }

                    String paymentmethod = settings.getString("paymentMethod", "");

                    double finalCosts = settings.getFloat("estimatedCost",0) + settings.getFloat("waitAmount",0);

                    if((taskCount == taskCompleted))
                    {

                        if(!paymentmethod.equals("Cash"))
                            finalCosts += (0.03*finalCosts);

                        db.collection("REQUEST").document(settings.getString("the_request_id","-")
                                .trim()).update("actualCost", new Float( Math.round( finalCosts) ),"status", "COMPLETED", "completedAt", FieldValue.serverTimestamp())
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                        Intent intent = new Intent(StartTripSummary.this, RiderConfirmPaymentActivity.class);

                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK );

                                        startActivity(intent);
                                        new AcTrans.Builder(StartTripSummary.this).performSlideToTop();
                                        finish();

                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        // dialogdismiss();
                                        Toast.makeText(StartTripSummary.this, "Error completing trip. Check your internet connection", Toast.LENGTH_SHORT).show();
                                    }
                                })
                        ;


                    }

                }

                //maybe start service again here

            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //  dialogdismiss();
                        Toast.makeText(StartTripSummary.this, "Error completing trip. Check your internet connection", Toast.LENGTH_SHORT).show();
                    }
                });


    }




    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                // when adapter is null
                return;
            }
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new AcTrans.Builder(this).performSlideToRight();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }





    public void addCancelledTrip(float basefare, String deliveryLoc, String pickuploc, float distanceCost, String driverRef, float earning,
                                 String paymentMethod, String requestRef, String senderName, String senderNumber, String status, float timeCost, float tripCost, String tripDate, float waitTime, String waitDuration)
    {
        TripCalculations calculate = new TripCalculations();

        double couponCost = calculate.promo_percentage(settings.getFloat("couponpercentage",0), tripCost);

        double enroutFee =  calculate.enrout_fee(tripCost,couponCost,earning);

        TripComplete trip_ended = new TripComplete();

        trip_ended.basefare = basefare;
        trip_ended.deliveryLocation = deliveryLoc;
        trip_ended.pickupLocation = pickuploc;
        trip_ended.distanceCost = distanceCost;
        trip_ended.driverReference = driverRef;
        trip_ended.earning = earning;
        trip_ended.paymentMethod = paymentMethod;
        trip_ended.requestReference = requestRef;
        trip_ended.senderName = senderName;
        trip_ended.senderNumber = senderNumber;
        trip_ended.status = status;
        trip_ended.timeCost = timeCost;
        trip_ended.tripCost = tripCost;
        trip_ended.tripDate = tripDate;
        trip_ended. waitTime = waitTime;
        trip_ended.payForMe = settings.getFloat("pay_for_me",0);
        trip_ended.couponAmount = Math.round(couponCost);
        trip_ended.enroutFee = Math.round(enroutFee);
        trip_ended.scheduleId = settings.getString("riderScheduleId","-").trim();
        trip_ended.waitDuration = waitDuration;


        db.collection("TRIPAUDITS").document().set(trip_ended)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        Toast.makeText(getApplicationContext(), "You have cancelled the trip", Toast.LENGTH_LONG).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("Failure", "Failed", e);

                        Toast.makeText(getApplicationContext(), "Kindly check connection", Toast.LENGTH_LONG).show();
                        return;

                    }
                });;


    }



    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        if (mp != null) {
            if (mp.isPlaying()) {
                mp.stop();
            }
        }

    }

    public void playSoundMoreTrip(Context context) {
        if (mp != null) {
            if (mp.isPlaying()) {
                mp.stop();
            }
            mp.reset();
        }
        mp = MediaPlayer.create(context, R.raw.awaiting);
        mp.start();
    }



    public static int getResourseId(Context context, String pVariableName, String pResourcename, String pPackageName) throws RuntimeException {
        try {
            return context.getResources().getIdentifier(pVariableName, pResourcename, pPackageName);
        } catch (Exception e) {
            throw new RuntimeException("Error getting Resource ID.", e);
        }
    }

    public void callPhoneNumber(String number)
    {

        the_number = number;

        Intent callIntent = new Intent(Intent.ACTION_DIAL,Uri.parse("tel:" + number));
        startActivity(callIntent);

        new AcTrans.Builder(this).performSlideToTop();
    }


    private void view_processed_trip(String trip_type,String the_documentId ,Map<String, Object> values_of_rider)

    {

        if (trip_type.trim().equals("TAKEOVER")) {
            Map<String, Object> takeOver_geopoints = (Map<String, Object>) values_of_rider.get("currentLocation");

            double latitude = ((Number) takeOver_geopoints.get("latitude")).doubleValue();
            double longitude = ((Number) takeOver_geopoints.get("longitude")).doubleValue();

            takeover_view.setVisibility(View.VISIBLE);
            locate_previous_rider.show();

            locate_previous_rider.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?daddr="+latitude+","+longitude));
                    startActivity(intent);
                    new AcTrans.Builder(StartTripSummary.this).performSlideToTop();

                }
            });


        }


        final DocumentReference request_ref = (DocumentReference) values_of_rider.get("requestId");

        final String request_id = request_ref.getId();

          /*      list.addAll(Arrays.asList(request_id));

                set.addAll(list);*/

        request_ref.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                if (task.isSuccessful()) {

                    if (task.getResult() != null && task.getResult().exists()) {

                        final Map<String, Object> trip_details = task.getResult().getData();

                        if (trip_details == null)
                            return;

                        Log.d("Tag", "Current data: is not null");

                        settings = getSharedPreferences("details",
                                Context.MODE_PRIVATE);

                        SharedPreferences.Editor editor = settings.edit();

                        String senderId = (String) trip_details.get("senderId");
                        String senderName = (String) trip_details.get("senderName");
                        String senderPhoneNumber = (String) trip_details.get("senderPhoneNumber");
                        String paymentMethod = (String) trip_details.get("paymentMethod");
                        double payforMe = ((Number) trip_details.get("payForMe")).doubleValue();
                        double timeinMinute = (((Number) trip_details.get("estimatedTimeInSeconds"))).doubleValue() / 60;
                        double distanceinKm = (((Number) trip_details.get("estimatedDistanceInMeters"))).doubleValue() / 1000;
                        Timestamp trip_date = (Timestamp) trip_details.get("arrivedAt");
                        String customerType = (String) trip_details.get("customerType");
                        String requestStatus = (String) trip_details.get("status");
                        double estimatedCost = ((Number) trip_details.get("estimatedCost")).doubleValue();
                        int task_to_doCount = ((Number) trip_details.get("taskCount")).intValue();
                        double couponValueInPercentage = ((Number) trip_details.get("couponValueInPercentage")).doubleValue();

                        Date date = trip_date.toDate();

                        if (customerType == null)
                            customerType = "CUSTOMER";

                        customer_name.setText(senderName);

                        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm aaa", Locale.getDefault());

                        editor.putString("senderId", senderId);
                        editor.putString("senderName", senderName);
                        editor.putString("senderPhoneNumber", senderPhoneNumber);
                        editor.putString("paymentMethod", paymentMethod);
                        editor.putString("requestStatus", requestStatus);
                        editor.putString("customerType", customerType);
                        editor.putFloat("pay_for_me", (float) payforMe);
                        editor.putInt("taskCount", task_to_doCount);
                        editor.putFloat("distanceinKm", (float) distanceinKm);
                        editor.putFloat("timeinMin", (float) timeinMinute);
                        editor.putString("dispatch_id", the_documentId.trim());
                        editor.putString("trip_date", sdf.format(date));
                        editor.putFloat("couponpercentage", (float) couponValueInPercentage);
                        editor.putFloat("estimatedCost", (float) estimatedCost);
                        editor.putString("the_request_id", request_id);
                        editor.putStringSet("ListofPickupRequests", set);
                        editor.apply();

                            startService(new Intent(getApplicationContext(), RiderGetLocationAndRequestService.class));


                        List<DocumentReference> arrayListPickups = (List<DocumentReference>) trip_details.get("pickups");

                        String finalCustomerType = customerType;

                        getPickUpsOfAGivenRequest(arrayListPickups, pickupArrayList -> {


                            List<DocumentReference> arrayListDestinations = (List<DocumentReference>) trip_details.get("destinations");

                            getDestinationsOfAGivenPickup(finalCustomerType, arrayListDestinations, destinations -> {

                                loading.setVisibility(View.GONE);

                                do_Counting();

                            });


                        });


                    } else {

                        Toast.makeText(StartTripSummary.this, "Error on trip", Toast.LENGTH_SHORT).show();

                    }

                } else {
                    Toast.makeText(StartTripSummary.this, "Error on trip. Check internet connection", Toast.LENGTH_SHORT).show();

                }
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(StartTripSummary.this, "Error on trip. Check internet connection", Toast.LENGTH_SHORT).show();


                    }
                });





    }


}