package com.enroutrider.ride_.model;

public class StartTripListPlaces {
    private static final String TAG = "Items";

    private int done_img;
	private String placeName;

	public StartTripListPlaces(int done, String placeName) {
		super();
        this.setDone(done);
		this.setPlaceName(placeName);

	}

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public int getDone() {
        return done_img;
    }

    public void setDone(int done_img) {
        this.done_img = done_img;
    }


}