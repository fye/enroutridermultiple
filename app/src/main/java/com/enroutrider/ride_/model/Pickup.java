package com.enroutrider.ride_.model;

import com.google.android.gms.maps.model.LatLng;

public class Pickup {

    public String pickUpId;
    public boolean isPickedup;
    public String itemCategory;
    public String instruction;
    public LatLng latLng;
    public int done_img;
    public String placeName;

    public Pickup(int done, String placeName, boolean isPicked, String pickupID, String ItemCategory, String instruction, LatLng location) {
        super();
        this.setDone(done);
        this.setPlaceName(placeName);
        this.setitemPicked(isPicked);
        this.setPickUpId(pickupID);
        this.setItemCategory(ItemCategory);
        this.setInstruction(instruction);
        this.setLatLng(location);


    }

    public String getPickUpId() {
        return pickUpId;
    }

    public void setPickUpId(String pickUpId) {
        this.pickUpId = pickUpId;
    }


    public String getItemCategory() {
        return itemCategory;
    }

    public void setItemCategory(String itemCategory) {
        this.itemCategory = itemCategory;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public int getDone() {
        return done_img;
    }

    public void setDone(int done_img) {
        this.done_img = done_img;
    }


    public boolean getitemPicked() {
        return isPickedup;
    }

    public void setitemPicked(Boolean picked) {
        this.isPickedup = picked;
    }






}