package com.enroutrider.ride_;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.enroutrider.ride_.model.DataParser;
import com.enroutrider.ride_.model.PostLocationUpdates;
import com.enroutrider.ride_.model.PostTimeToService;
import com.enroutrider.ride_.network.ServiceCheck;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.tapadoo.alerter.Alerter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.voela.actrans.AcTrans;


public class RiderAtWorkActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
   float Latitude = 0;
   float Longitude = 0;
    SharedPreferences settings;
    ArrayList<LatLng> MarkerPoints;
    public MediaPlayer mp;
    LinearLayout pickupViews, deliveryViews;
    String the_number;


    private TextView tripType,trip_fare, paymentMethod, pickup_name, delivery_name,payForME,picked_up_button, delivery_button, cancel_trip;
    LinearLayout call_number, locate, instructions, text;
    KProgressHUD dialog;

    ServiceCheck service = new ServiceCheck();

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rider_at_work2);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        EventBus.getDefault().register(this);

        MarkerPoints = new ArrayList<>();

        settings = getSharedPreferences("details",
                Context.MODE_PRIVATE);

        final  String rider_id =  settings.getString("enrout_rider_id","");

        trip_fare = (TextView) findViewById(R.id.trip_fare);
        paymentMethod = (TextView) findViewById(R.id.payment_method);
        pickup_name = (TextView) findViewById(R.id.pickuplocationname);
        tripType = findViewById(R.id.trip_detail_type);
        delivery_name = (TextView) findViewById(R.id.deliverylocname);
        locate = (LinearLayout) findViewById(R.id.locate);
        text = findViewById(R.id.text);
        call_number = (LinearLayout) findViewById(R.id.call);
        instructions = (LinearLayout) findViewById(R.id.instructions);
        picked_up_button = (TextView) findViewById(R.id.item_pickedbutton);

        payForME = (TextView)findViewById(R.id.pfm_amount) ;
        delivery_button = (TextView) findViewById(R.id.item_delivery);
        cancel_trip = (TextView)findViewById(R.id.cancel_trip);

        trip_fare.setText("" + settings.getFloat("estimatedCost", 0));
        paymentMethod.setText(settings.getString("paymentMethod", ""));
        payForME.setText(""+settings.getFloat("pay_for_me",0));

        pickupViews = (LinearLayout)findViewById(R.id.pickup_views);
        deliveryViews = (LinearLayout)findViewById(R.id.delivery_views);

        final SharedPreferences.Editor editor = settings.edit();

        editor.putInt("check_geofence", 1);

        JSONObject mapData = new JSONObject();

        try {

            mapData.put("requestId", settings.getString("the_request_id", "").trim());

            SingletonSocket.getInstance().emit("request::join::live-locations", mapData);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        editor.apply();

        LoadTripsFunction();

        Boolean hideCancel = settings.getBoolean("hideCancel", false);

        if(hideCancel)
            cancel_trip.setVisibility(View.GONE);

        cancel_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), CancelReason.class);

                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                startActivity(intent);
                new AcTrans.Builder(RiderAtWorkActivity.this).performSlideToTop();
            }
        });

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }


        buildGoogleApiClient();

       // mMap.setMyLocationEnabled(true);


    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();

    }



    private void checkRadius(double current_latitude, double current_longitude, double destination_latitude, double destination_longitude){

        float[] distance = new float[2];

        settings = getSharedPreferences("details",
                Context.MODE_PRIVATE);

        boolean showDialog =  settings.getBoolean("showAgain", true) ;

        Circle mapcircle = mMap.addCircle(new CircleOptions()
                .strokeColor(Color.TRANSPARENT)
                .center(new LatLng(destination_latitude,destination_longitude))

                .radius( new Float( Math.round(Constants.acceptanceRadius)))
        );

        Location.distanceBetween(current_latitude,
                current_longitude, mapcircle.getCenter().latitude,
                mapcircle.getCenter().longitude, distance);

        if (distance[0] <= mapcircle.getRadius()) {

            if (showDialog)
            {
                Intent i = new Intent(getApplicationContext(), ArrivalDialog.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
                new AcTrans.Builder(RiderAtWorkActivity.this).performSlideToTop();

        }

        }

    }


    private void LoadTripsFunction() {

        SharedPreferences.Editor editor = settings.edit();

        boolean flag = getIntent().getBooleanExtra("flag", true);

        final String pickupInstruction = getIntent().getStringExtra("pickup_instructions");
        final LatLng pickupCordinates = getIntent().getExtras().getParcelable("pickupCordinates");
        final String pickupId = getIntent().getStringExtra("PickupId");
        String pickupPlace = getIntent().getStringExtra("PickupPlace");
        boolean actingOnStatus = getIntent().getBooleanExtra("actedOnStatus",false);
        final String recipientNumber = getIntent().getStringExtra("recipientNumber");
        final LatLng destinationCordinates = getIntent().getExtras().getParcelable("destinationCordinates");
        final String destinationId = getIntent().getStringExtra("destinationId");
        String destinationPlace = getIntent().getStringExtra("destinationPlace");

        editor.putBoolean("tripType", flag);

        editor.apply();


        Boolean running = service.isMyServiceRunning(RiderGetLocationAndRequestService.class, RiderAtWorkActivity.this);

        if(!running){

            startService(new Intent(getBaseContext(), RiderGetLocationAndRequestService.class));

        }

        if (flag)

        {
            tripType.setText("Pickup");

            editor.putString("targetId", pickupId);
            editor.putFloat("pickup_longitude",(float) pickupCordinates.longitude);
            editor.putFloat("pickup_latitude",(float) pickupCordinates.latitude);

            if(actingOnStatus){
                picked_up_button.setVisibility(View.GONE);
                delivery_button.setVisibility(View.GONE);
                cancel_trip.setVisibility(View.GONE);
            }

            else
            {
            picked_up_button.setVisibility(View.VISIBLE);

            delivery_button.setVisibility(View.GONE);
            }

            deliveryViews.setVisibility(View.INVISIBLE);




            editor.putString("arrivalIdToUpdate", pickupId);

            editor.apply();


            text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i=new Intent(RiderAtWorkActivity.this,MessagingActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    new AcTrans.Builder(RiderAtWorkActivity.this).performSlideToTop();

                }
            });


            instructions.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                AlertDialog.Builder alert1 = new AlertDialog.Builder(RiderAtWorkActivity.this);

                alert1.setMessage(pickupInstruction);

                alert1.setCancelable(true);

                AlertDialog diag = alert1.create();

                diag.show();

                if (pickupInstruction == null) {
                    Toast.makeText(RiderAtWorkActivity.this, "No instructions given. Kindly call customer", Toast.LENGTH_SHORT).show();

                }
            }

        });


        locate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?daddr="+pickupCordinates.latitude+","+pickupCordinates.longitude));
                startActivity(intent);
                new AcTrans.Builder(RiderAtWorkActivity.this).performSlideToTop();
          /*
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" +  + "," +  + "&mode=d");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);*/

            }
        });


        final String sender_number = settings.getString("senderPhoneNumber", "");

        pickup_name.setText(pickupPlace);


        call_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callPhoneNumber(sender_number);

            }
        });


        picked_up_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog = KProgressHUD.create(RiderAtWorkActivity.this)
                        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                        .setBackgroundColor(Color.TRANSPARENT)
                        .setCancellable(false)
                        .setAnimationSpeed(2)
                        .setDimAmount(0.5f)
                        .show();



                db.collection("REQUEST").document(settings.getString("the_request_id","-").trim())
                        .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                        if (task.getResult() != null && task.getResult().exists()) {

                            Map<String, Object> count_trips = task.getResult().getData();

                            if (count_trips == null) return;

                            int taskCompleted = ((Number) count_trips.get("taskCompleted")).intValue();

                            db.collection("PICKUPPOINTS").document(pickupId.trim()).update("isPickedup", true, "pickedupAt", FieldValue.serverTimestamp());

                            playRequestUpdated(RiderAtWorkActivity.this);

                            db.collection("REQUEST").document(settings.getString("the_request_id","").trim())
                                    .update("taskCompleted", taskCompleted + 1 ).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    editor.putInt("check_geofence", 0);

                                    editor.putBoolean("showAgain", true);

                                    editor.putBoolean("calculate_wait_amount", false);

                                    editor.apply();

                                    /*Intent intent2 = new Intent(getBaseContext(),RiderGetLocationAndRequestService.class);

                                    startService(intent2);*/

                                    dialogdismiss();

                                    Intent intent = new Intent(RiderAtWorkActivity.this, ProcessTrip.class);

                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK );

                                    startActivity(intent);
                                    new AcTrans.Builder(RiderAtWorkActivity.this).performSlideToTop();
                                }
                            });

                        }

                    }
                });


            }
        });

    }


    else{

        if(settings.getString("customerType", "none").equals("VENDOR"))
        {
            Alerter.create(this)
                    .setTitle("Collect GHc" + getIntent().getIntExtra("individualPrice",0) + " upon delivery")
                    .setIcon(R.mipmap.amount)
                   // .setIconColorFilter(getResources().getColor(R.color.white))
                    .setBackgroundColorRes(R.color.colorAccent)
                    .enableSwipeToDismiss()
                    .setDuration(3600000)
                    //.setIconColorFilter(0) // Optional - Removes white tint
                    .show();

        }
            tripType.setText("Delivery");

            if(actingOnStatus)
            {
                picked_up_button.setVisibility(View.GONE);
                delivery_button.setVisibility(View.GONE);
                cancel_trip.setVisibility(View.GONE);
            }
            else
                {
            picked_up_button.setVisibility(View.GONE);

            delivery_button.setVisibility(View.VISIBLE);
                }

            instructions.setVisibility(View.GONE);

            pickupViews.setVisibility(View.INVISIBLE);

            editor.putString("arrivalIdToUpdate", destinationId);

            editor.putString("targetId", destinationId);

            editor.putFloat("pickup_longitude",(float) destinationCordinates.longitude);
            editor.putFloat("pickup_latitude",(float) destinationCordinates.latitude);

            editor.apply();


            if(!running){

                startService(new Intent(getBaseContext(), RiderGetLocationAndRequestService.class));
            }


            text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i=new Intent(RiderAtWorkActivity.this,MessagingActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    new AcTrans.Builder(RiderAtWorkActivity.this).performSlideToTop();

                }
            });

         /*   instructions.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    AlertDialog.Builder alert1 = new AlertDialog.Builder(RiderAtWorkActivity.this);

                    alert1.setMessage(pickupInstruction);

                    alert1.setCancelable(true);

                    AlertDialog diag = alert1.create();

                    diag.show();

                    if (pickupInstruction == null) {
                        Toast.makeText(RiderAtWorkActivity.this, "No instructions given. Kindly call customer", Toast.LENGTH_SHORT).show();

                    }
                }

            });*/


            locate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?daddr="+destinationCordinates.latitude+","+destinationCordinates.longitude));
                    startActivity(intent);
                    new AcTrans.Builder(RiderAtWorkActivity.this).performSlideToTop();
                  /*  Uri gmmIntentUri = Uri.parse("google.navigation:q=" +  + "," +  + "&mode=d");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
*/
                }
            });


            final String sender_number = recipientNumber;

            editor.putString("recipientNumber",recipientNumber);
            editor.apply();

          delivery_name.setText(destinationPlace);


            call_number.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    callPhoneNumber(sender_number);

                }
            });


            delivery_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialog = KProgressHUD.create(RiderAtWorkActivity.this)
                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                            .setBackgroundColor(Color.TRANSPARENT)
                            .setCancellable(false)
                            .setAnimationSpeed(2)
                            .setDimAmount(0.5f)
                            .show();

                    db.collection("REQUEST").document(settings.getString("the_request_id","-").trim())
                            .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                            if (task.getResult() != null && task.getResult().exists()) {

                                Map<String, Object> count_trips = task.getResult().getData();

                                if (count_trips == null) return;

                                int taskCompleted = ((Number) count_trips.get("taskCompleted")).intValue();

                                db.collection("DESTINATIONS").document(destinationId.trim()).update("isDelivered",true,"deliveredAt",FieldValue.serverTimestamp());

                                playRequestUpdated(RiderAtWorkActivity.this);

                                SharedPreferences.Editor editor = settings.edit();

                                db.collection("REQUEST").document(settings.getString("the_request_id","").trim())
                                        .update("taskCompleted", taskCompleted + 1 ).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {


                                        editor.putInt("check_geofence", 0);

                                        editor.putBoolean("showAgain", true);

                                        editor.putBoolean("calculate_wait_amount", false);

                                        editor.apply();

                                        /*Intent intent2 = new Intent(getBaseContext(),RiderGetLocationAndRequestService.class);

                                        startService(intent2);*/

                                        dialogdismiss();

                                        Intent intent = new Intent(RiderAtWorkActivity.this, ProcessTrip.class);

                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK );

                                        startActivity(intent);
                                        new AcTrans.Builder(RiderAtWorkActivity.this).performSlideToTop();

                                    }
                                });



                            }

                        }
                    });




                }
            });


        }

    }



    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    protected void onResume() {
        super.onResume();

       Boolean hideCancel = settings.getBoolean("hideCancel", false);

       if(hideCancel)
       cancel_trip.setVisibility(View.GONE);

    }

    @Override
    protected void onStop() {
        super.onStop();
        dialogdismiss();
    }

    @Subscribe
    public void locationsReceived(PostLocationUpdates locations){

        Latitude = locations.latitude;
        Longitude = locations.longitude;


        if (getApplicationContext() != null) {


            drawPath(new LatLng( Latitude,Longitude),
                    new LatLng(settings.getFloat("pickup_latitude",0),settings.getFloat("pickup_longitude",0)));

            LatLng latLng = new LatLng( Latitude,Longitude);

            LatLng dest = new LatLng(settings.getFloat("pickup_latitude",0),settings.getFloat("pickup_longitude",0));

            // mMap.moveCamera(getBounds(latLng, dest));
            mMap.animateCamera(getBounds(latLng, dest));

            int geo_value = settings.getInt("check_geofence",0);

            if(geo_value==1)

            {
                checkRadius(  Latitude,Longitude,
                        settings.getFloat("pickup_latitude",0),settings.getFloat("pickup_longitude",0));


              /*  checkRadius(location.getLatitude(),location.getLongitude(),
                   location.getLatitude()  ,location.getLongitude());*/


            }


            mMap.addMarker(new MarkerOptions()
                    .position( latLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.rider_icon)));



        }

    }

    @Override
    public void onPause() {
        super.onPause();

        dialogdismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mp != null) {
            if (mp.isPlaying()) {
                mp.stop();
            }
        }
        EventBus.getDefault().unregister(this);

        dialogdismiss();
    }

    public void dialogdismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }


    private CameraUpdate getBounds(LatLng src, LatLng dest){

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(src);
        builder.include(dest);
        LatLngBounds bounds = builder.build();
        Point displaySize = new Point();
        getWindowManager().getDefaultDisplay().getSize(displaySize);
        return CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, 250, 30);

    }



    public void drawPath(LatLng originpoint, LatLng destinationpoint){

            // Getting URL to the Google Directions API
            String url = getUrl(originpoint, destinationpoint);
            Log.d("onMapClick", url.toString());
            FetchUrl FetchUrl = new FetchUrl();

            // Start downloading json data from Google Directions API
            FetchUrl.execute(url);
            //move map camera



    }



    private String getUrl(LatLng origin, LatLng dest) {

        boolean flag = getIntent().getBooleanExtra("flag", true);

        if(!flag)
        {
            mMap.clear();
            mMap.addMarker(new MarkerOptions().position(dest).icon(BitmapDescriptorFactory.fromResource(R.mipmap.pick)));

        }
        else
            {
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(dest).icon(BitmapDescriptorFactory.fromResource(R.mipmap.drop)));

            }


        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "mode=driving&alternatives=true&key=AIzaSyAyiC71ahtoQ7XEG1zBUBTeQRAiLWsStfA&sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }


    // Fetches data from url passed
    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());

            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }




    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask",jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask","Executing routes");
                Log.d("ParserTask",routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask",e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            String the_time="";
            PolylineOptions lineOptions = null;

            try {

                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        String duration = point.get("duration");
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                        the_time = duration;
                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(5);
                    lineOptions.color(getResources().getColor(R.color.map_color));



                    Log.d("onPostExecute", "onPostExecute lineoptions decoded");

                }


            }catch (NullPointerException e){}

            // Drawing polyline in the Google Map for the i-th route
            if(lineOptions != null) {
                mMap.addPolyline(lineOptions);

                EventBus.getDefault().post(new PostTimeToService(the_time));
            }
            else {
                Log.d("onPostExecute","without Polylines drawn");
             //   Toast.makeText(RiderAtWorkActivity.this,"Error getting route",Toast.LENGTH_LONG).show();

            }
        }
    }


    public void playRequestUpdated(Context context) {
        if (mp != null) {
            if (mp.isPlaying()) {
                mp.stop();
            }
            mp.reset();
        }
        mp = MediaPlayer.create(context, R.raw.customer_sound);
        mp.start();
    }


    public void callPhoneNumber(String number)
    {

        the_number = number;

        Intent callIntent = new Intent(Intent.ACTION_DIAL,Uri.parse("tel:" + number));
        startActivity(callIntent);
        new AcTrans.Builder(this).performSlideToTop();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new AcTrans.Builder(this).performSlideToRight();
    }

}