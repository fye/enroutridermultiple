package com.enroutrider.ride_;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Hardik on 23-Mar-17.
 */

public class HelpActivity extends AppCompatActivity implements View.OnClickListener {

    TextView faqs,accessibility,signing_up,guide_me;
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help);

        faqs = (TextView)findViewById(R.id.faqs);
        accessibility = (TextView)findViewById(R.id.accessibility);
        signing_up = (TextView)findViewById(R.id.sign_up);
        guide_me = (TextView)findViewById(R.id.enrout_guide);

        faqs.setOnClickListener(this);
        accessibility.setOnClickListener(this);
        signing_up.setOnClickListener(this);
        guide_me.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.faqs:

            {
                Intent i = new Intent(HelpActivity.this, HelpFAQActivity.class);
                startActivity(i);
                ;

                break;
            }
            case R.id.accessibility:

            {
                Intent j = new Intent(HelpActivity.this, HelpAccessibility_Activity.class);
                startActivity(j);
                ;

                break;
            }

            case R.id.enrout_guide:

            {
                Intent k = new Intent(HelpActivity.this, Help_guide.class);
                startActivity(k);
                ;

                break;
            }
            case R.id.sign_up:

            {
                Intent l = new Intent(HelpActivity.this, HelpSign_Up.class);
                startActivity(l);
                ;

                break;

            }
        }
    }

}
