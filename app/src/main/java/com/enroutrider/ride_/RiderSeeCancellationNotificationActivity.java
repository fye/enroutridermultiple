package com.enroutrider.ride_;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import androidx.core.app.NotificationCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import id.voela.actrans.AcTrans;


public class RiderSeeCancellationNotificationActivity extends AppCompatActivity {

    Button ok;
    public MediaPlayer mp;
    TextView sender;
    SharedPreferences settings;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rider_see_cancellation_notification);

        settings = getSharedPreferences("details", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = settings.edit();

        ok = findViewById(R.id.okay);
        sender = findViewById(R.id.sender_name);

        sender.setText(settings.getString("senderName","Customer"));

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                editor.putString("the_request_id", "-");

                editor.apply();

                Intent intent = new Intent(RiderSeeCancellationNotificationActivity.this, ProcessTrip.class);

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                startActivity(intent);
                new AcTrans.Builder(RiderSeeCancellationNotificationActivity.this).performSlideToTop();

                if (mp != null) {
                    if (mp.isPlaying()) {
                        mp.stop();
                    }
                    mp.reset();
                }


                finish();

            }
        });

        sendNotification( true,  true);


    }



    private void sendNotification( boolean sound, boolean vibrate) {
        Intent intent = new Intent(this, StartLoadingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Notification notification = new Notification();

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // String channelId = "channel-01";
        String channelId = "enrout-01";
        String channelName = "Enrout";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,channelId);


        if (sound) {
            notification.defaults |= Notification.DEFAULT_SOUND;
        }

        if (vibrate) {
            notification.defaults |= Notification.DEFAULT_VIBRATE;
        }

        notificationBuilder.setDefaults(notification.defaults);
        notificationBuilder.setSmallIcon(R.mipmap.outer_icon)
                .setContentTitle("Enrout")
                .setContentText("Client Has Cancelled Request")
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);


        notificationManager.notify(0, notificationBuilder.build());

        MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.notification);
        mp.start();

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new AcTrans.Builder(this).performSlideToRight();
    }


}
