package com.enroutrider.ride_.dbModels;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;



public class RegistrationModule {

    public String fullname;
   public String phoneNumber;
    public String emailAddress;
public  String gender;
public String dob;
public boolean isApproved;
public boolean isWorking;
public boolean isOnline;
public boolean phoneRegistration;
public String vehicle;
public String fleet;
public boolean SOS;
public double averageRating;
public double totalNoOfVotes;
public double sumOfVotes;
public double withdrawalAmount;
public String scheduleId;

   @ServerTimestamp
   public Date createdAt;

}
