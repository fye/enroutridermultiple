package com.enroutrider.ride_;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;

//


public class Send_mailActivity extends AppCompatActivity {

    TextView email_exists, verify_text;
    Button continue_verify, check;
    EditText verifycode;
    SharedPreferences prefs;
    String verify_code_str="";
    String the_code_sent="";
    String code_entered="";
    String code_generated="";
    View view_verify;
    KProgressHUD dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verify_email);
        getApplicationContext().registerReceiver(receiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        email_exists=(TextView)findViewById(R.id.wait_text);
        verify_text=(TextView)findViewById(R.id.text_confirm_email);
        continue_verify=(Button)findViewById(R.id.continue_anyways);
        check=(Button)findViewById(R.id.check_code);
        verifycode=(EditText)findViewById(R.id.verify_code);
        view_verify=(View)findViewById(R.id.view_verify);


        verifycode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                verify_code_str = verifycode.getText().toString();

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() < 6) {
                    check.setVisibility(View.INVISIBLE);

                } else {
                    check.setVisibility(View.VISIBLE);

                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                verify_code_str = verifycode.getText().toString();


                SharedPreferences.Editor editor2 = prefs.edit();
                editor2.putString("the_verification_code", verify_code_str); //InputString: from the response from JSon

                editor2.commit();

            }
        });



        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prefs=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
               code_generated=prefs.getString("the_sent_verification_code","");
                code_entered=prefs.getString("the_verification_code","");

                if(code_entered.equals(code_generated)){


                    Intent intent = new Intent(getApplicationContext(),StartLoadingActivity.class);
                    //         Toast.makeText(getApplicationContext(),"Registration Successful, Please visit our Office to complete Registration"+result.getString("usertoken"),Toast.LENGTH_LONG).show();
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    startActivity(intent);

                    finish();


                }

                else{

                    Toast.makeText(Send_mailActivity.this,"Invalid Code entered",Toast.LENGTH_LONG).show();
                }

            }



        });


    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(!ConnectivityStatus.isConnected(getApplicationContext())){
                // no connection
                Toast.makeText(getApplicationContext(),"Email failed to send..Check your Internet Connection",Toast.LENGTH_LONG).show();
            }else {
                // connected

                continue_verify.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        email_exists.setVisibility(View.GONE);
                        continue_verify.setVisibility(View.GONE);
                        verify_text.setVisibility(View.VISIBLE);
                        verifycode.setVisibility(View.VISIBLE);
                        check.setVisibility(View.INVISIBLE);
                        view_verify.setVisibility(View.VISIBLE);


                        //****code to generate random function

                        int the_code = Integer.parseInt(("" + Long.parseLong(("" + Math.random()).substring(2))).substring(0, 6));

                        the_code_sent=""+the_code;

                        //Toast.makeText(Send_mailActivity.this,the_code_sent,Toast.LENGTH_LONG).show();

                        SharedPreferences.Editor editor3 = prefs.edit();
                        editor3.putString("the_sent_verification_code", the_code_sent); //InputString: from the response from JSon

                        editor3.commit();

                        SharedPreferences settings = getSharedPreferences("details",
                                Context.MODE_PRIVATE);

                        //*****code to send email

                        String verifyemail = settings.getString("verifyemail","");

                        new SendMailTask(Send_mailActivity.this).execute("info@enroutdelivery.com",
                                "Eelog@5656", verifyemail,"Enrout Verification code", "Your Account Verification code is "+the_code_sent);


                    }



                });
            }
        }
    };

    private Context getDialogContext()
    {
        Context context;
        if (getParent() != null)
            context = getParent();
        else
            context = this;
        return context;
    }


    @Override
    protected void onResume() {
        super.onResume();
        getApplicationContext().registerReceiver(receiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

    }

    @Override
    protected void onPause() {
        super.onPause();
        getApplicationContext().unregisterReceiver(receiver);
        dialogdismiss();
    }

    @Override
    public void onStop() {
        super.onStop();

        dialogdismiss();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();



        dialogdismiss();
    }

    public void dialogdismiss(){
        if (dialog != null) {
            dialog.dismiss();
        }
    }



}
