package com.enroutrider.ride_;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.enroutrider.ride_.dbModels.TripCalculations;
import com.enroutrider.ride_.dbModels.TripComplete;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.json.JSONException;
import org.json.JSONObject;

import id.voela.actrans.AcTrans;


public class CancelReason extends Activity {

    SharedPreferences settings;
  RadioGroup reasons;
  RadioButton selected_reason;
  Button positive, negative;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    ImageView go_back;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reason_to_cancel);

        reasons = findViewById(R.id.reasonsGroup);
        positive=findViewById(R.id.yes);
        negative=findViewById(R.id.no);
        go_back = findViewById(R.id.go_back);

         settings = getSharedPreferences("details",
                Context.MODE_PRIVATE);

        final SharedPreferences.Editor editor = settings.edit();

        final  String rider_id =  settings.getString("enrout_rider_id","");


         positive.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {

                 int selectedReason = reasons.getCheckedRadioButtonId();

                 selected_reason = (RadioButton)findViewById(selectedReason);

                 if (selectedReason!=-1)

                 {

                     db.collection("DISPATCH").document(settings.getString("dispatch_id","").trim()).update( "rejectedBy", FieldValue.arrayUnion(rider_id)).addOnSuccessListener(new OnSuccessListener<Void>() {

                         @Override
                         public void onSuccess(Void aVoid) {

                             db.collection("DISPATCH").document(settings.getString("dispatch_id","").trim()).update("isAccepted", false, "status", "PENDING", "acceptedBy","","actingOn",false,"cancelledBy",rider_id);


                             db.collection("REQUEST").document(settings.getString("the_request_id", "").trim()).update("status", "CANCELLED_R","rejectedBy", FieldValue.arrayUnion(rider_id),"isRiderCancelled",true);

                             JSONObject mapData = new JSONObject();

                             try {
                                 mapData.put("riderId", rider_id);
                                 mapData.put("requestId", settings.getString("the_request_id", "").trim());
                                 mapData.put("reason",selected_reason.getText().toString());

                                 SingletonSocket.getInstance().emit("request::rider::cancelled", mapData);

                             } catch (JSONException e) {
                                 e.printStackTrace();
                             }


                             // db.collection("DRIVERS").document(rider_id.trim()).update("isWorking", false);

                             addCancelledTrip(new Float(Constants.baseFare), settings.getString("array_destinations", ""), settings.getString("array_pickups", ""), new Float(Math.round(Constants.costperkm) * (settings.getFloat("distanceinKm", 0))), rider_id.trim(), 0,
                                     settings.getString("paymentMethod", ""), settings.getString("the_request_id", "").trim(),
                                     settings.getString("customerName", ""), settings.getString("customerPhoneNumber", ""), "CANCELLED(RIDER)", new Float(Math.round(Constants.costpermin) * (settings.getFloat("timeinMin", 0))), new Float(Math.round(settings.getFloat("estimatedCost", 0))),
                                     settings.getString("trip_date", ""), new Float(Math.round(settings.getFloat("waitAmount", 0))), DurationFormatUtils.formatDuration(settings.getLong("totalWaitTime",0), "HH' hr' mm' min'") );


                     /*editor.putBoolean("isFound", false);
                     editor.putBoolean("workingstatus", false);
*/
                             editor.putBoolean("showAgain", true);

                             editor.putBoolean("calculate_wait_amount", false);

                             editor.apply();

                             /*Intent intent3 = new Intent(getBaseContext(), RiderGetLocationAndRequestService.class);

                             startService(intent3);*/

                             Intent intent = new Intent(getApplicationContext(), ProcessTrip.class);

                             intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                             startActivity(intent);
                             new AcTrans.Builder(CancelReason.this).performSlideToLeft();
                         }
                     })

                     .addOnFailureListener(new OnFailureListener() {
                         @Override
                         public void onFailure(@NonNull Exception e) {

                             Toast.makeText(getApplicationContext(),"Trip Cancellation Failed. Check your internet connection", Toast.LENGTH_SHORT).show();

                         }
                     });




                 }

                 else

                 {

                     Toast.makeText(getApplicationContext(),"Select a reason for cancelling this trip", Toast.LENGTH_SHORT).show();

                 }
             }
         });

         go_back.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 finish();
             }
         });

         negative.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {

                 finish();

             }
         });

    }


    @Override
    protected void onResume() {
        super.onResume();


    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new AcTrans.Builder(this).performSlideToRight();
    }

    public void addCancelledTrip(float basefare, String deliveryLoc, String pickuploc, float distanceCost, String driverRef, float earning,
                                 String paymentMethod, String requestRef, String senderName, String senderNumber, String status, float timeCost, float tripCost, String tripDate, float waitTime, String waitDuration)
    {

        TripCalculations calculate = new TripCalculations();

       double couponCost = calculate.promo_percentage(settings.getFloat("couponpercentage",0), tripCost);

       double enroutFee =  calculate.enrout_fee(tripCost,couponCost,earning);

        TripComplete trip_ended = new TripComplete();

        trip_ended.basefare = basefare;
        trip_ended.deliveryLocation = deliveryLoc;
        trip_ended.pickupLocation = pickuploc;
        trip_ended.distanceCost = distanceCost;
        trip_ended.driverReference = driverRef;
        trip_ended.earning = earning;
        trip_ended.paymentMethod = paymentMethod;
        trip_ended.requestReference = requestRef;
        trip_ended.senderName = senderName;
        trip_ended.senderNumber = senderNumber;
        trip_ended.status = status;
        trip_ended.timeCost = timeCost;
        trip_ended.tripCost = tripCost;
        trip_ended.tripDate = tripDate;
        trip_ended. waitTime = waitTime;
        trip_ended.payForMe = settings.getFloat("pay_for_me",0);
        trip_ended.couponAmount = Math.round(couponCost);
        trip_ended.enroutFee = Math.round(enroutFee);
        trip_ended.scheduleId = settings.getString("riderScheduleId","-").trim();
        trip_ended.waitDuration = waitDuration;

        db.collection("TRIPAUDITS").document().set(trip_ended)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        Toast.makeText(getApplicationContext(), "You have cancelled the trip", Toast.LENGTH_LONG).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("Failure", "Failed", e);

                        Toast.makeText(getApplicationContext(), "Kindly check connection", Toast.LENGTH_LONG).show();
                        return;

                    }
                });;


    }




}
