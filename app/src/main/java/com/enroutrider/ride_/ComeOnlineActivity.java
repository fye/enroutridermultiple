package com.enroutrider.ride_;


import android.content.Context;

import android.content.Intent;
import android.content.SharedPreferences;

import android.content.pm.PackageManager;
import android.media.MediaPlayer;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.enroutrider.ride_.model.PostLocationUpdates;
import com.enroutrider.ride_.network.ServiceCheck;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


public class ComeOnlineActivity extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{


    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    private GoogleMap mMap;
    SharedPreferences settings;
    private View view;
    float Latitude = 0;
    float Longitude = 0;
    private static final String TAG = ComeOnlineActivity.class.getSimpleName();
    public MediaPlayer mp;
    Switch Go_online;

    ServiceCheck service = new ServiceCheck();

    Constants constants = new Constants();
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_offline_show, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment)getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        EventBus.getDefault().register(this);

        settings = getActivity().getSharedPreferences("details",
                Context.MODE_PRIVATE);


        getActivity().startService(new Intent(getActivity(), RiderGetLocationAndRequestService.class));

        Go_online = (Switch) view.findViewById(R.id.go_online);

        Go_online.setChecked(false);

        Go_online.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    settings = getContext().getSharedPreferences("details",
                            Context.MODE_PRIVATE);

                    String rider_id =  settings.getString("enrout_rider_id","");

                    db.collection("DRIVERS").document(rider_id.trim()).update("isOnline",true,"lastOnlineDateTime", FieldValue.serverTimestamp());

                    SharedPreferences.Editor editor = settings.edit();
                    editor.putBoolean("onlinestatus",  true);
                    editor.apply();


                    Boolean running = service.isMyServiceRunning(RiderGetLocationAndRequestService.class, getActivity());

                    if(!running){

                        getActivity().startService(new Intent(getActivity(), RiderGetLocationAndRequestService.class));
                    }
                    Intent intent = new Intent(getContext(), MainActivity.class);

                    startActivity(intent);

                    getActivity().finish();

                }
            }
        });


        return view;

    }


    @Override
    public void onStop() {
        super.onStop();

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if(mGoogleApiClient!=null && mGoogleApiClient.isConnected()){
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();

        }

    }

    @Override
    public void onDestroyView ()
    {
        super.onDestroyView();
        try{
            SupportMapFragment fragment = ((SupportMapFragment) getFragmentManager().findFragmentById(R.id.map));
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.remove(fragment);
            ft.commit();
        }catch(Exception e){
        }
        EventBus.getDefault().unregister(this);



    }
    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy");

        super.onDestroy();
        if (mp != null) {
            if (mp.isPlaying()) {
                mp.stop();
            }
        }
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }

        EventBus.getDefault().unregister(this);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        buildGoogleApiClient();
        mMap.setMyLocationEnabled(true);

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {

                if (mMap == null) {
                    return;
                }

            }
        });
    }

    protected synchronized void buildGoogleApiClient(){
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .enableAutoManage(getActivity(),4,
                        this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();

    }

    @Subscribe
    public void locationsReceived(PostLocationUpdates locations){

        Latitude = locations.latitude;
        Longitude = locations.longitude;


        if(getContext()!=null){


            LatLng latLng = new LatLng(Latitude,Longitude);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15));


        }

    }
}