package com.enroutrider.ride_.model;


import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.enroutrider.ride_.R;

import java.util.ArrayList;
import java.util.List;

public class CustomTripAdapter extends ArrayAdapter<TripList> implements Filterable{

    private List<TripList> PlaceList = new ArrayList<TripList>();
    private List<TripList> FilteredArrayNames = new ArrayList<TripList>();
private  LayoutInflater inflater;
    static class TripViewHolder {

        TextView pickupLocation, deliveryLocation, tripStatus, tripDate, tripCost;

    }

    public CustomTripAdapter(List<TripList>data,Context context, int textViewResourceId) {
        super(context, textViewResourceId);

        PlaceList=data;
       inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        FilteredArrayNames = new ArrayList(PlaceList);
    }

    @Override
    public void add(TripList object) {
        FilteredArrayNames.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return this.FilteredArrayNames.size();
    }

    @Override
    public TripList getItem(int index) {
        return this.FilteredArrayNames.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        TripViewHolder viewHolder;
        if (row == null) {

            row = inflater.inflate(R.layout.activity_ride_history, parent, false);
            viewHolder = new TripViewHolder();



            viewHolder.pickupLocation = (TextView) row.findViewById(R.id.pickuplocation);
            viewHolder.deliveryLocation = (TextView) row.findViewById(R.id.deliverylocation);
            viewHolder.tripStatus = (TextView) row.findViewById(R.id.status);
            viewHolder.tripDate = (TextView) row.findViewById(R.id.requestdatetime);
            viewHolder.tripCost = (TextView) row.findViewById(R.id.cost);


            row.setTag(viewHolder);
        } else {
            viewHolder = (TripViewHolder)row.getTag();
        }

        TripList the_trips = getItem(position);

        viewHolder.tripDate.setText(the_trips.getTripDate());
        viewHolder.tripCost.setText(""+the_trips.getTripCost());
        viewHolder.pickupLocation.setText(the_trips.getPickupLocation());
        viewHolder.deliveryLocation.setText(the_trips.getDeliveryLocation());
        viewHolder.tripStatus.setText(the_trips.getTripstatus());



        return row;
    }



    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {


                if (results.count == 0) {
                    //Make list invisible
                    //Make text view visible
                    FilteredArrayNames.clear();
                    notifyDataSetInvalidated();
                } else {
                    FilteredArrayNames = (List<TripList>)results.values;
                    notifyDataSetChanged();
                }

            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();


                constraint = constraint.toString().toLowerCase();


                if (TextUtils.isEmpty(constraint)) {
                    results.count = PlaceList.size();
                    results.values = new ArrayList(PlaceList);
                }

                else{

                    List<TripList> resultList = new ArrayList<TripList>();

                for (TripList dataNames:PlaceList) {
                   // TripList dataNames = PlaceList.get(i);


                    if (dataNames.tripDate.toLowerCase().contains(constraint.toString()) ||
                            dataNames.pickupLocation.toLowerCase().contains(constraint.toString()) ||
                            dataNames.deliveryLocation.toLowerCase().contains(constraint.toString()) ||
                            dataNames.tripstatus.toLowerCase().contains(constraint.toString())||
                            ((Number)dataNames.tripCost).toString().contains(constraint.toString())
                            ) {
                       resultList.add(dataNames);
                    }
                }

                results.count = resultList.size();
                results.values = resultList;
                Log.e("VALUES", results.values.toString());
            }
                return results;
            }
        };

        return filter;
    }


}