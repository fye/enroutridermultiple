package com.enroutrider.ride_.model;

/**
 * Created by Fortress on 6/9/2019.
 */

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.firestore.DocumentReference;

import java.util.ArrayList;
import java.util.List;

public class DestinationView {

    public String destinationId;


    public boolean isDelivered;
    public String place;
    public Pos pos;
    public String recipientName;
    public String recipientNumber;

    public LatLng latLng;

    public List<DocumentReference> pickups = new ArrayList<>();


}