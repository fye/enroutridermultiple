package com.enroutrider.ride_;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.kaopiz.kprogresshud.KProgressHUD;

import id.voela.actrans.AcTrans;

public class RiderTripDetailActivity extends AppCompatActivity {

    TextView tdistance,ttime,cost,pickuplocname,deliverlocname,requesteddatetime,customername,earnings,payment_Method, basefare_, status, pfm,
    waitTime_, waitDurationView;
    KProgressHUD dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rider_trip_detail);


        pickuplocname = (TextView)findViewById(R.id.pickuplocationname);

        deliverlocname = (TextView)findViewById(R.id.droplocationname);

        requesteddatetime = (TextView)findViewById(R.id.requestdatetime);

        waitDurationView = findViewById(R.id.wait_duration);

        customername = (TextView)findViewById(R.id.customername);

        earnings = (TextView)findViewById(R.id.earning);

        pfm = findViewById(R.id.pfm);

        cost = (TextView)findViewById(R.id.cost);

        ttime = (TextView)findViewById(R.id.time);

        tdistance = (TextView)findViewById(R.id.distance);

        payment_Method = (TextView)findViewById(R.id.payment_method);

        basefare_ = (TextView)findViewById(R.id.base_fare);

        waitTime_ = (TextView)findViewById(R.id.wait_time);

        status = (TextView)findViewById(R.id.status);

        String pickupLocation = getIntent().getStringExtra("pickupLocation");
        String deliveryLocation = getIntent().getStringExtra("deliverylocation");
        double distanceCost = getIntent().getDoubleExtra("distanceCost",0);
        double timeCost = getIntent().getDoubleExtra("timeCost",0);
        String tripDate = getIntent().getStringExtra("tripDate");
        String CustomerName = getIntent().getStringExtra("senderName");
        double basefare = getIntent().getDoubleExtra("basefare",0);
        double earning = getIntent().getDoubleExtra("earning",0);
        String paymentMethod = getIntent().getStringExtra("paymentMethod");
        double tripCost = getIntent().getDoubleExtra("tripCost",0);
        double waitTime = getIntent().getDoubleExtra("waitTime",0);
        String tripstatus = getIntent().getStringExtra("tripStatus");

        double payForme = getIntent().getDoubleExtra("payforMe",0);

        double finalCost = getIntent().getDoubleExtra("finalCost",0);

        String waitDuration = getIntent().getStringExtra("waitDuration_Span");

        pickuplocname.setText(pickupLocation);
        deliverlocname.setText(deliveryLocation);
        requesteddatetime.setText(tripDate);
        customername.setText(CustomerName);
        earnings.setText(""+earning);
        cost.setText(""+finalCost);
        ttime.setText(""+timeCost);
        tdistance.setText(""+distanceCost);
        payment_Method.setText(paymentMethod);
        basefare_.setText(""+basefare);
        waitTime_.setText(""+waitTime);
        status.setText(tripstatus);
        waitDurationView.setText(waitDuration);
        pfm.setText(""+payForme);



    }

    @Override
    public void onStop() {
        super.onStop();

        dialogdismiss();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new AcTrans.Builder(this).performSlideToRight();
    }


    @Override
    public void onPause() {
        super.onPause();

        dialogdismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        dialogdismiss();
    }

    public void dialogdismiss(){
        if (dialog != null) {
            dialog.dismiss();
        }
    }

}
