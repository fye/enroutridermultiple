package com.enroutrider.ride_;

import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;

public class SendMailTask extends AsyncTask {
		SendMailTask sample;


	private Activity Send_mailActivity;

	public SendMailTask(Activity activity) {
		Send_mailActivity = activity;

	}
	KProgressHUD statusDialog;

	protected void onPreExecute() {
		statusDialog = KProgressHUD.create(Send_mailActivity)
				.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
				.setBackgroundColor(Color.TRANSPARENT)
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f)
				.show();
	}

	@Override
	protected Object doInBackground(Object... args) {
		try {
			Log.i("SendMailTask", "About to instantiate GMail...");
			publishProgress("Processing input....");
			GMail androidEmail = new GMail(args[0].toString(),
					args[1].toString(),args[2].toString(), args[3].toString(),
					args[4].toString());
			publishProgress("Preparing mail message....");
			androidEmail.createEmailMessage();
			publishProgress("Sending email....");
			androidEmail.sendEmail();
			publishProgress("Email Sent.");
			Log.i("SendMailTask", "Mail Sent.");


		} catch (Exception e) {
			publishProgress(e.getMessage());
			Log.e("SendMailTask", e.getMessage(), e);
			Toast.makeText(Send_mailActivity.getApplicationContext(),"Error Sending Email..Kindly check Internet Connection",Toast.LENGTH_LONG).show();
			if(statusDialog!=null) {
				statusDialog.dismiss();
				statusDialog=null;
			}
				}
		return null;
	}



	@Override
	public void onProgressUpdate(Object... values) {
		statusDialog.setLabel(values[0].toString());

	}

	@Override
	public void onPostExecute(Object result) {
		if(statusDialog!=null) {
			//Toast.makeText(Send_mailActivity.getApplicationContext(),"Email Sent",Toast.LENGTH_SHORT).show();
			statusDialog.dismiss();
			statusDialog=null;
		}
	}

	@Override
	public void  onCancelled(){
		if(statusDialog!=null) {
			statusDialog.dismiss();
			statusDialog=null;
		}
		super.onCancelled();
	}


}
