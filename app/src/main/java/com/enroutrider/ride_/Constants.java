package com.enroutrider.ride_;


import android.location.Location;

import com.fonfon.geohash.GeoHash;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.Timestamp;

import java.util.Map;

/**
 * Created by David on 19/08/2017.
 */

public class Constants {

    public static String authEmail = "enrout.riders@gmail.com";

    public static String authPassword = "Qliqtech@1234444$";

    public static int REQUEST_CHECK_SETTINGS = 99;

    public String PaymentApiToken = "nbDnEmSA8puWsTNSC4NZYgR42uWQdwBwbEKkVbO8";

    public static String riderId;
    public static String riderName;
    public static String riderNumber;
    public static boolean useCourierBox;
    public static double riderPercentage ;
    public static double baseFare;
    public static double cancelPenalty;
    public static double costperkm;
    public static double costpermin;
    public static double acceptanceRadius;
    public static double waitPenalty;
    public static String imageURL;
    public static String imagedownloadUrl;
    public static String deviceID;


    public  static Map<String, Object> map;
    public static int range;

    public static int tripView;

    public static String officeLine;

    public static String FAQ;

    public static String plateNumber;

   public static String getGeoHash(LatLng latLng){

        Location location = new Location("geohash");
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);

        GeoHash hash = GeoHash.fromLocation(location, 9);
        return hash.toString();
    }

    public static final String PHONE_AUT_TYPE = "PHONE_AUT_TYPE";
    public static final String VERIFICATION_ID = "VERIFICATION_ID";
    public static final String USER_DISPLAY_NAME = "USER_DISPLAY_NAME";
    public static final String PHONE_AUTH_PHONE_NUMBER = "PHONE_AUTH_PHONE_NUMBER";
    public static final String USER = "USER";
    public static final String TAG = "Tag";
}
