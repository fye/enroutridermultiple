package com.enroutrider.ride_;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.text.DecimalFormat;

import de.hdodenhof.circleimageview.CircleImageView;
import id.voela.actrans.AcTrans;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    KProgressHUD dialog;
    Fragment fragment = null;
    SharedPreferences settings1;
    TextView namedrawer, rating;
    NavigationView nav_view;
    String the_number;
    CircleImageView rider_image;
    Constants constants = new Constants();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer_side);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        overridePendingTransition(0, R.anim.abc_fade_in);


        toolbar.setBackgroundColor(Color.TRANSPARENT);
        toolbar.setTitle("");
        // Status bar :: Transparent
        Window window = this.getWindow();

        nav_view = findViewById(R.id.nav_view);

        View header = nav_view.getHeaderView(0);

        namedrawer = (TextView) header.findViewById(R.id.namedrawer);
        rider_image = header.findViewById(R.id.riderImg);
        rating = (TextView)header.findViewById(R.id.rating);

         settings1 = getSharedPreferences("details",
                Context.MODE_PRIVATE);


        DecimalFormat df = new DecimalFormat("0.00");
        namedrawer.setText(settings1.getString("rider_fullname", ""));
        rating.setText(""+df.format(settings1.getFloat("rating",0)));

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.loadImage(Constants.imagedownloadUrl, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                if (loadedImage != null){
                    rider_image.setImageBitmap(loadedImage);
                }
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }

        setSupportActionBar(toolbar);


        if(isLocationEnabled(MainActivity.this)) {


            settings1 = getSharedPreferences("details",
                    Context.MODE_PRIVATE);

            String paymentconfirmed = settings1.getString("paymentstatus","-");



                loggon();


        }
        else{
            Toast.makeText(MainActivity.this, "Please turn on location", Toast.LENGTH_SHORT).show();

        }




      settings1 = getSharedPreferences("details",
                Context.MODE_PRIVATE);

        String promo = settings1.getString("promosall", "");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            new AcTrans.Builder(this).performSlideToRight();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.drawer_side, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        displaySelectedScreen(item.getItemId());

        return true;
    }



    private void displaySelectedScreen(int itemId) {


        switch (itemId) {

            case R.id.nav_account:
                Intent t = new Intent(getApplicationContext(), AccountActivity.class);
                startActivity(t);
                new AcTrans.Builder(this).performSlideToTop();
                break;

            case R.id.my_trips_option:
                Intent i = new Intent(getApplicationContext(), RiderHistoryActivity.class);
                startActivity(i);
                new AcTrans.Builder(this).performSlideToTop();
                break;

            case R.id.delayedTrip:
                Intent v = new Intent(getApplicationContext(), DelayedTrips.class);
                startActivity(v);
                new AcTrans.Builder(this).performSlideToTop();
                break;


            case R.id.nav_incentive:

                Intent y = new Intent(getApplicationContext(), IncentiveActivity.class);
                startActivity(y);
                new AcTrans.Builder(this).performSlideToTop();

                break;

            case R.id.nav_earnings:

                Intent p = new Intent(getApplicationContext(), FinancialsActivity.class);
                startActivity(p);
                new AcTrans.Builder(this).performSlideToTop();

                break;


            case R.id.nav_help:

                Intent k = new Intent(Intent.ACTION_VIEW,Uri.parse(Constants.FAQ));
               // k.addCategory("android.intent.category.BROWSABLE");
                //k.setComponent(null);
               // k.setSelector(null);
                startActivity(k);
                new AcTrans.Builder(this).performSlideToTop();

                break;

            case R.id.nav_call_office:

                callPhoneNumber(Constants.officeLine);

                break;

            case R.id.nav_pfm_wallet:

                Intent r = new Intent(getApplicationContext(), PFMActivity.class);
                startActivity(r);
                new AcTrans.Builder(this).performSlideToTop();
                break;

        }


        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }




    public void loggon(){


        dialog = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(Color.TRANSPARENT)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        FragmentManager fragmentManager =getSupportFragmentManager();

        settings1 = getSharedPreferences("details",
                Context.MODE_PRIVATE);

      boolean onlinestatus =  settings1.getBoolean("onlinestatus",false);

        if (onlinestatus==false) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new ComeOnlineActivity()).commit();

            dialogdismiss();

        } else {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new DriverMapActivity()).commit();

            dialogdismiss();
        }


                }



    @Override
    public void onStop() {
        super.onStop();

        dialogdismiss();
    }


    @Override
    public void onPause() {
        super.onPause();

        dialogdismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        dialogdismiss();
    }

    public void dialogdismiss(){
        if (dialog != null) {
            dialog.dismiss();
        }
    }


    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }


    }




    public void callPhoneNumber(String number)
    {

        the_number = number;

        Intent callIntent = new Intent(Intent.ACTION_DIAL,Uri.parse("tel:" + number));

        startActivity(callIntent);
        new AcTrans.Builder(this).performSlideToTop();
    }

}
