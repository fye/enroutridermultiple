package com.enroutrider.ride_;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Hardik on 23-Mar-17.
 */

public class HelpFAQActivity extends Activity {

    TextView unprofessional_text,package_text,pick_text,cancelation_text,rider_track_text,contact_rider_text,recognize_rider_text,location_text,complint_text;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_faq);

        unprofessional_text = (TextView) findViewById(R.id.unprofessional_id);
        package_text = (TextView) findViewById(R.id.package_id);
        pick_text = (TextView) findViewById(R.id.pick_item_id);
        cancelation_text = (TextView) findViewById(R.id.cancellation_id);
        rider_track_text = (TextView) findViewById(R.id.track_rider_id);
        contact_rider_text = (TextView) findViewById(R.id.contact_rider_id);
        recognize_rider_text = (TextView) findViewById(R.id.recognise_rider_id);
        location_text = (TextView) findViewById(R.id.office_location_id);
        complint_text = (TextView) findViewById(R.id.complaint_id);




        // hide until its title is clicked
        unprofessional_text.setVisibility(View.GONE);
        package_text.setVisibility(View.GONE);
        pick_text.setVisibility(View.GONE);
        cancelation_text.setVisibility(View.GONE);
        rider_track_text.setVisibility(View.GONE);
        contact_rider_text.setVisibility(View.GONE);
        recognize_rider_text.setVisibility(View.GONE);
        location_text.setVisibility(View.GONE);
        complint_text.setVisibility(View.GONE);

    }

    public void toggle_contents_unprov(View v) {

        if (unprofessional_text.isShown()) {
            ScrollAnime.slide_up(this, unprofessional_text);
            unprofessional_text.setVisibility(View.GONE);
        } else {
            unprofessional_text.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, unprofessional_text);
        }

    }

    public void toggle_contents_package(View v) {


        if (package_text.isShown()) {
            ScrollAnime.slide_up(this,package_text);
            package_text.setVisibility(View.GONE);
        } else {
            package_text.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, package_text);
        }

    }

    public void toggle_contents_pick(View v) {

        if (pick_text.isShown()) {
            ScrollAnime.slide_up(this, pick_text);
            pick_text.setVisibility(View.GONE);
        } else {
            pick_text.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this,pick_text);
        }

    }

    public void toggle_contents_cancel(View v) {

        if (cancelation_text.isShown()) {
            ScrollAnime.slide_up(this, cancelation_text);
            cancelation_text.setVisibility(View.GONE);
        } else {
            cancelation_text.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, cancelation_text);
        }

    }

    public void toggle_contents_track_rider(View v) {

        if (rider_track_text.isShown()) {
            ScrollAnime.slide_up(this,rider_track_text);
            rider_track_text.setVisibility(View.GONE);
        } else {
            rider_track_text.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, rider_track_text);
        }

    }

    public void toggle_contents_contact_rider(View v) {


        if (contact_rider_text.isShown()) {
            ScrollAnime.slide_up(this, contact_rider_text);
            contact_rider_text.setVisibility(View.GONE);
        } else {
            contact_rider_text.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, contact_rider_text);
        }

    }

    public void toggle_contents_recog_rider(View v) {


        if (recognize_rider_text.isShown()) {
            ScrollAnime.slide_up(this, recognize_rider_text);
            recognize_rider_text.setVisibility(View.GONE);
        } else {
            recognize_rider_text.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, recognize_rider_text);
        }

    }

    public void toggle_contents_location(View v) {

        if (location_text.isShown()) {
            ScrollAnime.slide_up(this, location_text);
            location_text.setVisibility(View.GONE);
        } else {
            location_text.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this,location_text);
        }

    }

    public void toggle_contents_complaint(View v) {

        if (complint_text.isShown()) {
            ScrollAnime.slide_up(this, complint_text);
            complint_text.setVisibility(View.GONE);
        } else {
            complint_text.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, complint_text);
        }

    }



}
