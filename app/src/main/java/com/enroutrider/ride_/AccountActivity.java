package com.enroutrider.ride_;

import android.app.ActivityManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;

import android.view.View;

import android.widget.Button;

import android.widget.TextView;

import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;


import de.hdodenhof.circleimageview.CircleImageView;
import id.voela.actrans.AcTrans;


public class AccountActivity extends AppCompatActivity {

   private TextView name,phone,email, date_of_birth;
   TextView cannot_edit_as_rider;
    private Button logoff;
    CircleImageView rider_Img;
    KProgressHUD pd;
    SharedPreferences settings;



    FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);

  
      
     //   Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
      //  setSupportActionBar(toolbar);

        name = (TextView) findViewById(R.id.name);

        cannot_edit_as_rider = (TextView) findViewById(R.id.cannot_edit_as_rider);

        phone = (TextView) findViewById(R.id.phone);

        email = (TextView) findViewById(R.id.id_email);

        rider_Img = findViewById(R.id.riderImg);

        date_of_birth= (TextView) findViewById(R.id.date_of_birth) ;

        logoff = (Button) findViewById(R.id.logoff);



        displayuserdetail();


        logoff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                android.app.AlertDialog.Builder alert1 = new android.app.AlertDialog.Builder(AccountActivity.this);


                //    final EditText edittext = new EditText(SeeBillActivity.this);
                alert1.setMessage("Are you sure you want to Logout of Enrout.\n " +
                        "You will need verification to log back in");
                alert1.setTitle("Log Out of Enrout ");
                alert1.setCancelable(false);

                alert1.setPositiveButton("YES", new DialogInterface.OnClickListener() {


                    public void onClick(DialogInterface dialog, int whichButton) {





                        logoff();


                    }
                });

                alert1.setNegativeButton("No ", new DialogInterface.OnClickListener() {


                    public void onClick(DialogInterface dialog, int whichButton) {

                        dialog.dismiss();
                    }
                });

                alert1.show();

            }
        });




    }



    public void displayuserdetail(){

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.loadImage(Constants.imagedownloadUrl, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                if (loadedImage != null){
                    rider_Img.setImageBitmap(loadedImage);
                }
            }
        });


         settings = getSharedPreferences("details",
                Context.MODE_PRIVATE);

        name.setText(settings.getString("rider_fullname", ""));
        email.setText(settings.getString("rider_email", ""));
        phone.setText(settings.getString("phoneNumber", ""));
        date_of_birth.setText(settings.getString("rider_dob", ""));


    }




    public void logoff(){

        pd = KProgressHUD.create(AccountActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(Color.TRANSPARENT)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();



        settings = getSharedPreferences("details",
                Context.MODE_PRIVATE);

        String rider_id =  settings.getString("enrout_rider_id","");


        db.collection("DRIVERS").document(rider_id.trim()).update("isOnline",false,"lastOfflineDatime", FieldValue.serverTimestamp());

        SharedPreferences.Editor editor = settings.edit();

        editor.putBoolean("onlinestatus",  false);

        editor.putString("verifyemail", "none");

        editor.apply();

        if (isMyServiceRunning(RiderGetLocationAndRequestService.class)) {
            stopService(new Intent(this, RiderGetLocationAndRequestService.class));
        }


       // FirebaseAuth.getInstance().signOut();

        dialogdismiss();

        Intent intent = new Intent(AccountActivity.this, LandingActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
        new AcTrans.Builder(AccountActivity.this).performSlideToLeft();
        finish();


    }

  private boolean isMyServiceRunning(Class <?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);

        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {

            if(serviceClass.getName().equals(service.service.getClassName())) {

                Log.i("Service Running", "isMyServiceRunning? " + true + "");
                return true;
            }
        }

        Log.i("Service Running", "isMyServiceRunning? " + false + "");
        return false;
    }

    @Override
    public void onStop() {
        super.onStop();

        dialogdismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new AcTrans.Builder(this).performSlideToRight();
    }


    @Override
    public void onPause() {
        super.onPause();

        dialogdismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        dialogdismiss();
    }

    public void dialogdismiss(){
        if (pd != null) {
            pd.dismiss();
        }
    }

}
