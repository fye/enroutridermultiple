package com.enroutrider.ride_.model;



import com.google.firebase.database.Exclude;
import com.google.firebase.database.ServerValue;

public class Chat {

    public Chat() {
    }

    public Chat(String messageText, String riderId, String customerId) {
        this.messageText = messageText;
        this.riderId = riderId;
        this.customerId = customerId;

        createdTimestamp = ServerValue.TIMESTAMP;
    }

    public String messageText;
    public String riderId;
    public String customerId;
    public Object createdTimestamp;
    public String sentBy = "rider";

    //please navigate back to the class we are working on

    @Exclude
    public long getCreatedTimestampLong(){
        return (long)createdTimestamp;
    }
}