package com.enroutrider.ride_;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Hardik on 23-Mar-17.
 */

public class Help_guide extends Activity {

    TextView unprofessional_text,package_text,pick_text,cancelation_text,rider_track_text,contact_rider_text,
            recognize_rider_text,location_text,complint_text,
            res_cancel,est_time,opt_pay,id_groc,pre_fin,id_promo,id_charge_no_sense,
            id_det_rating,id_score_rider,id_score_cust,id_tip_me,id_something_wrong,id_forget_trip,id_toll_fees;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_guide);

        unprofessional_text = (TextView) findViewById(R.id.request_pick_up);
        package_text = (TextView) findViewById(R.id.multiple_id);
        pick_text = (TextView) findViewById(R.id.request_delivery_id);
        cancelation_text = (TextView) findViewById(R.id.more_than_one_id);
      ///  rider_track_text = (TextView) findViewById(R.id.track_rider_id);
        contact_rider_text = (TextView) findViewById(R.id.schedule_pickup_id);
        recognize_rider_text = (TextView) findViewById(R.id.specific_rider_id);
        location_text = (TextView) findViewById(R.id.not_around_id);
        complint_text = (TextView) findViewById(R.id.cancel_by_user_id);

        res_cancel = (TextView) findViewById(R.id.res_cancel_trip);
        est_time = (TextView) findViewById(R.id.est_time);
        opt_pay = (TextView) findViewById(R.id.opt_pay);
        id_groc = (TextView) findViewById(R.id.id_grocery);
        pre_fin = (TextView) findViewById(R.id.id_pre_fin);
        id_promo = (TextView) findViewById(R.id.id_promo);
        id_charge_no_sense = (TextView) findViewById(R.id.id_charge_no_sense);
        id_det_rating = (TextView) findViewById(R.id.id_det_rating);
        id_score_rider = (TextView) findViewById(R.id.id_score_rider);
        id_score_cust = (TextView) findViewById(R.id.id_score_cust);
        id_tip_me = (TextView) findViewById(R.id.id_tip_me);
        id_something_wrong = (TextView) findViewById(R.id.id_something_wrong);
        id_forget_trip = (TextView) findViewById(R.id.id_forget_trip_content);
       id_toll_fees = (TextView) findViewById(R.id.id_toll_fees);







        // hide until its title is clicked
        unprofessional_text.setVisibility(View.GONE);
        package_text.setVisibility(View.GONE);
        pick_text.setVisibility(View.GONE);

        res_cancel.setVisibility(View.GONE);
        est_time.setVisibility(View.GONE);
        opt_pay.setVisibility(View.GONE);
        id_groc.setVisibility(View.GONE);
        pre_fin.setVisibility(View.GONE);
        id_promo.setVisibility(View.GONE);
        id_charge_no_sense.setVisibility(View.GONE);
        id_det_rating.setVisibility(View.GONE);
        id_score_rider.setVisibility(View.GONE);
        id_score_cust.setVisibility(View.GONE);
        id_tip_me.setVisibility(View.GONE);
       id_something_wrong.setVisibility(View.GONE);
        id_forget_trip.setVisibility(View.GONE);
        id_toll_fees.setVisibility(View.GONE);

        cancelation_text.setVisibility(View.GONE);
        //rider_track_text.setVisibility(View.GONE);
        contact_rider_text.setVisibility(View.GONE);
        recognize_rider_text.setVisibility(View.GONE);
        location_text.setVisibility(View.GONE);
        complint_text.setVisibility(View.GONE);

    }

    public void toggle_contents_unprov(View v) {

        if (unprofessional_text.isShown()) {
            ScrollAnime.slide_up(this, unprofessional_text);
            unprofessional_text.setVisibility(View.GONE);
        } else {
            unprofessional_text.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, unprofessional_text);
        }

    }


    public void res_cancel(View v) {

        if (res_cancel.isShown()) {
            ScrollAnime.slide_up(this, res_cancel);
            res_cancel.setVisibility(View.GONE);
        } else {
            res_cancel.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, res_cancel);
        }

    }


    public void eta(View v) {

        if (est_time.isShown()) {
            ScrollAnime.slide_up(this, est_time);
            est_time.setVisibility(View.GONE);
        } else {
            est_time.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, est_time);
        }

    }

    public void payment_tin(View v) {

        if (opt_pay.isShown()) {
            ScrollAnime.slide_up(this,opt_pay);
            opt_pay.setVisibility(View.GONE);
        } else {
            opt_pay.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, opt_pay);
        }

    }
    public void groc_tin(View v) {

        if (id_groc.isShown()) {
            ScrollAnime.slide_up(this, id_groc);
            id_groc.setVisibility(View.GONE);
        } else {
            id_groc.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, id_groc);
        }

    }
    public void pre_finance(View v) {

        if (pre_fin.isShown()) {
            ScrollAnime.slide_up(this,pre_fin);
            pre_fin.setVisibility(View.GONE);
        } else {
            pre_fin.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, pre_fin);
        }

    }
    public void promo_code(View v) {

        if (id_promo.isShown()) {
            ScrollAnime.slide_up(this, id_promo);
            id_promo.setVisibility(View.GONE);
        } else {
            id_promo.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, id_promo);
        }

    }
    public void charge_no_sense(View v) {

        if (id_charge_no_sense.isShown()) {
            ScrollAnime.slide_up(this, id_charge_no_sense);
            id_charge_no_sense.setVisibility(View.GONE);
        } else {
            id_charge_no_sense.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, id_charge_no_sense);
        }

    }
    public void det_rating(View v) {

        if (id_det_rating.isShown()) {
            ScrollAnime.slide_up(this,id_det_rating);
            id_det_rating.setVisibility(View.GONE);
        } else {
            id_det_rating.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, id_det_rating);
        }

    }
    public void score_rider(View v) {

        if (id_score_rider.isShown()) {
            ScrollAnime.slide_up(this,id_score_rider);
            id_score_rider.setVisibility(View.GONE);
        } else {
            id_score_rider.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this,id_score_rider);
        }

    }
    public void score_cust(View v) {

        if (id_score_cust.isShown()) {
            ScrollAnime.slide_up(this, id_score_cust);
            id_score_cust.setVisibility(View.GONE);
        } else {
            id_score_cust.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this,id_score_cust);
        }

    }
    public void tip_me(View v) {

        if (id_tip_me.isShown()) {
            ScrollAnime.slide_up(this, id_tip_me);
            id_tip_me.setVisibility(View.GONE);
        } else {
            id_tip_me.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this,id_tip_me);
        }

    }
    public void something_wrong(View v) {

        if (id_something_wrong.isShown()) {
            ScrollAnime.slide_up(this, id_something_wrong);
            id_something_wrong.setVisibility(View.GONE);
        } else {
            id_something_wrong.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, id_something_wrong);
        }

    }
    public void forget_trip_content(View v) {

        if (id_forget_trip.isShown()) {
            ScrollAnime.slide_up(this, id_forget_trip);
            id_forget_trip.setVisibility(View.GONE);
        } else {
            id_forget_trip.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this,id_forget_trip);
        }

    }
    public void toll_fees(View v) {

        if (id_toll_fees.isShown()) {
            ScrollAnime.slide_up(this,id_toll_fees);
            id_toll_fees.setVisibility(View.GONE);
        } else {
            id_toll_fees.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this,id_toll_fees);
        }

    }











    public void toggle_contents_package(View v) {


        if (package_text.isShown()) {
            ScrollAnime.slide_up(this,package_text);
            package_text.setVisibility(View.GONE);
        } else {
            package_text.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, package_text);
        }

    }

    public void toggle_contents_pick(View v) {

        if (pick_text.isShown()) {
            ScrollAnime.slide_up(this, pick_text);
            pick_text.setVisibility(View.GONE);
        } else {
            pick_text.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this,pick_text);
        }

    }

    public void toggle_contents_cancel(View v) {

        if (cancelation_text.isShown()) {
            ScrollAnime.slide_up(this, cancelation_text);
            cancelation_text.setVisibility(View.GONE);
        } else {
            cancelation_text.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, cancelation_text);
        }

    }

    public void toggle_contents_track_rider(View v) {

        if (rider_track_text.isShown()) {
            ScrollAnime.slide_up(this,rider_track_text);
            rider_track_text.setVisibility(View.GONE);
        } else {
            rider_track_text.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, rider_track_text);
        }

    }

    public void toggle_contents_contact_rider(View v) {


        if (contact_rider_text.isShown()) {
            ScrollAnime.slide_up(this, contact_rider_text);
            contact_rider_text.setVisibility(View.GONE);
        } else {
            contact_rider_text.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, contact_rider_text);
        }

    }

    public void toggle_contents_recog_rider(View v) {


        if (recognize_rider_text.isShown()) {
            ScrollAnime.slide_up(this, recognize_rider_text);
            recognize_rider_text.setVisibility(View.GONE);
        } else {
            recognize_rider_text.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, recognize_rider_text);
        }

    }

    public void toggle_contents_location(View v) {

        if (location_text.isShown()) {
            ScrollAnime.slide_up(this, location_text);
            location_text.setVisibility(View.GONE);
        } else {
            location_text.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this,location_text);
        }

    }

    public void toggle_contents_complaint(View v) {

        if (complint_text.isShown()) {
            ScrollAnime.slide_up(this, complint_text);
            complint_text.setVisibility(View.GONE);
        } else {
            complint_text.setVisibility(View.VISIBLE);
            ScrollAnime.slide_down(this, complint_text);
        }

    }



}
