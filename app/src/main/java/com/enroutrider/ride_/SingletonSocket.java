package com.enroutrider.ride_;

import androidx.multidex.MultiDexApplication;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.io.Serializable;
import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.engineio.client.transports.Polling;
import io.socket.engineio.client.transports.WebSocket;

public class SingletonSocket extends MultiDexApplication implements Serializable {
    public static volatile Socket mSocket;


    public SingletonSocket(){

        //Prevent form the reflection api.
        if (mSocket != null){
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisk(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .diskCacheSize(100 * 1024 * 1024).build();

        ImageLoader.getInstance().init(config);


    }

    private  static void initSocket(){

        try {
            String ridername= Constants.riderName;
            String rider_id =  Constants.riderId;
            String rider_number = Constants.riderNumber;
            String plateNumber = Constants.plateNumber;
            boolean useCourierBox = Constants.useCourierBox;

            IO.Options options = new IO.Options();
            options.query = "riderId="+rider_id + "&riderName=" + ridername + "&type=" + "DRIVER" +"&riderPhoneNumber="+ rider_number
                    +"&riderPlateNumber=" + plateNumber+ "&useCourierBox"+useCourierBox;
            options.transports = new String[] { WebSocket.NAME, Polling.NAME };
          //  mSocket = IO.socket("https://ec15951f.ngrok.io",options); // we'll change the url on production
           mSocket = IO.socket("https://ws.realtime.enroutdelivery.com",options); // we'll change the url on production

            if(!mSocket.connected())
                mSocket.connect();

        } catch (URISyntaxException e) {}

    }


    public static Socket getInstance(){

        if(mSocket != null){

            return mSocket;
        }
        else{

            initSocket();
        return mSocket;

        }
    }



}