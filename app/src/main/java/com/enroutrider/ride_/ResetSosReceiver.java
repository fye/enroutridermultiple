package com.enroutrider.ride_;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.firebase.firestore.FirebaseFirestore;

public class ResetSosReceiver extends BroadcastReceiver{

    SharedPreferences settings;
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    public void onReceive(Context context, Intent intent) {

        settings = context.getSharedPreferences("details",
                Context.MODE_PRIVATE);

        final String rider_id =  settings.getString("enrout_rider_id","");

        db.collection("DRIVERS").document(rider_id.trim()).update("SOS", false);

        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("SOS", false);
        editor.apply();

    }
}
