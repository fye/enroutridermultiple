package com.enroutrider.ride_;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kaopiz.kprogresshud.KProgressHUD;

import id.voela.actrans.AcTrans;

public class PFMActivity extends AppCompatActivity {


    KProgressHUD progress_dialog;
ImageView money, no_money;
    TextView transfer_amount, no_money_text;
    Button pay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_express);



        SharedPreferences settings = this.getSharedPreferences("details",
                Context.MODE_PRIVATE);


        transfer_amount = (TextView)findViewById(R.id.transfer_amount);
        no_money_text = findViewById(R.id.no_money_text);

        money = findViewById(R.id.money);
        no_money = findViewById(R.id.no_money);

            pay = findViewById(R.id.payBtn);


        Float payForMeValue = settings.getFloat("withdrawal_pfm",0);

        transfer_amount.setText(""+payForMeValue);

      if (payForMeValue!= 0)

        {
            money.setVisibility(View.VISIBLE);
            no_money.setVisibility(View.GONE);
            no_money_text.setVisibility(View.GONE);
           // pay.setVisibility(View.VISIBLE);
        }


        pay.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

    }
});

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new AcTrans.Builder(this).performSlideToRight();
    }

    @Override
    public void onStop() {
        super.onStop();

        dialogdismiss();
    }


    @Override
    public void onPause() {
        super.onPause();

        dialogdismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        dialogdismiss();
    }

    public void dialogdismiss(){
        if (progress_dialog != null) {
            progress_dialog.dismiss();
        }
    }

}