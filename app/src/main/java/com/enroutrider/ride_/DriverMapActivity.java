package com.enroutrider.ride_;

import android.content.DialogInterface;

import android.net.Uri;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.enroutrider.ride_.model.PostLocationUpdates;
import com.enroutrider.ride_.network.ServiceCheck;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;

import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;
import java.util.Map;


public class DriverMapActivity extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;

    LocationRequest mLocationRequest;
    SharedPreferences settings;
    private View view;
    TextView deliveriesCount, earningsCount;
    private static final String TAG = DriverMapActivity.class.getSimpleName();
    public MediaPlayer mp;
   // RelativeLayout showDelayed;
   float Latitude = 0;
    float Longitude = 0;
    LinearLayout gooffline_view, SOS_Mode;
    private Boolean isLoggingOut = false;
    Switch Gooffline;
    Constants constants = new Constants();
    double finalfare, enroutfee, promo,pfm, tip, fare, total;

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    ServiceCheck service = new ServiceCheck();

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view= inflater.inflate(R.layout.activity_driver_map, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment)getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        EventBus.getDefault().register(this);

        settings = getActivity().getSharedPreferences("details",
                Context.MODE_PRIVATE);

        Gooffline = (Switch) view.findViewById(R.id.gooffline);

        gooffline_view=(LinearLayout)view.findViewById(R.id.go_off_background);

        deliveriesCount = view.findViewById(R.id.deliveriesCount);

       // showDelayed = view.findViewById(R.id.delayedTrip);

        earningsCount = view. findViewById(R.id.earningsCount);

        SOS_Mode = view.findViewById(R.id.SOS_Mode);

        SharedPreferences.Editor editor = settings.edit();

        editor.putBoolean("onlinestatus", true);

        editor.putBoolean("workingstatus", false);

        editor.apply();


        if(settings.getBoolean("SOS",false))
            SOS_Mode.setVisibility(View.VISIBLE);


        versionCheck();


        db.collection("SETTINGS")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {

                        if (queryDocumentSnapshots.getDocuments().isEmpty())
                            return;

                            List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();

                            DocumentSnapshot the_document = documentSnapshots.get(0);

                            Map<String, Object> setting = the_document.getData();

                            String the_timeCode = (String) setting.get("timeCode");

                            editor.putString("timeCode", the_timeCode);
                            editor.apply();

                        getActivity().startService(new Intent(getActivity(), RiderGetLocationAndRequestService.class));
                    }
                });

        Boolean running = service.isMyServiceRunning(RiderGetLocationAndRequestService.class, getActivity());

        if(!running){

            getActivity().startService(new Intent(getActivity(), RiderGetLocationAndRequestService.class));
        }




        getTripCount(settings.getString("enrout_rider_id",""));

        Toast.makeText(getContext(),"You Have Successfully Come Online ",Toast.LENGTH_SHORT).show();

        playSoundOnline(getContext());

        gooffline_view.setVisibility(View.VISIBLE);

        Gooffline.setChecked(true);


        foo(getContext());

        Gooffline.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                final String sessionID = settings.getString("ridersessionID", "");

                if (!isChecked)
                {

                    settings = getContext().getSharedPreferences("details",
                            Context.MODE_PRIVATE);

                    String rider_id =  settings.getString("enrout_rider_id","");

                    db.collection("DRIVERS").document(rider_id.trim()).update("isOnline",false,"lastOfflineDatime", FieldValue.serverTimestamp());

                    SharedPreferences.Editor editor = settings.edit();
                    editor.putBoolean("onlinestatus",  false);
                    editor.commit();

                    Boolean running = service.isMyServiceRunning(RiderGetLocationAndRequestService.class, getActivity());

                    if(!running){

                        getActivity().startService(new Intent(getActivity(), RiderGetLocationAndRequestService.class));
                    }


                    Intent intent = new Intent(getContext(), MainActivity.class);

                    startActivity(intent);

                    getActivity().finish();



                return;
            }
            }
        });

        return view;


    }


    public void foo(Context context) {
        // when you need location
        // if inside activity context = getContext();

        SingleShotLocationProvider.requestSingleUpdate(context,
                new SingleShotLocationProvider.LocationCallback() {

                    @Override public void onNewLocationAvailable(Location location) {
                        Log.d("Location", "my location is " + location.toString());
                        //            Toast.makeText(getContext(), "my Location is" + location.toString(),
                        //                   Toast.LENGTH_SHORT).show();
                    }
                });
    }


    public void getTripCount(String rider_id){

    String SCHEDULEID = settings.getString("riderScheduleId","-").trim();

        SharedPreferences.Editor editor = settings.edit();

        db.collection("TRIPAUDITS")
                .whereEqualTo("driverReference",rider_id.trim())
                .whereEqualTo("status","COMPLETED")
                .whereEqualTo("scheduleId",SCHEDULEID)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful()) {
                            if (task.getResult().isEmpty())
                            {

                                deliveriesCount.setText(""+0);

                                earningsCount.setText(""+0);

                                editor.putFloat("finalFareFinance", 0);
                                editor.putFloat("totalFinance", 0);
                                editor.putFloat("pfmFinance", 0);
                                editor.putFloat("enroutFeeFinance", 0);
                                editor.putFloat("promoFinance", 0);
                                editor.putInt("tripCount", 0);

                                editor.apply();

                            }
                            else{

                                QuerySnapshot querySnapshot = task.getResult();

                                List<DocumentSnapshot> documentSnapshots = querySnapshot.getDocuments();

                                deliveriesCount.setText(""+documentSnapshots.size());

                                editor.putInt("tripCount", documentSnapshots.size());

                                editor.commit();

                                for (DocumentSnapshot  documentSnapshot : documentSnapshots ) {


                                    Map<String, Object> actualData = documentSnapshot.getData();

                                    finalfare += (double) actualData.get("finalCost");
                                    pfm += (double)actualData.get("payForMe");
                                    promo+= (double)actualData.get("couponAmount");
                                    enroutfee+= (double)actualData.get("enroutFee");


                                }

                                total = finalfare + pfm + promo - enroutfee;

                                earningsCount.setText(""+total);

                                editor.putFloat("finalFareFinance", (float)finalfare);
                                editor.putFloat("totalFinance", (float)total);
                                editor.putFloat("pfmFinance", (float)pfm);
                                editor.putFloat("enroutFeeFinance", (float)enroutfee);
                                editor.putFloat("promoFinance", (float)promo);

                                editor.apply();

                            }

                        } else {
                            Log.w("Tag", "Error connecting to server.", task.getException());
                        }

                    }

                });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        settings = getContext().getSharedPreferences("details",
                Context.MODE_PRIVATE);

        if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }


        buildGoogleApiClient();
        mMap.setMyLocationEnabled(true);

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {

                if (mMap == null) {
                    return;
                }

            }
        });
    }

    protected synchronized void buildGoogleApiClient(){
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .enableAutoManage(getActivity(),4,
                        this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();




    }

    @Subscribe
    public void locationsReceived(PostLocationUpdates locations){

        Latitude = locations.latitude;
        Longitude = locations.longitude;

        if(getContext()!=null){


            LatLng latLng = new LatLng(Latitude,Longitude);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

            checkRadius(Latitude, Longitude);



        }

    }

    @Override
    public void onDestroyView ()
    {
        super.onDestroyView();
        try{
            SupportMapFragment fragment = ((SupportMapFragment) getFragmentManager().findFragmentById(R.id.map));
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.remove(fragment);
            ft.commit();
        }catch(Exception e){
        }
        EventBus.getDefault().unregister(this);



    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }



    @Override
    public void onStop() {
        super.onStop();
        if (!isLoggingOut){
            //   disconnectDriver();
        }
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }



    private void checkRadius(float latitude, float longitude){

        float[] distance = new float[2];


        settings = getContext().getSharedPreferences("details",
                Context.MODE_PRIVATE);

        String rider_id =  settings.getString("enrout_rider_id","");

        Circle mapcircle = mMap.addCircle(new CircleOptions()
                .strokeColor(Color.TRANSPARENT)
                .center(new LatLng(5.55602, -0.1969))  // -Accra
                //.center(new LatLng(6.68848, -1.62443))// - Kumasi
                .radius(43000)
        );

        Location.distanceBetween(latitude,
                longitude, mapcircle.getCenter().latitude,
                mapcircle.getCenter().longitude, distance);

        if (distance[0] > mapcircle.getRadius()) {

            db.collection("DRIVERS").document(rider_id.trim()).update("isOnline",false,"lastOfflineDatime", FieldValue.serverTimestamp());

            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("onlinestatus",  false);
            editor.apply();

            Intent i=new Intent(getContext(),DialogActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(i,0);

            getActivity().finish();


        }

    }



    public void versionCheck()
    {
            int verCode = BuildConfig.VERSION_CODE;

            if (settings.getInt("versionNumber",0) > verCode)
            {
                android.app.AlertDialog.Builder alert1 = new android.app.AlertDialog.Builder(getContext());
                alert1.setMessage("Enrout Partner recommends that you update to the latest version");
                alert1.setTitle("Update Enrout Partner");
                alert1.setCancelable(false);

                alert1.setPositiveButton("Update", new DialogInterface.OnClickListener() {


                    public void onClick(DialogInterface dialog, int whichButton) {

                        final String appPackageName = BuildConfig.APPLICATION_ID; // package name of the app
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }

                    }
                });

                alert1.setNegativeButton("Remind Me Later", new DialogInterface.OnClickListener() {


                    public void onClick(DialogInterface dialog, int whichButton) {

                        dialog.dismiss();
                    }
                });

                alert1.show();
            }

    }

    @Override
    public void onPause() {
        super.onPause();
        if(mGoogleApiClient!=null && mGoogleApiClient.isConnected()){
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();

        }
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy");

        super.onDestroy();
        if (mp != null) {
            if (mp.isPlaying()) {
                mp.stop();
            }
        }
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
        EventBus.getDefault().unregister(this);

    }
  public void playSoundOnline(Context context) {
      if (mp != null) {
          if (mp.isPlaying()) {
              mp.stop();
          }
          mp.reset();
      }
      mp = MediaPlayer.create(context, R.raw.online);
      mp.start();
  }

    public void playSoundCancelled(Context context) {
        if (mp != null) {
            if (mp.isPlaying()) {
                mp.stop();
            }
            mp.reset();
        }
        mp = MediaPlayer.create(context, R.raw.short_notify);
        mp.start();
    }

}