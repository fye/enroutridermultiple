package com.enroutrider.ride_;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;
import com.enroutrider.ride_.dbModels.TripCalculations;
import com.enroutrider.ride_.dbModels.TripComplete;
import com.enroutrider.ride_.model.Chat;
import com.enroutrider.ride_.model.DestinationView;
import com.enroutrider.ride_.model.PickupView;
import com.enroutrider.ride_.model.PostLocationUpdates;
import com.enroutrider.ride_.model.PostTimeToService;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldPath;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import static android.content.Intent.ACTION_MAIN;


public class RiderGetLocationAndRequestService extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener  {

    public static boolean isServiceRunning = false;

    public MediaPlayer mp;
    private static final String TAG = RiderGetLocationAndRequestService.class.getSimpleName();
    SharedPreferences settings;
    Gson gson = new Gson();
    int detinationForPickupCounter = 0;
    int pickupsForARequestCounter = 0;
    Timer penaltyTimer, locationTimer;
    Set<String> set = new HashSet<String>();
    String realTime = "calculating...";
    GoogleApiClient mGoogleApiClient;

    float the_wait_amount;
    long wait_time, TotalTime, startTime;
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    LocationRequest mLocationRequest;

    private static final long LOCATION_REQUEST_INTERVAL = 10 * 1000;
    private static final long LOCATION_REQUEST_INTERVAL_FASTEST = 5 * 1000;
    private static final float LOCATION_REQUEST_DISPLACEMENT = 10;

    private FusedLocationProviderClient mFusedLocationProviderClient;

    private LocationCallback mLocationCallback;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef;
    float lat = 0;
    float longi = 0;
    final static String RIDER_STATE = "RIDER_STATE";

    final int period = 60000;
    final int timerdelay = 120000;

    private static final int TIMER_RATE = 10000;
    private static final int TIMER_DELAY = 0;



     // Handler handler = new Handler(Looper.getMainLooper());

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onCreate() {
        super.onCreate();


        if (isServiceRunning) return;
        isServiceRunning = true;

        settings = getSharedPreferences("details",
                Context.MODE_PRIVATE);


        EventBus.getDefault().register(this);

        buildGoogleApiClient();

        startTime = SystemClock.elapsedRealtime();


        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                if(locationResult==null)return;

                    for(Location location:locationResult.getLocations()){
                        if (location!=null){

                            lat = (float) location.getLatitude();
                            longi = (float) location.getLongitude();

                            EventBus.getDefault().post(new PostLocationUpdates(lat,longi));

                        }
                    }

            }
        };

        foreground_notification();


    }

    @SuppressWarnings("unchecked")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        super.onStartCommand(intent, flags, startId);

        SingletonSocket.getInstance().connect();

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        settings = getSharedPreferences("details",
                Context.MODE_PRIVATE);

        boolean onlinestatus = settings.getBoolean("onlinestatus", false);
        boolean workingstatus = settings.getBoolean("workingstatus", false);

        listenforDifferentLogin();

        if (onlinestatus) {

           TotalTime = SystemClock.elapsedRealtime() - startTime;

           Intent broadcastTime = new Intent("BroadcastTime");
           Bundle bundle = new Bundle();
           bundle.putLong("TotalTime",TotalTime);
           broadcastTime.putExtras(bundle);
           LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(broadcastTime);

                startLocationPush();

            listenforRequests();


                if (workingstatus) {
                    listenforCancellation();

                    if (settings.getBoolean("calculate_wait_amount", false))
                        calculate_penalty_amount();

                    else {
                        if (penaltyTimer != null) {
                            penaltyTimer.cancel();
                            penaltyTimer.purge();
                        }
                    }

                }

        }

        else
            startTime = SystemClock.elapsedRealtime();


        String rider_id = settings.getString("enrout_rider_id", "").trim();
        String customer_id = settings.getString("senderId", "").trim();

        myRef = database.getReference("messages/" + customer_id + rider_id);

        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {

                    Chat lastChat = new Chat();
                    lastChat.createdTimestamp = 0L;
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        Chat chat = postSnapshot.getValue(Chat.class);

                        if (chat == null) continue;

                        if (chat.getCreatedTimestampLong() > lastChat.getCreatedTimestampLong()) {
                            lastChat = chat;
                        }
                        //Log.e(Constants.TAG, new Gson().toJson(chat));
                    }

                    String sentby = lastChat.sentBy.trim();

                    if (sentby != null && !sentby.equals("rider")) {


                        if (workingstatus) {

                            playMessageReceived(getBaseContext());
                            messageNotification(true, lastChat.messageText, settings.getString("senderName", ""), MessagingActivity.class);
                        }
                       /* else {
                           // messageNotification(true, lastChat.messageText, settings.getString("senderName", ""), StartLoadingActivity.class);
                            //Toast.makeText(getApplicationContext(), "Trip has ended. You cannot communicate with the customer any longer", Toast.LENGTH_LONG).show();
                        }*/
                    }


                } catch (NullPointerException e) {

                }

            }

            @Override
            public void onCancelled(DatabaseError error) {

            }
        });


        return START_STICKY;


    }


    public void listenforOtherRiderAcceptance(String dispatchId) {

        final SharedPreferences.Editor editor = settings.edit();

        Intent intentBroadcast = new Intent();
        intentBroadcast.setAction(RIDER_STATE);

        db.collection("DISPATCH").document(dispatchId).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable DocumentSnapshot documentSnapshot, @javax.annotation.Nullable FirebaseFirestoreException e) {

                Map<String, Object> values_of_rider = documentSnapshot.getData();

                boolean accepted = (boolean) values_of_rider.get("isAccepted");
                String acceptedBy = ((String) values_of_rider.get("acceptedBy")).trim();
                String status = (String) values_of_rider.get("status");
                ArrayList <String> rejectRiders = (ArrayList<String>) values_of_rider.get("rejectedBy");

                if (!status.equals("PENDING") && !acceptedBy.equals(settings.getString("enrout_rider_id", "").trim())) {

                    editor.putBoolean("isFound", false);
                    editor.apply();

                    intentBroadcast.putExtra("other_rider", true);
                    sendBroadcast(intentBroadcast);

                }
                else if( rejectRiders.contains(settings.getString("enrout_rider_id", "").trim())){

                    intentBroadcast.putExtra("other_rider", true);
                    sendBroadcast(intentBroadcast);
                }

                else if (accepted && !acceptedBy.equals(settings.getString("enrout_rider_id", "").trim())) {


                    Toast.makeText(getApplicationContext(), "The request has been accepted by another rider", Toast.LENGTH_LONG).show();


                    editor.putString("the_request_id", "");
                    editor.remove("PickupsFromService");
                    editor.remove("DestinationsFromService");
                    editor.putBoolean("isFound", false);
                    editor.putBoolean("acceptedDispatch", false);
                    editor.putBoolean("workingstatus", false);
                    editor.putBoolean("showAgain", true);
                    editor.putBoolean("isFull", false);
                    editor.putBoolean("calculate_wait_amount", false);

                    editor.apply();

                    intentBroadcast.putExtra("other_rider", true);
                    sendBroadcast(intentBroadcast);

                } else if (acceptedBy.equals(settings.getString("enrout_rider_id", "").trim())&& accepted && status.equals("ONGOING")) {


                    editor.putBoolean("isFound", true);
                    editor.putBoolean("acceptedDispatch", true);
                    editor.putBoolean("workingstatus", true);

                    editor.apply();

                    intentBroadcast.putExtra("acceptedByMe", true);
                    sendBroadcast(intentBroadcast);


                }

            }
        });

    }




    @Override
    public void onDestroy() {
        super.onDestroy();

        if(penaltyTimer!=null)
        {
            penaltyTimer.cancel();
            penaltyTimer.purge();
        }

        if(locationTimer!=null)
        {
            locationTimer.cancel();
            locationTimer.purge();
        }

        isServiceRunning = false;

        EventBus.getDefault().unregister(this);

        if (mp != null) {
            if (mp.isPlaying()) {
                mp.stop();
            }
        }
        stopSelf();

     //   removeLocationUpdate();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Subscribe
    public void timeEventReceived(PostTimeToService event){

        realTime = event.data;

    }


    public void listenforDifferentLogin(){

        final SharedPreferences.Editor editor = settings.edit();

        final String rider_id = settings.getString("enrout_rider_id","-").trim();

        db.collection("DRIVERS").whereEqualTo(FieldPath.documentId(),rider_id.trim()).
                whereEqualTo("deviceId", Constants.deviceID).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {


                List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();

                if (documentSnapshots.isEmpty()){

                    editor.putBoolean("isFound", true);
                    editor.putBoolean("workingstatus",false);
                    editor.putBoolean("onlinestatus", false);
                    editor.putString("verifyemail", "none");
                    editor.putString("enrout_rider_id","none");

                    editor.apply();

                    Intent intent = new Intent(getApplicationContext(), LandingActivity.class);

                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    startActivity(intent);
                    stopSelf();

                }
                else return;




            }
        });
    }

    public void listenforCancellation(){

        final SharedPreferences.Editor editor = settings.edit();

        final String request_id = settings.getString("the_request_id","-").trim();

        db.collection("REQUEST").whereEqualTo("requestId",request_id.trim()).
                whereEqualTo("status", "CANCELLED").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {



                List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();

                if (documentSnapshots.isEmpty()) return;

                editor.putBoolean("calculate_wait_amount", false);

                editor.apply();


                Intent intent = new Intent(getApplicationContext(), RiderSeeCancellationNotificationActivity.class);

                addCancelledTrip(new Float(Math.round(Constants.baseFare)), settings.getString("array_destinations", ""), settings.getString("array_pickups", ""),new Float( Math.round((Constants.costperkm)*(settings.getFloat("distanceinKm",0))))
                        , settings.getString("enrout_rider_id","").trim(), 0, settings.getString("paymentMethod",""),
                        request_id.trim(), settings.getString("senderName",""), settings.getString("senderPhoneNumber",""), "CANCELLED", new Float( Math.round(Constants.costpermin) * (settings.getFloat("timeinMin",0))) ,   new Float( Math.round(settings.getFloat("estimatedCost",0))) ,
                        settings.getString("trip_date", ""),  new Float( settings.getFloat("waitAmount",0)), DurationFormatUtils.formatDuration(settings.getLong("totalWaitTime",0), "HH' hr' mm' min'"));


                startActivity(intent);



            }
        });
    }

    public void listenforRequests()
    {

        final  String rider_id =  settings.getString("enrout_rider_id","").trim();

        db.collection("DISPATCH").whereArrayContains("riders",rider_id).whereEqualTo("isAccepted",false).whereEqualTo("status","PENDING")
                .whereEqualTo("timeCode",settings.getString("timeCode","-").trim()).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {

                List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();

                if (documentSnapshots.isEmpty()) return;

                SharedPreferences.Editor editor = settings.edit();

                boolean riderfound = settings.getBoolean("isFound", false);

                if(!riderfound){

                    for (DocumentSnapshot the_document : documentSnapshots)

                    {

                        String the_documentId = the_document.getId();

                        Map<String, Object> values_of_rider = the_document.getData();

                        boolean accepted = (boolean) values_of_rider.get("isAccepted");
                        String status = (String)values_of_rider.get("status");
                        ArrayList <String> rejectRiders = (ArrayList<String>) values_of_rider.get("rejectedBy");

                        String cancelledBY_Rider = (String) values_of_rider.get("cancelledBy");

                        editor.putBoolean("acceptedDispatch", accepted);
                        editor.apply();

                        if (!accepted && !status.equals("DELAYED")&&!cancelledBY_Rider.trim().equals(rider_id)&&
                                !rejectRiders.contains(rider_id)) {

                            final DocumentReference request_ref = (DocumentReference) values_of_rider.get("requestId");


                            final String request_id = request_ref.getId();

                            request_ref.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                                    if (task.isSuccessful()) {

                                        DocumentSnapshot querySnapshot = task.getResult();

                                        if (querySnapshot.exists()) {
                                            Map<String, Object> trip_details = querySnapshot.getData();

                                            if (trip_details == null)
                                                return;

                                            Log.d("Tag", "Current data: is not null");

                                            settings = getSharedPreferences("details",
                                                    Context.MODE_PRIVATE);

                                            SharedPreferences.Editor editor = settings.edit();

                                            String senderId = (String) trip_details.get("senderId");
                                            String senderName = (String) trip_details.get("senderName");
                                            String senderPhoneNumber = (String) trip_details.get("senderPhoneNumber");
                                            String paymentMethod = (String) trip_details.get("paymentMethod");
                                            double payforMe = ((Number) trip_details.get("payForMe")).doubleValue();
                                            double timeinMinute = (((Number) trip_details.get("estimatedTimeInSeconds"))).doubleValue() / 60;
                                            double distanceinKm = (((Number) trip_details.get("estimatedDistanceInMeters"))).doubleValue() / 1000;
                                            Timestamp trip_date = (Timestamp) trip_details.get("arrivedAt");
                                            String requestStatus = (String) trip_details.get("status");
                                            double estimatedCost = ((Number) trip_details.get("estimatedCost")).doubleValue();
                                            int task_to_doCount = ((Number) trip_details.get("taskCount")).intValue();
                                            double couponValueInPercentage = ((Number) trip_details.get("couponValueInPercentage")).doubleValue();

                                            Date date = trip_date.toDate();

                                            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm aaa", Locale.getDefault());

                                            editor.putString("senderId_VIEW", senderId);
                                            editor.putString("senderName_VIEW", senderName);
                                            editor.putString("senderPhoneNumber_VIEW", senderPhoneNumber);
                                            editor.putString("paymentMethod_VIEW", paymentMethod);
                                            editor.putString("requestStatus_VIEW", requestStatus);
                                            editor.putFloat("pay_for_me_VIEW", (float) payforMe);
                                            editor.putInt("taskCount_VIEW", task_to_doCount);
                                            editor.putFloat("distanceinKm_VIEW", (float) distanceinKm);
                                            editor.putFloat("timeinMin_VIEW", (float) timeinMinute);
                                            editor.putString("dispatch_id_VIEW", the_documentId.trim());
                                            editor.putString("trip_date_VIEW", sdf.format(date));
                                            editor.putFloat("couponpercentage_VIEW", (float) couponValueInPercentage);
                                            editor.putFloat("estimatedCost_VIEW", (float) estimatedCost);
                                            editor.putString("the_request_id_VIEW", request_id);
                                            editor.apply();

                                            List<DocumentReference> arrayListPickups = (List) trip_details.get("pickups");

                                            getPickUpsOfAGivenRequest(arrayListPickups, new OnPickUpsGathered() {
                                                @Override
                                                public void pickupsGathered(ArrayList<PickupView> pickupArrayList) {

                                                    String stringsOfPickUps = gson.toJson(pickupArrayList);
                                                    SharedPreferences.Editor editor = settings.edit();
                                                    editor.putString("PickupsFromService_VIEW", stringsOfPickUps);
                                                    editor.apply();



                                            List<DocumentReference> arrayListDestinations = (List) trip_details.get("destinations");

                                            getDestinationsOfAGivenPickup(arrayListDestinations, new OnDestinationsGathered() {
                                                @Override
                                                public void destinationsGathered(ArrayList<DestinationView> destinations) {

                                                    String stringsOfPickUps = gson.toJson(destinations);
                                                    SharedPreferences.Editor editor = settings.edit();
                                                    editor.putString("DestinationsFromService_VIEW", stringsOfPickUps);
                                                    editor.apply();
                                                    if (!(settings.getBoolean("acceptedDispatch", false))) {
                                                        editor.putBoolean("isFound", true);
                                                        editor.apply();
;
                                                        Intent intent = new Intent(getApplicationContext(), RiderViewTripActivity.class);

                                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                                        startActivity(intent);
                                                        listenforOtherRiderAcceptance(the_documentId);

                                                    }

                                                }

                                            });

                                                }
                                            });

                                        }
                                    }


                                }
                            });




                        }

                        else continue;


                    }

                }

            }
        });

    }



    private void getPickUpsOfAGivenRequest(final List <DocumentReference> arrayListPickups, final OnPickUpsGathered onPickUpsGathered){

        pickupsForARequestCounter = 0;

        final ArrayList<PickupView> pickupsArrayList = new ArrayList<>();
        for (final DocumentReference pickup_references : arrayListPickups ) {


            pickup_references.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                    if (task.getResult() != null && task.getResult().exists()) {

                        String pickupPointsId = pickup_references.getId();

                        Map<String, Object> pickup_details = task.getResult().getData();

                        if (pickup_details == null) return;

                        // playRequestUpdated(getApplicationContext());

                        String category = (String) pickup_details.get("itemCategory");
                        String instructions = (String) pickup_details.get("instruction");
                        String pickupPlace = (String) pickup_details.get("place");
                        Boolean pickupStatus = (Boolean)pickup_details.get("isPickedup");
                        Map<String, Object> pickUpcordinates = (Map<String, Object>) pickup_details.get("pos");
                        Map<String, Object> geopoints = (Map<String, Object>) pickUpcordinates.get("geopoint");

                        double latitude = (double)geopoints.get("latitude");
                        double longitude = (double)geopoints.get("longitude");


                        final PickupView pickup = new PickupView();
                        pickup.pickUpId = pickupPointsId;
                        pickup.itemCategory = category;
                        pickup.instruction = instructions;
                        pickup.isPickedup = pickupStatus;
                        pickup.place = pickupPlace;
                        pickup.latLng = new LatLng(latitude,longitude);

                        pickupsArrayList.add(pickup);

                        pickupsForARequestCounter ++;

                        if (pickupsForARequestCounter >= (arrayListPickups.size())){
                            onPickUpsGathered.pickupsGathered(pickupsArrayList);


                        }

                    }

                }
            });




        }
    }

    private void getDestinationsOfAGivenPickup(final List<DocumentReference> arrayListDestinations, final OnDestinationsGathered onDestinationsGathered){
        final ArrayList<DestinationView> destinationsArrayList = new ArrayList<>();

        detinationForPickupCounter = 0;

        for(final DocumentReference destination_references : arrayListDestinations){

            destination_references.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {



                    if (task.getResult() != null && task.getResult().exists()) {

                        String destinationsId = destination_references.getId();

                        Map<String, Object> destination_details = task.getResult().getData();

                        if (destination_details == null) return;

                        String recipientName = (String) destination_details.get("recipientName");
                        String recipientNumber = (String) destination_details.get("recipientNumber");
                        String destinationPlace = (String) destination_details.get("place");
                        Boolean deliveryStatus = (Boolean)destination_details.get("isDelivered");
                        Map<String, Object> destinationCordinates = (Map<String, Object>) destination_details.get("pos");
                        Map<String, Object> geopoints = (Map<String, Object>) destinationCordinates.get("geopoint");

                        double latitude = (double)geopoints.get("latitude");
                        double longitude = (double)geopoints.get("longitude");

                        DestinationView destination = new DestinationView();
                        destination.destinationId = destinationsId;
                        destination.recipientName = recipientName;
                        destination.isDelivered = deliveryStatus;
                        destination.recipientNumber = recipientNumber;
                        destination.place = destinationPlace;
                        destination.latLng = new LatLng(latitude,longitude);

                        destinationsArrayList.add(destination);


                        detinationForPickupCounter++;

                        if (detinationForPickupCounter >= (arrayListDestinations.size())){
                            onDestinationsGathered.destinationsGathered(destinationsArrayList);
                        }



                    }

                }
            });


        }
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mGoogleApiClient.connect();
    }

    private void createLocationRequest() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(LOCATION_REQUEST_INTERVAL);
        mLocationRequest.setFastestInterval(LOCATION_REQUEST_INTERVAL_FASTEST);
        mLocationRequest.setSmallestDisplacement(LOCATION_REQUEST_DISPLACEMENT);

        requestLocationUpdate();
    }



    private void requestLocationUpdate() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                //get the last location of the device
            }
        });

        mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback,
                Looper.myLooper());
    }

 /*   private void removeLocationUpdate() {
        mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
    }*/

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

       createLocationRequest();

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private interface OnDestinationsGathered{
        void destinationsGathered(ArrayList<DestinationView> destinations);
    }

    private interface OnPickUpsGathered{
        void pickupsGathered(ArrayList<PickupView> pickupArrayList);
    }

    public void SendCordinatesAtWork() {

        final  String rider_id =  settings.getString("enrout_rider_id","").trim();

        settings = getSharedPreferences("details",
                Context.MODE_PRIVATE);

        String ridername= settings.getString("rider_fullname","");
        String fleetId= settings.getString("fleetID","");
        boolean onlinestatus =  settings.getBoolean("onlinestatus",false);
        boolean workingstatus = settings.getBoolean("workingstatus", false);
        boolean approvalStatus = settings.getBoolean("approvalstatus",false);
        boolean riderFound = settings.getBoolean("isFound",false);
        boolean riderFull = settings.getBoolean("isFull",false);
        boolean SOS_Status = settings.getBoolean("SOS",false);


        JSONObject mapData = new JSONObject();
        GeoPoint riderGeopoint = new GeoPoint(lat, longi);

        try {

            mapData.put("riderId", rider_id);
            mapData.put("geoPoint", riderGeopoint);
            mapData.put("riderName",ridername);
            mapData.put("geohash", Constants.getGeoHash(new LatLng(lat, longi)));
            mapData.put("onlineStatus",onlinestatus );
            mapData.put("workingStatus",workingstatus  );
            mapData.put("type","DRIVER");
            mapData.put("longitude",longi);
            mapData.put("latitude",lat);
            mapData.put("isOnline",onlinestatus);
            mapData.put("isWorking",workingstatus);
            mapData.put("isApproved", approvalStatus);
            mapData.put("isFound",riderFound);
            mapData.put("isFull",riderFull);
            mapData.put("averageRatings",settings.getFloat("rating",0));
            mapData.put("requestId", settings.getString("the_request_id",""));
            mapData.put("sos", settings.getBoolean("SOS",false));
            mapData.put("timeOnline",TotalTime );
            mapData.put("scheduleId", settings.getString("riderScheduleId","-").trim());
            mapData.put("fleetId", settings.getString("fleetID","-").trim());
            mapData.put("riderLastTrip",settings.getBoolean("onLastTrip",false));

            SingletonSocket.getInstance().emit("updatelocation", mapData);

         /*  handler.post(
                    new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            Toast.makeText(getApplicationContext(),""+mapData,Toast.LENGTH_SHORT).show();

                        }
                    }
            );*/

            JSONObject mapDataOngoingTrip = new JSONObject();

            if(workingstatus)
            {

                mapDataOngoingTrip.put("riderId", rider_id);
                mapDataOngoingTrip.put("geoPoint", riderGeopoint);
                mapDataOngoingTrip.put("riderName", ridername);
                mapDataOngoingTrip.put("geohash", Constants.getGeoHash(new LatLng(lat, longi)));
                mapDataOngoingTrip.put("onlineStatus", onlinestatus);
                mapDataOngoingTrip.put("workingStatus", workingstatus);
                mapDataOngoingTrip.put("type", "DRIVER");
                mapDataOngoingTrip.put("longitude", longi);
                mapDataOngoingTrip.put("latitude", lat);
                mapDataOngoingTrip.put("isOnline", onlinestatus);
                mapDataOngoingTrip.put("isWorking", workingstatus);
                mapDataOngoingTrip.put("isApproved", approvalStatus);
                mapDataOngoingTrip.put("fleetId", fleetId.trim());
                mapDataOngoingTrip.put("customerName",settings.getString("senderName",""));
                mapDataOngoingTrip.put("customerPhoneNumber",settings.getString("senderPhoneNumber",""));
                mapDataOngoingTrip.put("requestId",settings.getString("the_request_id","").trim());
                mapDataOngoingTrip.put("status",settings.getString("requestStatus","").trim());
                mapDataOngoingTrip.put("customerId",settings.getString("senderId","").trim());
                mapDataOngoingTrip.put("targetId",settings.getString("targetId",""));
                mapDataOngoingTrip.put("targetLongitude",settings.getFloat("pickup_longitude", longi));
                mapDataOngoingTrip.put("targetLatitude",settings.getFloat("pickup_latitude",lat));
                mapDataOngoingTrip.put("tripType", settings.getBoolean("tripType",true));
                mapDataOngoingTrip.put("place",settings.getString("geolocation_name","on trip"));
                mapDataOngoingTrip.put("sos", settings.getBoolean("SOS",false));
                mapDataOngoingTrip.put("triptimeUpdate", realTime);
                mapDataOngoingTrip.put("isTapped", settings.getBoolean("isTapped",false));
                mapDataOngoingTrip.put("riderLastTrip",settings.getBoolean("onLastTrip",false));
                mapDataOngoingTrip.put("isFull",riderFull);
                mapDataOngoingTrip.put("allRequests", settings.getStringSet("ListofPickupRequests", set));
                mapDataOngoingTrip.put("customerType",settings.getString("customerType","CUSTOMER"));


                SingletonSocket.getInstance().emit("request::trip-ongoing", mapDataOngoingTrip);




            }

            SingletonSocket.getInstance().on("updatelocation::push::back", args -> {
                JSONObject obj2 = (JSONObject) args[0];

                // Log.d(Constants.TAG, "updatelocation::push::back - " + obj2);

            });


            SingletonSocket.getInstance().on("updatelocation::push::forward", args -> {
                JSONObject obj2 = (JSONObject) args[0];

                // Log.d(Constants.TAG, "updatelocation::push::forward - " + obj2);

            });




        } catch (JSONException e) {
            e.printStackTrace();
        }




    }






    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);

        Log.i(TAG, "serviceonTaskRemoved()");

        isServiceRunning = false;

        EventBus.getDefault().unregister(this);

        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
        restartServiceIntent.setPackage(getPackageName());

        PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(
                AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + 1000,
                restartServicePendingIntent);


    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.i(TAG, "onLowMemory()");


    }

    public void playMessageReceived(Context context) {

        if (mp != null) {
            if (mp.isPlaying()) {
                mp.stop();
            }
            mp.reset();
        }
        mp = MediaPlayer.create(context, R.raw.messaging);
        mp.start();



    }

    private Thread.UncaughtExceptionHandler defaultUEH;
    private Thread.UncaughtExceptionHandler uncaughtExceptionHandler = new Thread.UncaughtExceptionHandler() {

        @Override
        public void uncaughtException(Thread thread, Throwable ex) {
            Log.d(TAG, "Uncaught exception start!");
            ex.printStackTrace();

            //Same as done in onTaskRemoved()
            Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
            restartServiceIntent.setPackage(getPackageName());

            PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
            AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
            alarmService.set(
                    AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    SystemClock.elapsedRealtime() + 1000,
                    restartServicePendingIntent);

        }
    };



    private void startLocationPush() {
        cancelTimer();
        scheduleTimer();
    }

    private void scheduleTimer() {
        locationTimer = new Timer();
        locationTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                SendCordinatesAtWork();

            }
        }, TIMER_DELAY, TIMER_RATE);
    }

    private void cancelTimer() {
        if (locationTimer != null){
            locationTimer.cancel();
            locationTimer.purge();}
    }

    public void addCancelledTrip(float basefare, String deliveryLoc, String pickuploc, float distanceCost, String driverRef, float earning,
                                 String paymentMethod, String requestRef, String senderName, String senderNumber, String status, float timeCost, float tripCost, String tripDate, float waitTime,String waitDuration )
    {

        SharedPreferences.Editor editor = settings.edit();

        TripCalculations calculate = new TripCalculations();

        double couponCost = calculate.promo_percentage(settings.getFloat("couponpercentage",0), tripCost);

        double enroutFee =  calculate.enrout_fee(tripCost,couponCost,earning);

        TripComplete trip_ended = new TripComplete();

        trip_ended.basefare = basefare;
        trip_ended.deliveryLocation = deliveryLoc;
        trip_ended.pickupLocation = pickuploc;
        trip_ended.distanceCost = distanceCost;
        trip_ended.driverReference = driverRef;
        trip_ended.earning = earning;
        trip_ended.paymentMethod = paymentMethod;
        trip_ended.requestReference = requestRef;
        trip_ended.senderName = senderName;
        trip_ended.senderNumber = senderNumber;
        trip_ended.status = status;
        trip_ended.timeCost = timeCost;
        trip_ended.tripCost = tripCost;
        trip_ended.tripDate = tripDate;
        trip_ended. waitTime = waitTime;
        trip_ended.payForMe = settings.getFloat("pay_for_me",0);
        trip_ended.couponAmount = Math.round(couponCost);
        trip_ended.enroutFee = Math.round(enroutFee);
        trip_ended.scheduleId = settings.getString("riderScheduleId","-").trim();
        trip_ended.waitDuration = waitDuration;

        db.collection("TRIPAUDITS").document().set(trip_ended)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        Toast.makeText(getApplicationContext(), "Trip has been cancelled by client", Toast.LENGTH_LONG).show();

                        editor.putBoolean("showAgain", false);

                        editor.apply();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("Failure", "Failed", e);

                        Toast.makeText(getApplicationContext(), "Kindly check connection", Toast.LENGTH_LONG).show();
                        return;

                    }
                });;


    }



    public void calculate_penalty_amount(){

        try {
            penaltyTimer=new Timer();

            penaltyTimer.scheduleAtFixedRate(new TimerTask()
            {
                public void run()
                {


                    the_wait_amount+=Constants.waitPenalty;


                    wait_time+=period;

                    SharedPreferences.Editor editor = settings.edit();
                    editor.putFloat("waitAmount", the_wait_amount);
                    editor.putLong("totalWaitTime",wait_time);
                    editor.apply();


                    Log.e("wait_time", "Amount "+the_wait_amount +" Time "+wait_time );
                }

            }, timerdelay, period);


        }
        catch (NullPointerException e){

        }



    }


    private void messageNotification(boolean vibrate, String message, String senderName, Class the_class) {
        Intent intent = new Intent(this, the_class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Notification notification = new Notification();

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // String channelId = "channel-01";
        String channelId = "enrout-01";
        String channelName = "Enrout";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,channelId);

        if (vibrate) {
            notification.defaults |= Notification.DEFAULT_VIBRATE;
        }

        notificationBuilder.setDefaults(notification.defaults);
        notificationBuilder.setSmallIcon(R.mipmap.outer_icon)
                .setContentTitle(senderName)
                .setContentText(message)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);





        notificationManager.notify(0 , notificationBuilder.build());

    }

    public void foreground_notification()
    {

        Intent notificationIntent = new Intent(getApplicationContext(), StartLoadingActivity.class);
        notificationIntent.setAction(ACTION_MAIN);  // A string containing the action name
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent contentPendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        String channelId = "enrout-01";
        String channelName = "Enrout";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);

            Notification notification = new Notification.Builder(this, channelId)
                    .setContentTitle(getResources().getString(R.string.app_name))
                    .setTicker(getResources().getString(R.string.app_name))
                    .setContentText("Enrout Partner is active")
                    .setSmallIcon(R.mipmap.outer_icon)
                    .setContentIntent(contentPendingIntent)
                    .setOngoing(true)
//                .setDeleteIntent(contentPendingIntent)  // if needed
                    .build();
            notification.flags = notification.flags | Notification.FLAG_NO_CLEAR;     // NO_CLEAR makes the notification stay when the user performs a "delete all" command
            startForeground(1, notification);
        } else {
            Notification notification = new Notification.Builder(this)
                    .setContentTitle(getResources().getString(R.string.app_name))
                    .setTicker(getResources().getString(R.string.app_name))
                    .setContentText("Enrout Partner is active")
                    .setSmallIcon(R.mipmap.outer_icon)
                    .setContentIntent(contentPendingIntent)
                    .setOngoing(true)
//                .setDeleteIntent(contentPendingIntent)  // if needed
                    .build();
            notification.flags = notification.flags | Notification.FLAG_NO_CLEAR;     // NO_CLEAR makes the notification stay when the user performs a "delete all" command
            startForeground(1, notification);
        }
    }

}




