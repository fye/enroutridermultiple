package com.enroutrider.ride_;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.Manifest;

import com.enroutrider.ride_.network.ServiceCheck;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;
import java.util.Map;

import id.voela.actrans.AcTrans;


public class StartLoadingActivity extends Activity {

    SharedPreferences settings;
    SharedPreferences.Editor editor;
    ProgressBar loading;
    private static int SPLASH_TIME_OUT = 3 * 1000;
    private FirebaseAuth mAuth;


    FirebaseFirestore db = FirebaseFirestore.getInstance();
    ServiceCheck service = new ServiceCheck();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_start_loading);

        loading = findViewById(R.id.show_loading);

        mAuth = FirebaseAuth.getInstance();

        Constants.deviceID = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);


        MapsInitializer.initialize(getApplicationContext());
        // Fixing Later Map loading Delay
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    MapView mv = new MapView(getApplicationContext());
                    mv.onCreate(null);
                    mv.onPause();
                    mv.onDestroy();
                } catch (Exception ignored) {

                }
            }
        }).start();


        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

        settings = getSharedPreferences("details",
                Context.MODE_PRIVATE);

                if (ContextCompat.checkSelfPermission(StartLoadingActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                    if (ActivityCompat.shouldShowRequestPermissionRationale(StartLoadingActivity.this,
                            Manifest.permission.ACCESS_FINE_LOCATION)){
                        ActivityCompat.requestPermissions(StartLoadingActivity.this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                    }else{
                        ActivityCompat.requestPermissions(StartLoadingActivity.this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                    }
                }
                else do_LocationCheck();

            }
        }, SPLASH_TIME_OUT);
    }


    public void do_LocationCheck ()
    {

        LocationRequest mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(1000)
                .setNumUpdates(2);

        final LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        builder.setNeedBle(true);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                Log.d("LocationEnabled", "onSuccess() called with: locationSettingsResponse = [" + locationSettingsResponse + "]");

                mAuth.signInWithEmailAndPassword(Constants.authEmail, Constants.authPassword)
                        .addOnCompleteListener(StartLoadingActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {

                                    loggon();

                                } else {

                                    Log.w("SafeLog", "signInWithEmail:failure", task.getException());
                                    Toast.makeText(StartLoadingActivity.this, "Authentication failed. Check Internet Connection",
                                            Toast.LENGTH_SHORT).show();
                                }



                            }
                        });





            }
        });
        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("LocationEnabled", "onSuccess --> onFailure() called with: e = [" + e + "]");
                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(StartLoadingActivity.this,
                                Constants.REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException t) {

                        t.printStackTrace();
                    }
                }

            }
        });
    }



    public void loggon() {

        settings = getSharedPreferences("details",
                Context.MODE_PRIVATE);


        db.collection("SETTINGS")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if (task.getResult().isEmpty()) {

                                return;

                            } else {


                                QuerySnapshot querySnapshot = task.getResult();

                                List<DocumentSnapshot> documentSnapshots = querySnapshot.getDocuments();

                                DocumentSnapshot the_document = documentSnapshots.get(0);

                                Map<String, Object> setting = the_document.getData();

                                Constants.officeLine = (String) setting.get("companyPhoneNumber");

                                Constants.range = ((Number) setting.get("range")).intValue();

                                Constants.tripView = ((Number) setting.get("tripAuditview")).intValue();

                                Constants.FAQ = (String) setting.get("faq");

                                String the_timeCode = (String) setting.get("timeCode");

                                SharedPreferences.Editor editor = settings.edit();

                                editor.putString("timeCodeURL", (String) setting.get("timeCodeUrl"));

                                editor.putString("timeCode", the_timeCode);

                                editor.putInt("versionNumber", ((Number) setting.get("versionNumber")).intValue());
                                editor.apply();

                            }
                        } else {
                            Log.w("Tag", "Error connecting to server.", task.getException());
                        }


        // String email_address = "gagcc.database@gmail.com";

        String phoneNumber_verify = settings.getString("verifyemail", "none");

        db.collection("DRIVERS")
                .whereEqualTo("phoneNumber", phoneNumber_verify)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if (task.getResult().isEmpty()) {

                               // dialog.dismiss();

                                Intent intent = new Intent(StartLoadingActivity.this, LandingActivity.class);
                                startActivity(intent);
                                new AcTrans.Builder(StartLoadingActivity.this).performSlideToBottom();
                                finish();

                            } else {

                                QuerySnapshot querySnapshot = task.getResult();

                                List<DocumentSnapshot> documentSnapshots = querySnapshot.getDocuments();

                                DocumentSnapshot the_document = documentSnapshots.get(0);


                                String rider_id = the_document.getId();

                                Constants.riderId = the_document.getId();


                                Map<String, Object> values_of_rider = the_document.getData();

                                String date_of_birth = (String) values_of_rider.get("dob");
                                String fullname = (String) values_of_rider.get("fullname");

                                Constants.riderName = (String) values_of_rider.get("fullname");
                                String gender = (String) values_of_rider.get("gender");
                                String phoneNumber = (String) values_of_rider.get("phoneNumber");
                                String vehicle = (String) values_of_rider.get("vehicle");
                                Constants.riderNumber = (String) values_of_rider.get("phoneNumber");
                                double rating = ((Number) values_of_rider.get("averageRating")).doubleValue();
                                String email = (String) values_of_rider.get("emailAddress");
                                double withdrawalAmount = ((Number) values_of_rider.get("withdrawalAmount")).doubleValue();
                                boolean approved = (boolean) values_of_rider.get("isApproved");
                                String fleetId = (String) values_of_rider.get("fleet");
                                boolean working = (boolean) values_of_rider.get("isWorking");
                                boolean online = (boolean) values_of_rider.get("isOnline");
                                boolean SOS = (boolean) values_of_rider.get("SOS");
                                String scheduleID = (String) values_of_rider.get("scheduleId");

                                settings = getSharedPreferences("details",
                                        Context.MODE_PRIVATE);

                                SharedPreferences.Editor editor = settings.edit();

                                editor.putBoolean("onlinestatus", online);
                                editor.putBoolean("approvalstatus", approved);
                                editor.putString("fleetID", fleetId.trim());
                                editor.putBoolean("workingstatus", working);
                                editor.putString("rider_gender", gender.trim());
                                editor.putFloat("rating", (float) rating);
                                editor.putString("riderScheduleId", scheduleID.trim());
                                editor.putFloat("withdrawal_pfm", (float) withdrawalAmount);
                                editor.putString("phoneNumber", phoneNumber.trim());
                                editor.putString("rider_email", email.trim());
                                editor.putBoolean("SOS", SOS);
                                editor.putString("enrout_rider_id", rider_id.trim());
                                editor.putString("rider_fullname", fullname);
                                editor.putString("rider_dob", date_of_birth.trim());
                                editor.apply();


                                DocumentReference docref = db.collection("VEHICLES").document(vehicle);

                                docref.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        if (task.isSuccessful()) {

                                            DocumentSnapshot querySnapshot = task.getResult();

                                            if (querySnapshot.exists()) {
                                                Map<String, Object> values_of_rider = querySnapshot.getData();

                                                // String fleetNumber = (String)values_of_rider.get("plateNumber");
                                                Constants.useCourierBox = (boolean) values_of_rider.get("useCourierBox") ;
                                                Constants.plateNumber = (String) values_of_rider.get("plateNumber");
                                            } else {
                                                AlertDialog.Builder builder = new AlertDialog.Builder(StartLoadingActivity.this);
                                                builder.setCancelable(true);
                                                builder.setInverseBackgroundForced(true);
                                                builder.setMessage("You have not been assigned a bike number. Kindly visit the office");
                                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                      //  dialog.dismiss();
                                                    }
                                                });

                                                AlertDialog alert = builder.create();
                                                alert.show();
                                            }

                                        }


                                    }
                                });


                                db.collection("RIDERPROFILEIMAGES").
                                        whereEqualTo("driver", rider_id).get().
                                        addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                                                if (task.isSuccessful()) {
                                                    if (task.getResult().isEmpty()) {

                                                    //    dialogdismiss();
                                                        loading.setVisibility(View.GONE);

                                                        AlertDialog.Builder builder = new AlertDialog.Builder(StartLoadingActivity.this);
                                                        builder.setCancelable(false);
                                                        builder.setIcon(R.mipmap.customer);
                                                        builder.setTitle("Profile picture not set");
                                                        builder.setInverseBackgroundForced(true);
                                                        builder.setMessage("Kindly visit the Enrout office to set your rider profile picture");
                                                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {

                                                                finish();
                                                            }
                                                        });

                                                        AlertDialog alert = builder.create();
                                                        alert.show();

                                                        return;
                                                    }
                                                    QuerySnapshot querySnapshot = task.getResult();

                                                    List<DocumentSnapshot> documentSnapshots = querySnapshot.getDocuments();

                                                    DocumentSnapshot the_document = documentSnapshots.get(0);

                                                    Map<String, Object> values_of_rider = the_document.getData();

                                                    Constants.imagedownloadUrl = (String) values_of_rider.get("downloadURL");

                                                    Constants.imageURL = (String) values_of_rider.get("path");

                                                    if (approved == true && working == true) {


                                                        db.collection("DRIVERS").document(rider_id.trim()).update("deviceId", Constants.deviceID).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                            @Override
                                                            public void onSuccess(Void aVoid) {
                                                              //  dialogdismiss();
                                                                loading.setVisibility(View.GONE);

                                                                Boolean running = service.isMyServiceRunning(RiderGetLocationAndRequestService.class, StartLoadingActivity.this);

                                                                if(!running){

                                                                    Intent intent2 = new Intent(getBaseContext(), RiderGetLocationAndRequestService.class);

                                                                    startService(intent2);
                                                                }

                                                                Intent intent = new Intent(StartLoadingActivity.this, ProcessTrip.class);
                                                                startActivity(intent);

                                                                new AcTrans.Builder(StartLoadingActivity.this).performSlideToTop();

                                                                Toast.makeText(StartLoadingActivity.this, "You have an uncompleted Delivery, Please Complete it", Toast.LENGTH_LONG).show();
                                                                Toast.makeText(StartLoadingActivity.this, "Delivery is completed when client pays.", Toast.LENGTH_LONG).show();

                                                                finish();
                                                            }
                                                        });


                                                    } else if (approved == true && working == false) {

                                                        if (!settings.getBoolean("SOS", false)) {
                                                            editor.remove("the_request_id");
                                                        }

                                                        editor.remove("tripType");
                                                        editor.remove("arrivalIdToUpdate");
                                                        editor.remove("PickupsFromService");
                                                        editor.remove("DestinationsFromService");
                                                        editor.remove("waitAmount");
                                                        editor.remove("hideCancel");
                                                        editor.remove("newMessage");
                                                        editor.remove("isFound");
                                                        editor.remove("pickup_longitude");
                                                        editor.remove("pickup_latitude");
                                                        editor.remove("geolocation_name");
                                                        editor.remove("totalWaitTime");
                                                        editor.remove("isTapped");
                                                        editor.remove("isFull");
                                                        editor.remove("workingstatus");
                                                        editor.putBoolean("showAgain", true);

                                                        editor.putBoolean("calculate_wait_amount", false);

                                                        editor.apply();

                                                        //   mWorkManager.enqueue(mRequest);
                                                        db.collection("DRIVERS").document(rider_id.trim()).update("deviceId", Constants.deviceID).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                            @Override
                                                            public void onSuccess(Void aVoid) {
                                                             //   dialogdismiss();
                                                                loading.setVisibility(View.GONE);

                                                                Boolean running = service.isMyServiceRunning(RiderGetLocationAndRequestService.class, StartLoadingActivity.this);

                                                                if(!running)
                                                                {
                                                                    Intent intent2 = new Intent(getBaseContext(), RiderGetLocationAndRequestService.class);

                                                                    startService(intent2);
                                                                }


                                                                Intent intent = new Intent(StartLoadingActivity.this, MainActivity.class);

                                                                startActivity(intent);
                                                                new AcTrans.Builder(StartLoadingActivity.this).performSlideToLeft();
                                                                finish();
                                                            }
                                                        });


                                                    } else {

                                                     //   dialogdismiss();
                                                        loading.setVisibility(View.GONE);

                                                        AlertDialog.Builder builder = new AlertDialog.Builder(StartLoadingActivity.this);
                                                        builder.setCancelable(false);
                                                        builder.setIcon(R.mipmap.not_approved);
                                                        builder.setTitle("Account Not Active");
                                                        builder.setInverseBackgroundForced(true);
                                                        builder.setMessage("Kindly visit the Enrout office to have your rider account activated");
                                                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {

                                                                finish();
                                                            }
                                                        });

                                                        AlertDialog alert = builder.create();
                                                        alert.show();

                                                    }

                                                }

                                            }
                                        });


                            }
                        } else {
                            Log.w("Tag", "Error connecting to server.", task.getException());
                        }
                    }
                });
    }
}).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(StartLoadingActivity.this,"Error starting Enrout Rider. Check internet connection",
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new AcTrans.Builder(this).performSlideToRight();
        loading.setVisibility(View.GONE);

    }

    @Override
    public void onStop() {
        super.onStop();

      //  dialogdismiss();
        loading.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        loading.setVisibility(View.VISIBLE);
    }


    @Override
    public void onPause() {
        super.onPause();

     //   dialogdismiss();
        loading.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

       // dialogdismiss();
        loading.setVisibility(View.GONE);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CHECK_SETTINGS) {

            // Make sure the request was successful
            if (resultCode == RESULT_OK) {

                mAuth.signInWithEmailAndPassword(Constants.authEmail, Constants.authPassword)
                        .addOnCompleteListener(StartLoadingActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {

                                    loggon();

                                } else {

                                    Log.w("SafeLog", "signInWithEmail:failure", task.getException());
                                    Toast.makeText(StartLoadingActivity.this, "Authentication failed. Check Internet Connection",
                                            Toast.LENGTH_SHORT).show();
                                }



                            }
                        });

            } else
            {
               Toast.makeText(StartLoadingActivity.this,"Turn on your location to use Enrout Partner",Toast.LENGTH_SHORT).show();

                       finish();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults){
        switch (requestCode){
            case 1: {
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    if (ContextCompat.checkSelfPermission(StartLoadingActivity.this,
                            Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED)
                    {
                        do_LocationCheck();
                    }
                }else
                    {
                        Toast.makeText(StartLoadingActivity.this,"Grant location access to use Enrout Partner",Toast.LENGTH_SHORT).show();

                        finish(); }
                return;
            }
        }
    }
}
