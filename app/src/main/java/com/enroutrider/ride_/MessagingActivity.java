package com.enroutrider.ride_;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.enroutrider.ride_.model.Chat;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.FirebaseFirestore;

import co.intentservice.chatui.ChatView;
import co.intentservice.chatui.models.ChatMessage;
import id.voela.actrans.AcTrans;


public class MessagingActivity extends Activity {

    SharedPreferences settings;
    TextView cancel;
    ImageView go_back, call_customer;
    String the_number;
    ChatView chatView;
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.messaging);

        go_back = findViewById(R.id.go_back);
        call_customer = findViewById(R.id.call_customer);

     //   mSocket.connect();


      chatView = (ChatView) findViewById(R.id.chat_view);



         settings = getSharedPreferences("details",
                Context.MODE_PRIVATE);


       final boolean type= settings.getBoolean("tripType",true);


       // it is this activity instead

        call_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(type){

                    String sender_number = settings.getString("senderPhoneNumber", "");

                    callPhoneNumber(sender_number);
                }

                else
                    {

                        String sender_number = settings.getString("recipientNumber","");
                        callPhoneNumber(sender_number);
                    }


            }
        });

        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AcTrans.Builder(MessagingActivity.this).performSlideToRight();
               finish();
            }
        });


        String rider_id = settings.getString("enrout_rider_id","");
        String customer_id = settings.getString("senderId","");
        myRef = database.getReference("messages/"+customer_id+rider_id);

        myRef.keepSynced(true);

        chatView.setOnSentMessageListener(new ChatView.OnSentMessageListener(){
            @Override
            public boolean sendMessage(ChatMessage chatMessage){
                // perform actual message sending

                String rider_id = settings.getString("enrout_rider_id","");
                String customer_id = settings.getString("senderId","");


                Chat value =  new Chat(chatMessage.getMessage(), rider_id, customer_id);

                chatMessage.setType(ChatMessage.Type.SENT);

               DatabaseReference newRef = myRef.push();
               newRef.setValue(value);

                return true;
            }
        });





//        // Read from the database
//        myRef.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                try {
//                    Chat value = dataSnapshot.getValue(Chat.class);
//
//                    if (!value.sentBy.equals("rider")  && !value.sentBy.equals(null)) {
//                        //this message is sent by customer
//                        SharedPreferences.Editor editor = settings.edit();
//                        editor.putString("newMessage", value.messageText);
//                        editor.apply();
//
//
//                        Log.d("Tag", "Value is: " + new Gson().toJson(value));
//                    } else {
//
//                    }
//
//                }catch (NullPointerException e){}
//            }
//
//            @Override
//            public void onCancelled(DatabaseError error) {
//                // Failed to read value
//             //   Log.w(TAG, "Failed to read value.", error.toException());
//            }
//        });


    }


    public void callPhoneNumber(String number)
    {

        the_number = number;

        Intent callIntent = new Intent(Intent.ACTION_DIAL,Uri.parse("tel:" + number));
        startActivity(callIntent);

    }

    @Override
    protected void onResume() {
        super.onResume();

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                updateUI(dataSnapshot);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
               // Log.d(Constants.TAG, "onCancelled: " + databaseError);
            }
        });
    }

    private void updateUI(DataSnapshot dataSnapshots) {

        chatView.clearMessages();

        for(DataSnapshot chatSnapShot : dataSnapshots.getChildren()){

            Chat chat = chatSnapShot.getValue(Chat.class);
            if (chat == null) continue;

            chatView.addMessage(new ChatMessage(chat.messageText, System.currentTimeMillis(),"customer".equals(chat.sentBy)
                    ? ChatMessage.Type.RECEIVED : ChatMessage.Type.SENT));


        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new AcTrans.Builder(this).performSlideToRight();
    }


}
