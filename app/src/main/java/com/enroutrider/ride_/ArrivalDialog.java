package com.enroutrider.ride_;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.enroutrider.ride_.network.ServiceCheck;
import com.google.firebase.firestore.FirebaseFirestore;


public class ArrivalDialog extends Activity {

    SharedPreferences settings;
    TextView cancel,arrival_location;
    Button arrived;

    ServiceCheck service = new ServiceCheck();

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflate = LayoutInflater.from(getApplicationContext());
        View view = inflate.inflate(R.layout.confirm_arrival, null);
        builder.setCustomTitle(view);

         settings = getSharedPreferences("details",
                Context.MODE_PRIVATE);

        final Boolean tripType = settings.getBoolean("tripType",true);

         arrived = (Button)view.findViewById(R.id.confirm);

         cancel = (TextView) view.findViewById(R.id.cancel);

         arrival_location = (TextView)view.findViewById(R.id.arrival_location);

        arrival_location.setText(settings.getString("geolocation_name","the location"));

        if(settings.getBoolean("checkWhetherActed",false))
            arrived.setVisibility(View.GONE);

        arrived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences.Editor editor = settings.edit();

                editor.putInt("check_geofence", 0);

                editor.putBoolean("showAgain", false);

                editor.putBoolean("calculate_wait_amount", true);

                editor.apply();

                Boolean running = service.isMyServiceRunning(RiderGetLocationAndRequestService.class, ArrivalDialog.this);

                if(!running){

                    Intent intent = new Intent(getBaseContext(),RiderGetLocationAndRequestService.class);

                    startService(intent);
                }


            if(tripType)
                {
                    db.collection("PICKUPPOINTS").document(settings.getString("arrivalIdToUpdate","").trim()).update("isArrived", true);

                }
                else
                    {
                        db.collection("DESTINATIONS").document(settings.getString("arrivalIdToUpdate","").trim()).update("isArrived", true);

            }


               finish();


            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                SharedPreferences.Editor editor = settings.edit();

                editor.putInt("check_geofence", 0);

                editor.putBoolean("showAgain", true);

                editor.putBoolean("calculate_wait_amount", false);

                editor.apply();


                Boolean running = service.isMyServiceRunning(RiderGetLocationAndRequestService.class, ArrivalDialog.this);

                if(!running){

                    Intent intent = new Intent(getBaseContext(),RiderGetLocationAndRequestService.class);

                    startService(intent);
                }




                finish();
            }
        });

        Dialog alert = builder.create();
        alert.setCancelable(false);
        alert.show();



    }

}
