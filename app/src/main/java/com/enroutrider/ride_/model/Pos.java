package com.enroutrider.ride_.model;

import com.google.android.gms.maps.model.LatLng;

public class Pos {
    public LatLng geopoint;
    public String geohash;
}
