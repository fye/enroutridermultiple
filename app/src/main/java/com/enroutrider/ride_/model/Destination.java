package com.enroutrider.ride_.model;

/**
 * Created by Fortress on 6/9/2019.
 */

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.firestore.DocumentReference;

import java.util.ArrayList;
import java.util.List;

public class Destination {



    public String destinationId;
    public boolean isDelivered;
    public Pos pos;
    public String recipientName;
    public String recipientNumber;
    public int individualPrice;
    public LatLng latLng;

    public int done_img;
    public String placeName;



    public Destination(int done, String placeName, String destinationId, boolean isDelivered, String recipientName, String recipientNumber, int individualPrice, LatLng latLng) {
        super();
        this.setDone(done);
        this.setPlaceName(placeName);
        this.setDestinationId(destinationId);
        this.setDelivered(isDelivered);
        this.setRecipientName(recipientName);
        this.setRecipientNumber(recipientNumber);
        this.setIndividualPrice(individualPrice);
        this.setLatLng(latLng);
    }

    public String getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public int getDone() {
        return done_img;
    }

    public void setDone(int done_img) {
        this.done_img = done_img;
    }


    public boolean getDelivered() {
        return isDelivered;
    }

    public void setDelivered(boolean delivered) {
        isDelivered = delivered;
    }



    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getRecipientNumber() {
        return recipientNumber;
    }

    public void setRecipientNumber(String recipientNumber) {
        this.recipientNumber = recipientNumber;
    }

    public int getIndividualPrice() {
        return individualPrice;
    }

    public void setIndividualPrice(int individualPrice) {
        this.individualPrice = individualPrice;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }
}