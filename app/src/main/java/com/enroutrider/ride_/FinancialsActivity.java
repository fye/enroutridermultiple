package com.enroutrider.ride_;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.annotation.NonNull;
import android.content.Context;
import android.content.SharedPreferences;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import android.widget.TextView;
import android.widget.Toast;

import com.enroutrider.ride_.network.ServiceCheck;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.apache.commons.lang3.time.DurationFormatUtils;

import java.text.SimpleDateFormat;

import java.util.Locale;
import java.util.Map;

import id.voela.actrans.AcTrans;

public class FinancialsActivity extends AppCompatActivity {

    KProgressHUD progress_dialog;
    SharedPreferences settings;
private MyBroadcastREceiver myREceiver;

    TextView fareAmountText, tripsCount, totalCostText, payforme, enroutfeeText, promoText, dateRange, payout, hours;

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    ServiceCheck service = new ServiceCheck();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.financials_ui);

        tripsCount = findViewById(R.id.earningsCount);
        fareAmountText = findViewById(R.id.fare);
        totalCostText = findViewById(R.id.totalCost);
        payforme = findViewById(R.id.payforme);
        enroutfeeText = findViewById(R.id.enroutFee);
        promoText = findViewById(R.id.promo);
        dateRange = findViewById(R.id.daterange);
        payout = findViewById(R.id.payout);

        hours = findViewById(R.id.hours);

        settings = this.getSharedPreferences("details",
                Context.MODE_PRIVATE);

        startService(new Intent(getBaseContext(), RiderGetLocationAndRequestService.class));

        getTripCount();


    }


    public void getTripCount(){

        String SCHEDULEID = settings.getString("riderScheduleId","-").trim();


        db.collection("SCHEDULES").document(SCHEDULEID).get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                if (task.getResult() != null && task.getResult().exists()) {

                    Map<String, Object> tripdetails = task.getResult().getData();

                    if (tripdetails == null) return;

                    Timestamp endDate = (Timestamp) tripdetails.get("endDate");
                    Timestamp StartDate = (Timestamp) tripdetails.get("startDate");

                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());

                    dateRange.setText(""+sdf.format(StartDate.toDate())+" - "+ ""+sdf.format(endDate.toDate()));
                }



            }
        })
         .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("Failure", "Failed", e);

                Toast.makeText(getApplicationContext(), "Kindly check connection", Toast.LENGTH_LONG).show();
                return;

            }
        });


        tripsCount.setText(""+settings.getInt("tripCount",0));

        fareAmountText.setText(""+settings.getFloat("finalFareFinance",0));
        payforme.setText(""+ settings.getFloat("pfmFinance",0));
        promoText.setText(""+settings.getFloat("promoFinance",0));
        enroutfeeText.setText(""+settings.getFloat("enroutFeeFinance",0));
        totalCostText.setText(""+settings.getFloat("totalFinance",0));
        payout.setText(""+settings.getFloat("enroutFeeFinance",0));

    }

    public class MyBroadcastREceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();

            long onlineTime = b.getLong("TotalTime");

            String hoursonlineHrs = DurationFormatUtils.formatDuration(onlineTime, "HH' hr' mm' min'");

            hours.setText(hoursonlineHrs);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        myREceiver= new MyBroadcastREceiver();
        final IntentFilter intentFilter = new IntentFilter("BroadcastTime");
        LocalBroadcastManager.getInstance(this).registerReceiver(myREceiver,intentFilter);
    }


    @Override
    public void onPause() {
        super.onPause();
        dialogdismiss();

        if(myREceiver!=null)
            LocalBroadcastManager.getInstance(this).unregisterReceiver(myREceiver);
        myREceiver=null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new AcTrans.Builder(this).performSlideToRight();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        dialogdismiss();

    }

    public void dialogdismiss(){
        if (progress_dialog != null) {
            progress_dialog.dismiss();
        }
    }

}