package com.enroutrider.ride_.model;

import com.google.android.gms.maps.model.LatLng;

public class PickupView {

    public boolean picked = false;

    public String pickUpId;
    public boolean isPickedup;

    public String place;

    public String itemCategory;
    public String instruction;

    public LatLng latLng;


    public boolean getitemPicked() {
        return picked;
    }

    public void setitemPicked(Boolean picked) {
        this.picked = picked;
    }

}