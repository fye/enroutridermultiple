package com.enroutrider.ride_;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;


import com.kaopiz.kprogresshud.KProgressHUD;

public class TransferComplete extends AppCompatActivity {


    KProgressHUD progress_dialog;

    Button complete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transfer_complete);


            complete = findViewById(R.id.payment_completed);


        complete.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        Intent intent = new Intent(getApplicationContext(),StartLoadingActivity.class);
        //         Toast.makeText(getApplicationContext(),"Registration Successful, Please visit our Office to complete Registration"+result.getString("usertoken"),Toast.LENGTH_LONG).show();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);

        finish();

    }
});

    }


    @Override
    public void onStop() {
        super.onStop();

        dialogdismiss();
    }


    @Override
    public void onPause() {
        super.onPause();

        dialogdismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        dialogdismiss();
    }

    public void dialogdismiss(){
        if (progress_dialog != null) {
            progress_dialog.dismiss();
        }
    }

}