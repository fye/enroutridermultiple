package com.enroutrider.ride_;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.enroutrider.ride_.model.CustomArrayDelayed;
import com.enroutrider.ride_.model.Delayed;
import com.enroutrider.ride_.model.DelayedDestinationView;
import com.enroutrider.ride_.model.DelayedPickupView;
import com.enroutrider.ride_.network.ServiceCheck;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import id.voela.actrans.AcTrans;


public class DelayedTrips extends AppCompatActivity {


    FirebaseFirestore db = FirebaseFirestore.getInstance();
    Gson gson = new Gson();
    SharedPreferences settings;

    int detinationForPickupCounter = 0;
    int pickupsForARequestCounter = 0;
    Delayed places;
    RelativeLayout no_trip;
    KProgressHUD dialog;

    ListView listView;

    private CustomArrayDelayed customArrayDelayed;

    ServiceCheck service = new ServiceCheck();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.trip_delayed);

        settings =  getSharedPreferences("details",
                Context.MODE_PRIVATE);

        listView = (ListView) findViewById(R.id.delayed_trip_list);

        no_trip = findViewById(R.id.no_trip);

        listenforDelayed();



    }


    public void listenforDelayed()
    {


        final  String rider_id =  settings.getString("enrout_rider_id","");


        db.collection("DISPATCH").whereEqualTo("isAccepted",false).whereEqualTo("timeCode",settings.getString("timeCode","-"))
                .whereEqualTo("status","DELAYED").limit(Constants.range)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {

                        final ArrayList<Delayed> delayedArrayList = new ArrayList<>();


                        List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();

                        if (documentSnapshots.isEmpty()) {
                            no_trip.setVisibility(View.VISIBLE);
                            listView.setVisibility(View.GONE);

                            return;

                        }

                        else
                            {
                                customArrayDelayed = new CustomArrayDelayed(DelayedTrips.this, R.layout.trip_summary_delayed);

                                listView.setAdapter(customArrayDelayed);

                                listView.setVisibility(View.VISIBLE);
                                no_trip.setVisibility(View.GONE);


                        for (DocumentSnapshot the_document : documentSnapshots) {
                            SharedPreferences.Editor editor = settings.edit();

                            String the_documentId = the_document.getId();

                            Map<String, Object> values_of_rider = the_document.getData();

                            boolean accepted = (boolean) values_of_rider.get("isAccepted");

                            ArrayList<String> rejected = (ArrayList<String>) values_of_rider.get("rejectedBy");


                            editor.putBoolean("acceptedDispatch", accepted);
                            editor.apply();


                            if (!accepted && !rejected.contains(rider_id)) {

                                final DocumentReference request_ref = (DocumentReference) values_of_rider.get("requestId");

                                final String request_id = request_ref.getId();

                                request_ref.addSnapshotListener(new EventListener<DocumentSnapshot>() {
                                    @Override
                                    public void onEvent(@Nullable DocumentSnapshot snapshot,
                                                        @Nullable FirebaseFirestoreException e) {
                                        if (e != null) {
                                            Log.w("Tag", "Listen failed.", e);
                                            return;
                                        }

                                        if (snapshot != null && snapshot.exists()) {


                                            final Map<String, Object> trip_details = snapshot.getData();

                                            if (trip_details == null) return;


                                            Log.d("Tag", "Current data: is not null");

                                            settings = getSharedPreferences("details",
                                                    Context.MODE_PRIVATE);

                                            String document_id = the_documentId.trim();

                                            List<DocumentReference> arrayListPickups = (List) trip_details.get("pickups");

                                            getPickUpsOfAGivenRequest(arrayListPickups, new OnPickUpsGathered() {
                                                @Override
                                                public void pickupsGathered(ArrayList<DelayedPickupView> pickupArrayList) {

                                                    String stringsOfPickUps = gson.toJson(pickupArrayList);
                                                    String all_pickups = stringsOfPickUps.replace("place", "").
                                                            replaceAll("[\\[\\](\\){\\}:]", "").replace("\"", "");


                                                    List<DocumentReference> arrayListDestinations = (List) trip_details.get("destinations");

                                                    getDestinationsOfAGivenPickup(arrayListDestinations, new OnDestinationsGathered() {
                                                        @Override
                                                        public void destinationsGathered(ArrayList<DelayedDestinationView> destinations) {

                                                            String stringsOfDestinations = gson.toJson(destinations);

                                                            String all_destinations = stringsOfDestinations.replace("destinations", "").
                                                                    replaceAll("[\\[\\](\\){\\}:]", "").replace("\"", "");


                                                            places = new Delayed(document_id,request_id,all_pickups, all_destinations);



                                                            places.dispatchId = document_id;
                                                            places.requestId = request_id;
                                                            places.place=all_pickups;
                                                            places.destinations=all_destinations;

                                                            delayedArrayList.add(places);

                                                            customArrayDelayed.add(places);

                                                            dialogdismiss();


                                                            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                                @Override
                                                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                                                    Delayed the_selectedDelayTrip = delayedArrayList.get(i);
                                                                    //Toast.makeText(DelayedTrips.this,"List Clicked",Toast.LENGTH_SHORT).show();

                                                                    proceesing_acceptance(the_selectedDelayTrip.dispatchId,
                                                                            the_selectedDelayTrip.requestId);
                                                                }
                                                            });

                                                        }

                                                    });
                                                }
                                            });


                                        }

                                    }
                                });


                            }

                            else continue;

                            customArrayDelayed.notifyDataSetChanged();
                        }

                    }

                    }


                });

    }

    private void proceesing_acceptance(String DispatchId,String request_id ){


        String rider_id =  settings.getString("enrout_rider_id","");
        String rider_name =  settings.getString("rider_fullname","");
        String rider_phoneNumber =  settings.getString("phoneNumber","");
        String fleetID =  settings.getString("fleetID","").trim();
        String rider_plateNumber =  Constants.plateNumber;
        String riderImageURL = Constants.imagedownloadUrl;

        db.collection("DISPATCH").document(DispatchId).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {

                Map<String, Object> values_of_rider = documentSnapshot.getData();

                boolean accepted = (boolean) values_of_rider.get("isAccepted");
                String acceptedBy = (String)values_of_rider.get("acceptedBy");


                if(!accepted && acceptedBy.trim().equals(""))
                {
                    db.collection("DISPATCH").document(DispatchId.trim()).update("isAccepted", true, "status", "ONGOING", "acceptedBy",rider_id.trim()).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {


                            dialogdismiss();

                            db.collection("DRIVERS").document(rider_id.trim()).update("isWorking",true);

                            db.collection("REQUEST").document(request_id.trim()).update("status","ONGOING","riderId",rider_id.trim()
                                    ,"riderName",rider_name,"riderPhoneNumber",rider_phoneNumber,"fleetId",fleetID,"riderPlateNumber",rider_plateNumber,"isAccepted",true, "riderImage",riderImageURL,"acceptedAt", FieldValue.serverTimestamp());


                            Boolean running = service.isMyServiceRunning(RiderGetLocationAndRequestService.class, DelayedTrips.this);

                            if(!running){
                                startService(new Intent(getBaseContext(), RiderGetLocationAndRequestService.class));
                            }


                            Intent intent = new Intent(DelayedTrips.this, ProcessTrip.class);

                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                            startActivity(intent);
                            new AcTrans.Builder(DelayedTrips.this).performSlideToLeft();
                            finish();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                            dialogdismiss();
                            finish();

                            // stopSound();

                            Toast.makeText(getApplicationContext(), "The request could not be accepted. Check your network connection", Toast.LENGTH_LONG).show();

                        }
                    });

                }

                else {



                    Toast.makeText(getApplicationContext(), "The request has been accepted by another rider", Toast.LENGTH_LONG).show();


                    db.collection("DRIVERS").document(rider_id.trim()).update("isWorking", false);

                    Boolean running = service.isMyServiceRunning(RiderGetLocationAndRequestService.class, DelayedTrips.this);

                    if(!running){
                        startService(new Intent(getBaseContext(), RiderGetLocationAndRequestService.class));
                    }

                    finish();

                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                Toast.makeText(getApplicationContext(), "Processing failed. Check Internet connection", Toast.LENGTH_LONG).show();

            }
        });
    }

    private void getPickUpsOfAGivenRequest(final List <DocumentReference> arrayListPickups, final OnPickUpsGathered onPickUpsGathered){

        pickupsForARequestCounter = 0;
        final ArrayList<DelayedPickupView> pickupsArrayList = new ArrayList<>();

        for (final DocumentReference pickup_references : arrayListPickups ) {


            pickup_references.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                    if (task.getResult() != null && task.getResult().exists()) {

                        Map<String, Object> pickup_details = task.getResult().getData();

                        if (pickup_details == null) return;


                        String pickupPlace = (String) pickup_details.get("place");


                        final DelayedPickupView delayedpickup = new DelayedPickupView();

                        delayedpickup.place = pickupPlace;


                        pickupsArrayList.add(delayedpickup);

                        pickupsForARequestCounter ++;

                        if (pickupsForARequestCounter >= (arrayListPickups.size())){
                            onPickUpsGathered.pickupsGathered(pickupsArrayList);


                        }

                    }

                }
            });




        }
    }


    private void getDestinationsOfAGivenPickup(final List<DocumentReference> arrayListDestinations, final OnDestinationsGathered onDestinationsGathered){

        final ArrayList<DelayedDestinationView> destinationsArrayList = new ArrayList<>();
        detinationForPickupCounter = 0;

        for(final DocumentReference destination_references : arrayListDestinations){

            destination_references.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {



                    if (task.getResult() != null && task.getResult().exists()) {

                        String destinationsId = destination_references.getId();

                        Map<String, Object> destination_details = task.getResult().getData();

                        if (destination_details == null) return;


                        String destinationPlace = (String) destination_details.get("place");


                        DelayedDestinationView destination = new DelayedDestinationView();

                        destination.destinations = destinationPlace;


                        destinationsArrayList.add(destination);


                        detinationForPickupCounter++;

                        if (detinationForPickupCounter >= (arrayListDestinations.size())){
                            onDestinationsGathered.destinationsGathered(destinationsArrayList);
                        }



                    }

                }
            });


        }
    }

    private interface OnDestinationsGathered{
        void destinationsGathered(ArrayList<DelayedDestinationView> destinations);
    }

    private interface OnPickUpsGathered{
        void pickupsGathered(ArrayList<DelayedPickupView> pickupArrayList);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    public void onStop() {
        super.onStop();

        dialogdismiss();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        dialogdismiss();
    }

    public void dialogdismiss(){
        if (dialog != null) {
            dialog.dismiss();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new AcTrans.Builder(this).performSlideToRight();
    }

}
