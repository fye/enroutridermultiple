package com.enroutrider.ride_.dbModels;

public class TripCalculations {

    public double promo_percentage(double percent_value, double tripCost){

        double promocost = percent_value*tripCost;

        return promocost;
    }


    public double enrout_fee(double finalCost, double promoValue, double earnings){

        double enroutfee = (finalCost+promoValue) - earnings;

        return enroutfee;
    }


}
