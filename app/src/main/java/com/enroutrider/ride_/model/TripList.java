package com.enroutrider.ride_.model;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class TripList {

    public double baseFare;
    public String deliveryLocation ;
    public double DistanceCost;
    public double earning;
    public String paymentMethod;
    public String pickupLocation;
    public  String senderName;
    public String senderNumber;
    public String tripstatus;
    public double timeCost;
    public double tripCost;
    public double finalCost;
    public String tripDate;
    public double waitTime;
    public String waitDuration;
    public double payForme;

    @ServerTimestamp
    public Date createdAt;


    public TripList(double basefare, String deliveryLocation, double DistanceCost, double earning, String paymentMethod, String pickupLocation, String senderName
    , String senderNumber, String tripstatus, double timeCost, double tripCost, String tripDate, double waitTIme, double finalCost, String waitDuration, double payForme) {
        super();
        this.setBaseFare(basefare);
        this.setDeliveryLocation(deliveryLocation);
        this.setDistanceCost(DistanceCost);
        this.setEarning(earning);
        this.setPaymentMethod(paymentMethod);
        this.setPickupLocation(pickupLocation);
        this.setSenderName(senderName);
        this.setPayForMe(payForme);
        this.setSenderNumber(senderNumber);
        this.setTripstatus(tripstatus);
        this.setTimeCost(timeCost);
        this.setTripCost(tripCost);
        this.setTripDate(tripDate);
        this.setWaitTime(waitTIme);
        this.setFinalCost(finalCost);
        this.setWaitDuration(waitDuration);

    }

    public String getWaitDuration() {
        return waitDuration;
    }

    public void setWaitDuration(String waitDuration) {
        this.waitDuration = waitDuration;
    }

    public void setPayForMe(double payForMe) {
        this.payForme = payForMe;
    }

    public double getPayForMe() {
        return payForme;
    }

    public double getFinalCost() {
        return finalCost;
    }

    public void setFinalCost(double FinalCost) {
        this.finalCost = FinalCost;
    }

    public double getBaseFare() {
        return baseFare;
    }

    public void setBaseFare(double baseFare) {
        this.baseFare = baseFare;
    }

    public String getDeliveryLocation() {
        return deliveryLocation;
    }

    public void setDeliveryLocation(String deliveryLocation) {
        this.deliveryLocation = deliveryLocation;
    }

    public double getDistanceCost() {
        return DistanceCost;
    }

    public void setDistanceCost(double distanceCost) {
        DistanceCost = distanceCost;
    }

    public double getEarning() {
        return earning;
    }

    public void setEarning(double earning) {
        this.earning = earning;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderNumber() {
        return senderNumber;
    }

    public void setSenderNumber(String senderNumber) {
        this.senderNumber = senderNumber;
    }

    public String getTripstatus() {
        return tripstatus;
    }

    public void setTripstatus(String tripstatus) {
        this.tripstatus = tripstatus;
    }

    public double getTimeCost() {
        return timeCost;
    }

    public void setTimeCost(double timeCost) {
        this.timeCost = timeCost;
    }

    public double getTripCost() {
        return tripCost;
    }

    public void setTripCost(double tripCost) {
        this.tripCost = tripCost;
    }

    public String getTripDate() {
        return tripDate;
    }

    public void setTripDate(String tripDate) {
        this.tripDate = tripDate;
    }

    public double getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(double waitTime) {
        this.waitTime = waitTime;
    }



}