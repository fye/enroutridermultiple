package com.enroutrider.ride_;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import id.voela.actrans.AcTrans;


public class DialogActivity extends AppCompatActivity {

    Button the_ok_button;
    TextView the_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_activity);
        this.setFinishOnTouchOutside(false);
        the_ok_button  = (Button) findViewById(R.id.dialog_ok_button);
        the_text=(TextView)findViewById(R.id.textme) ;
        the_ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i=new Intent(getApplicationContext(),MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivityForResult(i,0);
                new AcTrans.Builder(DialogActivity.this).performSlideToLeft();
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent i=new Intent(getApplicationContext(),MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(i,0);
        new AcTrans.Builder(this).performSlideToRight();
        finish();
        super.onBackPressed();
    }
}
