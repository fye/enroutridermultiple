package com.enroutrider.ride_;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.enroutrider.ride_.model.CustomTripAdapter;
import com.enroutrider.ride_.model.TripList;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.kaopiz.kprogresshud.KProgressHUD;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import id.voela.actrans.AcTrans;


public class RiderHistoryActivity extends AppCompatActivity {
    KProgressHUD dialog;
    EditText inputSearch;
    SharedPreferences settings;
    Constants constants = new Constants();
    private CustomTripAdapter adapter;
    private ListView listView;
    DocumentSnapshot  lastVisible;

    FirebaseFirestore db = FirebaseFirestore.getInstance();


     TripList tripInfo;

     ArrayList<TripList> TripDetails = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_history_list);
        listView = (ListView) findViewById(R.id.lst_ride_history);
        inputSearch = findViewById(R.id.inputSearch);

        settings = getSharedPreferences("details",
                Context.MODE_PRIVATE);

        listView.addFooterView(new ProgressBar(this));


        adapter = new CustomTripAdapter(TripDetails,getApplicationContext(),R.layout.activity_ride_history);

        listView.setAdapter(adapter);

        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {


                RiderHistoryActivity.this.adapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) { }

            @Override
            public void afterTextChanged(Editable arg0) {}
        });



        ListItems();

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //     getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public void onStop() {
        super.onStop();

        dialogdismiss();
    }


    @Override
    public void onPause() {
        super.onPause();

        dialogdismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        dialogdismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new AcTrans.Builder(this).performSlideToRight();
    }


    public void dialogdismiss(){
        if (dialog != null) {
            dialog.dismiss();
        }
    }


    public void ListItems(){

        final  String rider_id =  settings.getString("enrout_rider_id","");

        db.collection("TRIPAUDITS")
                .whereEqualTo("driverReference",rider_id.trim()).orderBy("finishedAt", Query.Direction.DESCENDING)
                .limit(Constants.tripView)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if (task.getResult().isEmpty())
                            {

                                Toast.makeText(RiderHistoryActivity.this,"You have not embarked on a trip", Toast.LENGTH_SHORT).show();
                                return;

                            }
                            else{

                                QuerySnapshot querySnapshot = task.getResult();

                                List<DocumentSnapshot> documentSnapshots = querySnapshot.getDocuments();



                                for (DocumentSnapshot  documentSnapshot : documentSnapshots ) {

                                    Map<String, Object> actualData = documentSnapshot.getData();

                                  double basefare = (double)actualData.get("basefare");
                                  final String deliveryLocation = (String) actualData.get("deliveryLocation");
                                  double distanceCost = (double)actualData.get("distanceCost");
                                  double earnings = (double)actualData.get("earning");
                                  String paymentMethod = (String)actualData.get("paymentMethod");
                                  String pickupLocation = (String)actualData.get("pickupLocation");
                                  String senderName = (String)actualData.get("senderName");
                                  String senderNumber = (String)actualData.get("senderNumber");
                                  String status = (String)actualData.get("status");
                                  double payForMe = ((Number)actualData.get("payForMe")).doubleValue();
                                  double timeCost = (double)actualData.get("timeCost");
                                  double tripCost = (double)actualData.get("tripCost");
                                  String tripDate = (String)actualData.get("tripDate");
                                  double waitTime = (double)actualData.get("waitTime");
                                  double finalCost = (double)actualData.get("finalCost");
                                  String waitDurationTime = (String)actualData.get("waitDuration");


                                    tripInfo = new TripList(basefare,deliveryLocation,distanceCost,earnings,paymentMethod,pickupLocation,
                                            senderName,senderNumber,status,timeCost,tripCost,tripDate,waitTime,finalCost,waitDurationTime,payForMe);

                                    tripInfo.baseFare = basefare;
                                    tripInfo.deliveryLocation= deliveryLocation;
                                    tripInfo.DistanceCost = distanceCost;
                                    tripInfo.earning = earnings;
                                    tripInfo.paymentMethod = paymentMethod;
                                    tripInfo.pickupLocation = pickupLocation;
                                    tripInfo.senderName = senderName;
                                    tripInfo.senderNumber = senderNumber;
                                    tripInfo.tripstatus = status;
                                    tripInfo.timeCost = timeCost;
                                    tripInfo.tripCost = tripCost;
                                    tripInfo.tripDate = tripDate;
                                    tripInfo.payForme = payForMe;
                                    tripInfo.waitTime = waitTime;
                                    tripInfo.finalCost= finalCost;
                                    tripInfo.waitDuration=waitDurationTime;

                                    TripDetails.add(tripInfo);



                                adapter.add(tripInfo);


                                inputSearch.setVisibility(View.VISIBLE);

                                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                            TripList the_selected_trip = TripDetails.get(i);

                                            Intent intent = new Intent(RiderHistoryActivity.this, RiderTripDetailActivity.class);

                                                    intent.putExtra("basefare", the_selected_trip.getBaseFare());
                                                    intent.putExtra("deliverylocation", the_selected_trip.getDeliveryLocation());
                                                    intent.putExtra("distanceCost",the_selected_trip.getDistanceCost());
                                                    intent.putExtra("earning",the_selected_trip.getEarning());
                                                    intent.putExtra("paymentMethod", the_selected_trip.getPaymentMethod());
                                                    intent.putExtra("pickupLocation",the_selected_trip.getPickupLocation());
                                                    intent.putExtra("senderName", the_selected_trip.getSenderName());
                                                    intent.putExtra("timeCost", the_selected_trip.getTimeCost());
                                                    intent.putExtra("tripCost", the_selected_trip.getTripCost());
                                                    intent.putExtra("tripDate", the_selected_trip.getTripDate());
                                                    intent.putExtra("waitTime",the_selected_trip.getWaitTime());
                                                    intent.putExtra("payforMe", the_selected_trip.getPayForMe());
                                                    intent.putExtra("tripStatus", the_selected_trip.getTripstatus());
                                                    intent.putExtra("finalCost", the_selected_trip.getFinalCost());
                                                    intent.putExtra("waitDuration_Span",the_selected_trip.getWaitDuration());

                                            startActivity(intent);
                                            new AcTrans.Builder(RiderHistoryActivity.this).performSlideToLeft();
                                        }
                                    });

                                }


                              lastVisible = task.getResult().getDocuments().get(task.getResult().size() - 1);

                              listView.setOnScrollListener(new AbsListView.OnScrollListener() {
                                  @Override
                                  public void onScrollStateChanged(AbsListView absListView, int i) {
                                      if (i == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                                              && (listView.getLastVisiblePosition() - listView.getHeaderViewsCount() -
                                              listView.getFooterViewsCount()) >= (adapter.getCount() - 1))
                                      {

                                          repeat_Scroll(rider_id);

                                      }
                                  }

                                  @Override
                                  public void onScroll(AbsListView absListView, int i, int i1, int i2) {

                                  }
                              });


                            }
                        } else {
                            Log.w("Tag", "Error connecting to server.", task.getException());
                        }
                    }
                });

    }

    public void repeat_Scroll(String rider_id)
    {
        db.collection("TRIPAUDITS")
                .whereEqualTo("driverReference",rider_id.trim()).orderBy("finishedAt", Query.Direction.DESCENDING)
                .startAfter(lastVisible).limit(Constants.tripView)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful()) {
                            if (task.getResult().isEmpty()) {

                                Toast.makeText(RiderHistoryActivity.this, "No more trips to load", Toast.LENGTH_SHORT).show();
                                return;

                            }

                            else{

                                QuerySnapshot querySnapshot = task.getResult();

                                List<DocumentSnapshot> documentSnapshots = querySnapshot.getDocuments();



                                for (DocumentSnapshot  documentSnapshot : documentSnapshots ) {

                                    Map<String, Object> actualData = documentSnapshot.getData();

                                    double basefare = (double)actualData.get("basefare");
                                    final String deliveryLocation = (String) actualData.get("deliveryLocation");
                                    double distanceCost = (double)actualData.get("distanceCost");
                                    double earnings = (double)actualData.get("earning");
                                    String paymentMethod = (String)actualData.get("paymentMethod");
                                    String pickupLocation = (String)actualData.get("pickupLocation");
                                    String senderName = (String)actualData.get("senderName");
                                    String senderNumber = (String)actualData.get("senderNumber");
                                    String status = (String)actualData.get("status");
                                    double payForMe = ((Number)actualData.get("payForMe")).doubleValue();
                                    double timeCost = (double)actualData.get("timeCost");
                                    double tripCost = (double)actualData.get("tripCost");
                                    String tripDate = (String)actualData.get("tripDate");
                                    double waitTime = (double)actualData.get("waitTime");
                                    double finalCost = (double)actualData.get("finalCost");
                                    String waitDurationTime = (String)actualData.get("waitDuration");


                                    tripInfo = new TripList(basefare,deliveryLocation,distanceCost,earnings,paymentMethod,pickupLocation,
                                            senderName,senderNumber,status,timeCost,tripCost,tripDate,waitTime,finalCost,waitDurationTime,payForMe);

                                    tripInfo.baseFare = basefare;
                                    tripInfo.deliveryLocation= deliveryLocation;
                                    tripInfo.DistanceCost = distanceCost;
                                    tripInfo.earning = earnings;
                                    tripInfo.paymentMethod = paymentMethod;
                                    tripInfo.pickupLocation = pickupLocation;
                                    tripInfo.senderName = senderName;
                                    tripInfo.senderNumber = senderNumber;
                                    tripInfo.tripstatus = status;
                                    tripInfo.timeCost = timeCost;
                                    tripInfo.tripCost = tripCost;
                                    tripInfo.tripDate = tripDate;
                                    tripInfo.payForme = payForMe;
                                    tripInfo.waitTime = waitTime;
                                    tripInfo.finalCost= finalCost;
                                    tripInfo.waitDuration=waitDurationTime;

                                    TripDetails.add(tripInfo);



                                    adapter.add(tripInfo);


                                    inputSearch.setVisibility(View.VISIBLE);

                                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                            TripList the_selected_trip = TripDetails.get(i);

                                            Intent intent = new Intent(RiderHistoryActivity.this, RiderTripDetailActivity.class);

                                            intent.putExtra("basefare", the_selected_trip.getBaseFare());
                                            intent.putExtra("deliverylocation", the_selected_trip.getDeliveryLocation());
                                            intent.putExtra("distanceCost",the_selected_trip.getDistanceCost());
                                            intent.putExtra("earning",the_selected_trip.getEarning());
                                            intent.putExtra("paymentMethod", the_selected_trip.getPaymentMethod());
                                            intent.putExtra("pickupLocation",the_selected_trip.getPickupLocation());
                                            intent.putExtra("senderName", the_selected_trip.getSenderName());
                                            intent.putExtra("timeCost", the_selected_trip.getTimeCost());
                                            intent.putExtra("tripCost", the_selected_trip.getTripCost());
                                            intent.putExtra("tripDate", the_selected_trip.getTripDate());
                                            intent.putExtra("waitTime",the_selected_trip.getWaitTime());
                                            intent.putExtra("payforMe", the_selected_trip.getPayForMe());
                                            intent.putExtra("tripStatus", the_selected_trip.getTripstatus());
                                            intent.putExtra("finalCost", the_selected_trip.getFinalCost());
                                            intent.putExtra("waitDuration_Span",the_selected_trip.getWaitDuration());

                                            startActivity(intent);
                                            new AcTrans.Builder(RiderHistoryActivity.this).performSlideToLeft();
                                        }
                                    });

                                }
                                adapter.notifyDataSetChanged();
                                lastVisible = task.getResult().getDocuments().get(task.getResult().size() - 1);

                            }
                        }

                    }
                });
    }


}