package com.enroutrider.ride_;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.enroutrider.ride_.model.DestinationView;

import com.enroutrider.ride_.model.PickupView;
import com.enroutrider.ride_.network.ServiceCheck;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import id.voela.actrans.AcTrans;


public class RiderViewTripActivity extends Activity {

   ProgressBar showLoad;
    MyReceiver myReceiver;
    private MediaPlayer mp;
    LinearLayout expandedView;
    Button acceptbutton,cancelbutton;
    List<String> listDataHeader, Destinations, items_;
    TextView senderName,costofdelivery,pickuplocname,deliverlocname,countdown_text, item_category;
    SharedPreferences settings;

//
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    ServiceCheck service = new ServiceCheck();

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub

        //Register BroadcastReceiver
        //to receive event from our service
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(RiderGetLocationAndRequestService.RIDER_STATE);
        registerReceiver(myReceiver, intentFilter);


        Boolean running = service.isMyServiceRunning(RiderGetLocationAndRequestService.class, RiderViewTripActivity.this);

        if(!running){
            startService(new Intent(getBaseContext(), RiderGetLocationAndRequestService.class));
        }

        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_rider_view_trip);

        getWindow().setBackgroundDrawable(new ColorDrawable(0));

         sendNotification(true, true);

        setFinishOnTouchOutside(false);

        cancelbutton = (Button) findViewById(R.id.cancelbutton);
        acceptbutton = (Button) findViewById(R.id.acceptbutton);

        showLoad = findViewById(R.id.show_loading);

        expandedView = findViewById(R.id.expandedView);

        senderName = (TextView)findViewById(R.id.sender_name);
        costofdelivery = (TextView)findViewById(R.id.cost);
        countdown_text=(TextView)findViewById(R.id.countdown_text) ;

        pickuplocname = (TextView)findViewById(R.id.pickuplocname);
        deliverlocname = (TextView)findViewById(R.id.deliverlocname);
        item_category = findViewById(R.id.pickup_items);

       // String DispatchId = getIntent().getStringExtra("DispatchId");

       settings = getSharedPreferences("details",
                Context.MODE_PRIVATE);

        String rider_id =  settings.getString("enrout_rider_id","");
        String rider_name =  settings.getString("rider_fullname","");
        String rider_phoneNumber =  settings.getString("phoneNumber","");
        String fleetID =  settings.getString("fleetID","").trim();
        String rider_plateNumber =  Constants.plateNumber;
        String riderImageURL = Constants.imagedownloadUrl;

        String request_id = settings.getString("the_request_id_VIEW","");

        String DispatchId = settings.getString("dispatch_id_VIEW","");

       final SharedPreferences.Editor editor = settings.edit();

       senderName.setText(settings.getString("senderName_VIEW",""));
       costofdelivery.setText(""+settings.getFloat("estimatedCost_VIEW",0));


       prepareListData();

        acceptbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showLoad.setVisibility(View.VISIBLE);
                acceptbutton.setVisibility(View.GONE);
                cancelbutton.setVisibility(View.GONE);


                db.collection("DISPATCH").document(DispatchId).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {

                        Map<String, Object> acceptTripCheck = documentSnapshot.getData();

                        if (acceptTripCheck == null) return;

                        boolean accepted = (boolean)acceptTripCheck.get("isAccepted");

                        if(!accepted)
                        {

                            db.collection("DRIVERS").document(rider_id.trim()).update("isWorking",true).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {

                                    db.collection("DISPATCH").document(DispatchId.trim()).update("isAccepted", true, "status", "ONGOING", "acceptedBy",rider_id.trim());

                                    db.collection("REQUEST").document(request_id.trim()).update("status","ONGOING","riderId",rider_id.trim()
                                            ,"riderName",rider_name,"riderPhoneNumber",rider_phoneNumber,"fleetId",fleetID,"riderPlateNumber",rider_plateNumber,"isAccepted",true, "riderImage",riderImageURL,"acceptedAt",FieldValue.serverTimestamp());

                                }
                            });

                       }


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        showLoad.setVisibility(View.GONE);
                        acceptbutton.setVisibility(View.VISIBLE);
                        cancelbutton.setVisibility(View.VISIBLE);

                        Toast.makeText(RiderViewTripActivity.this,
                                "Request could not be accepted\n Check your internet connection and try again", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });




        cancelbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showLoad.setVisibility(View.VISIBLE);
                acceptbutton.setVisibility(View.GONE);
                cancelbutton.setVisibility(View.GONE);

                stopSound();

              db.collection("DISPATCH").document(DispatchId).update("rejectedBy", FieldValue.arrayUnion(settings.getString("enrout_rider_id",""))).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        db.collection("REQUEST").document(request_id).update("rejectedBy", FieldValue.arrayUnion(settings.getString("enrout_rider_id","")));

                        editor.putBoolean("isFound", false);
                        editor.remove("PickupsFromService");
                        editor.remove("DestinationsFromService");

                        editor.apply();

                    }
                }).addOnFailureListener(new OnFailureListener() {
                  @Override
                  public void onFailure(@NonNull Exception e) {

                      showLoad.setVisibility(View.GONE);
                      acceptbutton.setVisibility(View.VISIBLE);
                      cancelbutton.setVisibility(View.VISIBLE);

                      Toast.makeText(RiderViewTripActivity.this,
                              "Request could not be cancelled\n Check your internet connection and try again", Toast.LENGTH_SHORT).show();

                  }
              });

            }
        });

    }


    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context arg0, Intent arg1) {
            // TODO Auto-generated method stub

            boolean other_rider_accepted = arg1.getBooleanExtra("other_rider",false);
            boolean accepted_by_me = arg1.getBooleanExtra("acceptedByMe",false);

         if(other_rider_accepted){
             stopSound();
             finish();
         }

        else if(accepted_by_me)
         {
             stopSound();

             Intent i = new Intent();
             i.setClassName(arg0.getPackageName(), ProcessTrip.class.getName());
             i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK );
             arg0.startActivity(i);
             new AcTrans.Builder(RiderViewTripActivity.this).performSlideToTop();
         }

        }

    }

    private void sendNotification( boolean sound, boolean vibrate) {
        Intent intent = new Intent(this, RiderViewTripActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Notification notification = new Notification();

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // String channelId = "channel-01";
        String channelId = "enrout-01";
        String channelName = "Enrout";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);


        if (sound) {

            notification.defaults |= Notification.DEFAULT_SOUND;

        }
        if (vibrate) {
            notification.defaults |= Notification.DEFAULT_VIBRATE;
        }


            notificationBuilder.setDefaults(notification.defaults);
        notificationBuilder.setSmallIcon(R.mipmap.outer_icon)
                .setContentTitle("Enrout")
                .setContentText("You Have A Request")
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);


        notificationManager.notify(0, notificationBuilder.build());

    }

    private void prepareListData() {
        listDataHeader = new ArrayList<>();
        Destinations = new ArrayList<>();
        items_ = new ArrayList<>();

        Gson gson = new Gson();
        String trip_elements = settings.getString("PickupsFromService_VIEW", "");
        String destination_elements = settings.getString("DestinationsFromService_VIEW", "");

        if (trip_elements.isEmpty()|| destination_elements.isEmpty()) {
            return;
        }
        else
        {
            Type type = new TypeToken<List<PickupView>>() {
            }.getType();

            Type type2 = new TypeToken<List<DestinationView>>() {
            }.getType();

            ArrayList<PickupView> PickupsArray = gson.fromJson(trip_elements, type);

            for(PickupView pickup : PickupsArray){

                String PickupPoint =  pickup.place;
                String items = pickup.itemCategory;

                listDataHeader.add(PickupPoint);
                items_.add(items);

                pickuplocname.setText(listDataHeader.toString());
                item_category.setText(items_.toString());


                SharedPreferences.Editor editor = settings.edit();

                editor.putString("array_pickups", listDataHeader.toString());

                editor.apply();

            }

            List<DestinationView> destinations =  gson.fromJson(destination_elements, type2); // this returns a list of destinations for this particular pickup

            for(DestinationView destination : destinations){
                String DestinationOfCurrentPickup =  destination.place;


                Destinations.add(DestinationOfCurrentPickup);

                deliverlocname.setText(Destinations.toString());

                SharedPreferences.Editor editor = settings.edit();

                editor.putString("array_destinations", Destinations.toString());

                editor.apply();

            }
            playSound(RiderViewTripActivity.this);
        }



    }

    public void toggle_contents_trip(View v) {

        if (!expandedView.isShown()) {
            ScrollAnime.slide_up(this, expandedView);
            expandedView.setVisibility(View.VISIBLE);
        } else {
            ScrollAnime.slide_down(this, expandedView);
            expandedView.setVisibility(View.GONE);
        }

    }

  @Override
  public void onStop(){
      super.onStop();

      unregisterReceiver(myReceiver);
      stopSound();
  }
    @Override
    public void onPause(){
        super.onPause();
    }

    public void stopSound()
    {
        if (mp != null) {
            if (mp.isPlaying()|| mp.isLooping()) {
                mp.stop();
                mp.reset();

            }
        }
    }

    public void finish() {
        super.finish();
    }

    public void playSound(Context context) {

        mp = MediaPlayer.create(context, R.raw.rider_sound_noise);
        mp.start();
       // mp.setLooping(true);

        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(500);
        }
    }


    @Override
    public void onDestroy() {

        super.onDestroy();

        stopSound();
    }





}
