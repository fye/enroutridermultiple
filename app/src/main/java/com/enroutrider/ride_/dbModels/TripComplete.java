package com.enroutrider.ride_.dbModels;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

/**
 * Created by Fortress on 5/26/2019.
 */

public class TripComplete {

    public float basefare;
    public String deliveryLocation;
    public float distanceCost;
    public  String driverReference;
    public float earning;
    public String paymentMethod;
    public String pickupLocation;
    public  String requestReference;
    public String senderName;
    public String senderNumber;
    public String status;
    public float  timeCost;
    public  float tripCost;
    public String tripDate;
    public float waitTime;
    public float finalCost;
    public float payForMe;
    public double couponAmount;
    public double enroutFee;
    public String scheduleId;
    public String waitDuration;

    @ServerTimestamp
    public Date finishedAt;
}
