package com.enroutrider.ride_;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.kaopiz.kprogresshud.KProgressHUD;


import java.util.ArrayList;

import id.voela.actrans.AcTrans;

public class IncentiveActivity extends AppCompatActivity {

    int hoursdone, tripsfinished,
            tripsaccepted;

    KProgressHUD progress_dialog;

    TextView incentiveapply;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incentive);



        SharedPreferences settings = this.getSharedPreferences("details",
                Context.MODE_PRIVATE);

        drawchart();

    
    }


    public void drawchart(){


        PieChart pieChart = findViewById(R.id.piechart);
        ArrayList Incentives = new ArrayList();

       progress_dialog = KProgressHUD.create(IncentiveActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(Color.TRANSPARENT)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();




        Incentives.add(new PieEntry(hoursdone, "Hours Online"));
        Incentives.add(new PieEntry(tripsaccepted, "Trips Accepted"));
        Incentives.add(new PieEntry(tripsfinished, "Trips Completed"));

        PieDataSet dataSet = new PieDataSet(Incentives,"" );


        PieData data = new PieData(dataSet);
        pieChart.setData(data);
        dataSet.setColors(new int[] {Color.BLUE, Color.GREEN, Color.GRAY});
        pieChart.animateXY(2000, 2000);


        dialogdismiss();

    }
    @Override
    public void onStop() {
        super.onStop();

        dialogdismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new AcTrans.Builder(this).performSlideToRight();
    }

    @Override
    public void onPause() {
        super.onPause();

        dialogdismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        dialogdismiss();
    }

    public void dialogdismiss(){
        if (progress_dialog != null) {
            progress_dialog.dismiss();
        }
    }
    
}