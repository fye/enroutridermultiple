package com.enroutrider.ride_.model;

public class PostLocationUpdates {

    public float latitude;
    public float longitude;

    public PostLocationUpdates(float latitude, float longitude)
    {
        this.latitude=latitude;
        this.longitude = longitude;
    }
}
