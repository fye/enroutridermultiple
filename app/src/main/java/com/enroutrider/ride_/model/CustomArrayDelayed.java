package com.enroutrider.ride_.model;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.enroutrider.ride_.R;

import java.util.ArrayList;
import java.util.List;

public class CustomArrayDelayed extends ArrayAdapter<Delayed> {

    private List<Delayed> PlaceList = new ArrayList<Delayed>();

    static class TripViewHolder {

        TextView PickupName;
        TextView DestinationName;

    }

    public CustomArrayDelayed(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    @Override
    public void add(Delayed object) {
        PlaceList.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return this.PlaceList.size();
    }

    @Override
    public Delayed getItem(int index) {
        return this.PlaceList.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        TripViewHolder viewHolder;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.trip_summary_delayed, parent, false);
            viewHolder = new TripViewHolder();

            viewHolder.DestinationName = (TextView) row.findViewById(R.id.destination_location);

            viewHolder.PickupName = (TextView) row.findViewById(R.id.pickup_location);


            row.setTag(viewHolder);
        } else {
            viewHolder = (TripViewHolder)row.getTag();
        }

        Delayed the_place = getItem(position);

        viewHolder.PickupName.setText(the_place.getPlace());

        viewHolder.DestinationName.setText(the_place.getDestinations());

        return row;
    }


}