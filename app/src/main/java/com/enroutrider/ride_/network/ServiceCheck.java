package com.enroutrider.ride_.network;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;

import java.util.Objects;

public class ServiceCheck {

    public boolean isMyServiceRunning(Class<?> serviceClass, Activity activity) {
        ActivityManager manager = (ActivityManager) Objects.requireNonNull(activity.getSystemService(Context.ACTIVITY_SERVICE));
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
