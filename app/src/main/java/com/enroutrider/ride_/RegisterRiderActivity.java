package com.enroutrider.ride_;

/**
 * Created by David on 19/08/2017.
 */

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.enroutrider.ride_.dbModels.RegistrationModule;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.kaopiz.kprogresshud.KProgressHUD;

public class RegisterRiderActivity extends AppCompatActivity  {



    KProgressHUD dialog ;
RadioButton male,female;
    public Constants constants = new Constants();

    EditText editEmail,editFullname,editPhone,editDateofBirth;
    RadioGroup gendergroup;
    Button btnSignIn;
    TextView already_registered_rider;
    SharedPreferences settings;
    private DatePickerDialog mDatePickerDialog;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    String gender ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_rider);
       gendergroup=(RadioGroup) findViewById(R.id.genderGroup);

        editEmail=(EditText)findViewById(R.id.id_email);
        editFullname=(EditText)findViewById(R.id.fullname);
        editPhone=(EditText)findViewById(R.id.phone);
        already_registered_rider=(TextView)findViewById(R.id.id_already_registered_rider);
        male = (RadioButton) findViewById(R.id.male);
        female = (RadioButton) findViewById(R.id.female);

        editDateofBirth = (EditText)findViewById(R.id.date_of_birth);

        setDateTimeField();

        editDateofBirth.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mDatePickerDialog.show();
                return false;
            }
        });

        btnSignIn=(Button)findViewById(R.id.signin);


        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(RegisterRiderActivity.this, "Please Wait", Toast.LENGTH_LONG).show();

                String email_pat = editEmail.getText().toString().trim();

                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";


                if (email_pat.matches(emailPattern)&& ((editPhone.getText().toString().trim().length()) == 10)
                        &&((editFullname.getText().toString().trim().length()) >0)&&
                        ((editDateofBirth.getText().toString().trim().length()) >0)&&(gendergroup.getCheckedRadioButtonId() != -1))

                {

                    if (male.isChecked()) {
                        gender = male.getText().toString();
                    } else if (female.isChecked()) {
                        gender = female.getText().toString();
                    }

                        register(editEmail.getText().toString(),editFullname.getText().toString(),editPhone.getText().toString(),editDateofBirth.getText().toString(),gender);
                }


                else
                {
                    Toast.makeText(getApplicationContext(),"Kindly fill in all fields correctly", Toast.LENGTH_SHORT).show();
                }


            }
        });

        already_registered_rider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), EnterEmailForVerificationActivity.class);
                startActivityForResult(intent,5);
            }
        });

    }

    public void register(String email,String fullname,String phone, String dateofBirth, String gender){

        dialog= KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setBackgroundColor(Color.TRANSPARENT)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();


       final RegistrationModule register= new RegistrationModule();

                register.fullname=fullname;
               register.emailAddress=email;
               register.phoneNumber = phone;
               register.isApproved=false;
               register.isWorking=false;
               register.dob=dateofBirth;
                register.gender=gender;
                register.isOnline = false;
                register.phoneRegistration = true;
                register.vehicle = "-";
                register.SOS = false;
                register.fleet = "-";
                register.averageRating = 0;
                register.totalNoOfVotes = 0;
                register.sumOfVotes = 0;
                register.withdrawalAmount = 0;
                register.scheduleId = "00";

        db.collection("DRIVERS")
                .whereEqualTo("emailAddress",email).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if(task.getResult().isEmpty())
                            {
                                Log.d("Tag", "No Rider Found");

                                db.collection("DRIVERS").document().set(register)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {

                                                dialog.dismiss();

                                                Intent intent = new Intent(getApplicationContext(),StartLoadingActivity.class);
                                                //         Toast.makeText(getApplicationContext(),"Registration Successful, Please visit our Office to complete Registration"+result.getString("usertoken"),Toast.LENGTH_LONG).show();
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_SINGLE_TOP);

                                                startActivity(intent);

                                                settings = getSharedPreferences("details",
                                                        Context.MODE_PRIVATE);

                                                SharedPreferences.Editor editor = settings.edit();

                                                editor.putString("verifyemail", editEmail.getText().toString());

                                                editor.apply();

                                                finish();
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Log.d("Failure", "Failed", e);

                                                dialog.dismiss();

                                                Toast.makeText(getApplicationContext(), "Unable to retrieve any data from server. Kindly check connection", Toast.LENGTH_LONG).show();


                                            }
                                        });;

                            }
                            else{

                                dialog.dismiss();

                                Toast.makeText(getApplicationContext(),"Sorry, this email Already Exists \n Click on LOGIN to enter application",Toast.LENGTH_LONG).show();
                                 settings = getSharedPreferences("details",
                                        Context.MODE_PRIVATE);


                                SharedPreferences.Editor editor = settings.edit();

                                editor.putString("verifyemail", "none");

                                editor.commit();



                            }
                        } else {
                            Log.w("Tag", "Error connecting to server.", task.getException());
                        }
                    }
                });




    }




    private void setDateTimeField() {

        Calendar newCalendar = Calendar.getInstance();
        mDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy");
                final Date startDate = newDate.getTime();
                String fdate = sd.format(startDate);

                editDateofBirth.setText(fdate);

            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        mDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());

    }

    @Override
    protected void onStop() {
        super.onStop();


        dialogdismiss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==5)
        {
           finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
    }


    @Override
    public void onPause() {
        super.onPause();

        dialogdismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        dialogdismiss();
    }

    public void dialogdismiss(){
        if (dialog != null) {
            dialog.dismiss();
        }
    }
}
