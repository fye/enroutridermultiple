package com.enroutrider.ride_.dbModels;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class RiderCancelled {


   public String driverId;

   public String requestId;

  @ServerTimestamp
   public Date rejectedAt;
}
