package com.enroutrider.ride_;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.google.android.gms.tasks.OnCompleteListener;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.kaopiz.kprogresshud.KProgressHUD;

import id.voela.actrans.AcTrans;


public class EnterEmailForVerificationActivity extends AppCompatActivity {

    Button continue_verify;
    EditText email;
    PinView verifycode;
    SharedPreferences prefs;
    TextView enter_text;
    View view_verify;
    KProgressHUD dialog;

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_email_for_verification);

      prefs =  getSharedPreferences("details",
                Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = prefs.edit();

        email=(EditText) findViewById(R.id.email);

        continue_verify=(Button)findViewById(R.id.continue_anyways);

        verifycode=(PinView) findViewById(R.id.verify_code);
        view_verify=(View)findViewById(R.id.view_verify);
        enter_text = findViewById(R.id.enter_text);


        verifycode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() < 4) {
                    continue_verify.setVisibility(View.INVISIBLE);

                } else {
                    continue_verify.setVisibility(View.VISIBLE);
                    hideKeyboard(EnterEmailForVerificationActivity.this);

                }

            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() < 10) {
                    verifycode.setVisibility(View.INVISIBLE);
                    enter_text.setVisibility(View.INVISIBLE);
                    continue_verify.setVisibility(View.INVISIBLE);
                    verifycode.getText().clear();

                } else {

                    verifycode.setVisibility(View.VISIBLE);
                    enter_text.setVisibility(View.VISIBLE);
                    hideKeyboard(EnterEmailForVerificationActivity.this);

                }

            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });



        continue_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                dialog = KProgressHUD.create(EnterEmailForVerificationActivity.this)
                        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                        .setCancellable(false)
                        .setAnimationSpeed(2)
                        .setBackgroundColor(Color.TRANSPARENT)
                        .setDimAmount(0.5f)
                        .show();

                db.collection("DRIVERS")
                        .whereEqualTo("phoneNumber",email.getText().toString().trim())
                        .whereEqualTo("riderPassword",verifycode.getText().toString().trim())
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    if(task.getResult().isEmpty())
                                    {
                                        Log.d("Tag", "No Rider Found");

                                        continue_verify.setVisibility(View.INVISIBLE);

                                        Toast.makeText(getApplicationContext(), "Incorrect login details. Kindly try again with correct details", Toast.LENGTH_LONG).show();

                                        verifycode.getText().clear();
                                        email.getText().clear();

                                        dialogdismiss();

                                    }
                                    else

                                    {

                                        dialogdismiss();

                                        editor.putString("verifyemail", email.getText().toString().trim());

                                        editor.apply();

                                        Intent intent = new Intent(EnterEmailForVerificationActivity.this,StartLoadingActivity.class);

                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_SINGLE_TOP);

                                        startActivity(intent);
                                        new AcTrans.Builder(EnterEmailForVerificationActivity.this).performSlideToLeft();



                                    }
                                } else {

                                    dialogdismiss();
                                    Log.w("Tag", "Error connecting to server.", task.getException());
                                }
                            }
                        });


            }



        });



    }

    public static void hideKeyboard(Activity activity)
    {
        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if(view==null){view= new View(activity);}
        imm.hideSoftInputFromWindow(view.getWindowToken(),0);
    }

    @Override
    protected void onResume() {
        super.onResume();
       // getApplicationContext().registerReceiver(receiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

    }

    @Override
    protected void onPause() {
        super.onPause();
      //  getApplicationContext().unregisterReceiver(receiver);
        dialogdismiss();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new AcTrans.Builder(this).performSlideToRight();
    }

    @Override
    public void onStop() {
        super.onStop();

        dialogdismiss();
    }



    @Override
    public void onDestroy() {
        super.onDestroy();

        dialogdismiss();
    }

    public void dialogdismiss(){
        if (dialog != null) {
            dialog.dismiss();
        }
    }
}
