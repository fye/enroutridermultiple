package com.enroutrider.ride_.model;


public class Delayed {

    public Delayed(String DispatchId, String requestId,String pickupName, String destinationName) {
        super();

        this.setPlace(pickupName);
        this.setDestinations(destinationName);
        this.setDispatchId(DispatchId);
        this.setRequestId(requestId);
    }



    public String dispatchId;

    public String requestId;

    public String place;

    public String destinations;

    public String getDispatchId() {
        return dispatchId;
    }

    public void setDispatchId(String dispatchId) {
        this.dispatchId = dispatchId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }



    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDestinations() {
        return destinations;
    }

    public void setDestinations(String destinations) {
        this.destinations = destinations;
    }


}