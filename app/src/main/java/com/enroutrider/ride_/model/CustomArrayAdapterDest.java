package com.enroutrider.ride_.model;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.enroutrider.ride_.R;

import java.util.ArrayList;
import java.util.List;

public class CustomArrayAdapterDest extends ArrayAdapter<Destination> {

    private List<Destination> PlaceList = new ArrayList<Destination>();

    static class TripViewHolder {

        TextView PlaceName;
        ImageView doneImg;
    }

    public CustomArrayAdapterDest(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    @Override
    public void add(Destination object) {
        PlaceList.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return this.PlaceList.size();
    }

    @Override
    public Destination getItem(int index) {
        return this.PlaceList.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        TripViewHolder viewHolder;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.trip_summary_pickups, parent, false);
            viewHolder = new TripViewHolder();

            viewHolder.doneImg = (ImageView) row.findViewById(R.id.done);

            viewHolder.PlaceName = (TextView) row.findViewById(R.id.pickup_location);

            row.setTag(viewHolder);
        } else {
            viewHolder = (TripViewHolder)row.getTag();
        }

        Destination the_place = getItem(position);

        viewHolder.PlaceName.setText(the_place.getPlaceName());

        viewHolder.doneImg.setImageResource(the_place.getDone());

        return row;
    }


}